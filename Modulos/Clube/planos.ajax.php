<?php

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);
include "../../Connections/configini.php";

$mdl_seg_ = returnPart($_SESSION["modulos"],14);

$tipo = $_REQUEST['txtTipo'];
$pessoa = $_REQUEST['txtPessoa'];
$lote = $_REQUEST['txtLote'];
$local = array();

if ($mdl_seg_ == 0 && ((is_numeric($pessoa) && $pessoa > 0 && ($tipo == 1 || $tipo == 2)) || (is_numeric($lote) && $tipo == 0) || ($lote == "null" && $tipo == 0))) {
    $sql = "select distinct p.conta_produto, p.descricao from sf_vendas_planos vp 
    inner join sf_produtos p on vp.id_prod_plano = p.conta_produto
    where dt_cancelamento is null and p.inativa = 0 " . ($tipo == 0 ? " and p.tp_valid_acesso in ('L','E') " : "") . "
    and ((DATEADD(day,(select ACA_PLN_DIAS_RENOVACAO from sf_configuracao),vp.dt_fim) > GETDATE() and p.parcelar = 0) or
    (vp.dt_fim >= GETDATE() and p.parcelar = 1) or
    ((select COUNT(*) from sf_vendas_planos_mensalidade C where C.id_plano_mens = vp.id_plano and dt_pagamento_mens is null) > 0))    
    and favorecido in (" . ($tipo == 0 ? "select id_fornecedores_despesas from sf_fornecedores_despesas where grupo_pessoa in (" . 
    (is_numeric($lote) ? $lote : "select id_grupo from sf_grupo_cliente where tipo_grupo = 'C' and inativo_grupo = 0") . ")" : $pessoa) . ") order by descricao";
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('id_plano' => utf8_encode($RFP['conta_produto']), 'descricao' => utf8_encode($RFP['descricao']));
    }
} else if ($mdl_seg_ > 0 && ((is_numeric($pessoa) && $pessoa > 0 && ($tipo == 1 || $tipo == 2)) || (is_numeric($lote) && $tipo == 0) || ($lote == "null" && $tipo == 0))) {    
    $sql = "select distinct vp.id_plano, p.conta_produto, p.descricao, v.placa 
    from sf_vendas_planos vp 
    inner join sf_produtos p on vp.id_prod_plano = p.conta_produto
    left join sf_fornecedores_despesas_veiculo v on v.id = vp.id_veiculo
    where dt_cancelamento is null and p.inativa = 0 " . ($tipo == 0 ? " and p.tp_valid_acesso in ('L','E') " : "") . "
    and ((DATEADD(day,(select ACA_PLN_DIAS_RENOVACAO from sf_configuracao),vp.dt_fim) > GETDATE() and p.parcelar = 0) or
    (vp.dt_fim >= GETDATE() and p.parcelar = 1) or
    ((select COUNT(*) from sf_vendas_planos_mensalidade C where C.id_plano_mens = vp.id_plano and dt_pagamento_mens is null) > 0))    
    and favorecido in (" . ($tipo == 0 ? "select id_fornecedores_despesas from sf_fornecedores_despesas where grupo_pessoa in (" . 
    (is_numeric($lote) ? $lote : "select id_grupo from sf_grupo_cliente where tipo_grupo = 'C' and inativo_grupo = 0") . ")" : $pessoa) . ") order by descricao";
    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $local[] = array('id_plano' => utf8_encode($RFP['id_plano']), 'descricao' => (strlen($RFP['placa']) > 0 ? $RFP['placa'] . " - " : "") . utf8_encode($RFP['descricao']));
    }    
}

echo(json_encode($local));
odbc_close($con);
