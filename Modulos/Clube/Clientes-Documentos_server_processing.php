<?php

require_once(__DIR__ . '/../../Connections/configini.php');
$aColumns = array('id_fornecedores_despesas', 'cnpj', 'razao_social', 'data_nascimento', 'id_documento', 'data_documento', 'descricao_doc', 'validade_doc');
$table = "sf_fornecedores_despesas left join sf_fornecedores_despesas_documentos on id_fornecedor_despesas = id_fornecedores_despesas
and dateadd(day, isnull((select max(validade_doc) from sf_documentos where id_doc = id_documento),0),data_documento) > getdate()
left join sf_documentos on id_documento = id_doc";

$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sOrder = " ORDER BY " . $aColumns[0] . " asc ";
$sOrder2 = " ORDER BY " . $aColumns[0] . " asc ";
$sLimit = 20;
$imprimir = 0;

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i])], 0) . " " . $_GET['sSortDir_' . $i] . ", ";
            $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i])], 1) . " " . $_GET['sSortDir_' . $i] . ", ";
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY " . $aColumns[0] . " asc";
        $sOrder2 = " ORDER BY " . $aColumns[0] . " asc";
    }
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $sWhereX .= " and dateadd(day, validade_doc, data_documento) between " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']);
}  

if (isset($_GET['doc']) && $_GET['doc'] != 'null') {
    $sWhereX .= " and id_documento in (" . $_GET['doc'] . ")";
}

if ($_GET['txtBusca'] != "") {
    $sWhereX .= " and (razao_social like '%" . $_GET['txtBusca'] . "%')";
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($i == 0) {
        $colunas = $aColumns[$i];
    } else {
        $colunas = $colunas . "," . $aColumns[$i];
    }
}

$sQuery = "set dateformat dmy;SELECT COUNT(*) total from " . $table . " where " . $aColumns[0] . " > 0 AND tipo = 'C' and inativo = 0 " . $sWhereX;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
    $iFilteredTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$sQuery1 = "set dateformat dmy;SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row," . $colunas . ",fornecedores_status, dateadd(day, validade_doc, data_documento) data_validade,
(STUFF((SELECT distinct ',' + B.descricao from sf_vendas_planos A inner join sf_produtos B on A.id_prod_plano = B.conta_produto
inner join sf_fornecedores_despesas D on D.id_fornecedores_despesas = favorecido left join sf_vendas_planos_dcc_agendamento C on A.id_plano = C.id_plano_agendamento
where favorecido = sf_fornecedores_despesas.id_fornecedores_despesas and ((DATEADD(day,(select ACA_PLN_DIAS_RENOVACAO from sf_configuracao),A.dt_fim) > GETDATE() and B.parcelar = 0) or
(A.dt_fim >= GETDATE() and B.parcelar = 1) or ((select COUNT(*) from sf_vendas_planos_mensalidade C where C.id_plano_mens = A.id_plano and dt_pagamento_mens is null) > 0))
FOR XML PATH('')), 1, 1, '')) descricao2    
from " . $table . " where " . $aColumns[0] . " > 0 AND tipo = 'C' and inativo = 0 " . $sWhereX . 
") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder2;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<center><span title='" . $aRow['fornecedores_status'] . "' class='label-" . $aRow['fornecedores_status'] . "' style='display: inline-block;width: 13px;height: 13px;border-radius: 50%!important;'></span></center>";
    $row[] = "<center>" . $aRow[$aColumns[0]] . "</center>";
    $row[] = "<center>" . $aRow[$aColumns[1]] . "</center>";
    $row[] = utf8_encode($aRow[$aColumns[2]]);
    $row[] = utf8_encode($aRow["descricao2"]);
    $row[] = escreverData($aRow[$aColumns[3]]);
    $row[] = utf8_encode($aRow[$aColumns[6]]);
    $row[] = escreverData($aRow[$aColumns[5]]);
    $row[] = escreverData($aRow["data_validade"]);
    $row[] = "<center><a href=\"javascript:;\" data-id=\"" . $aRow[$aColumns[0]] . "\" data-ix=\"" . $aRow[$aColumns[4]] . "\" data-ex=\"" . utf8_encode($aRow[$aColumns[6]]) . "\" data-dt=\"" . escreverData($aRow[$aColumns[5]]) . "\" id=\"btnAlt\"><span class=\"ico-edit\" title=\"Alterar\" style=\"font-size:18px;\"></span></a></center>";
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
