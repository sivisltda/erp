<?php
include '../../Connections/configini.php';
$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_categoria_socio set " .
        "nome_cat = " . valoresTexto("txtDescricao") . "," .
        "dependente_cat = " . valoresSelect("txtDependente") . "," .
        "idade_cat = " . valoresNumericos("txtLimite") . "," .
        "masc_cat = " . valoresSelect("txtMasculino") . "," .
        "fem_cat = " . valoresSelect("txtFeminino") . 
        " where id_cat = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_categoria_socio(nome_cat, dependente_cat, idade_cat, masc_cat, fem_cat) values (" .
        valoresTexto("txtDescricao") . "," .
        valoresSelect("txtDependente") . "," .
        valoresNumericos("txtLimite") . "," .
        valoresSelect("txtMasculino") . "," .
        valoresSelect("txtFeminino") . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_cat from sf_categoria_socio order by id_cat desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_categoria_socio WHERE id_cat = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_categoria_socio where id_cat =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = $RFP['id_cat'];
        $descricao = $RFP['nome_cat'];
        $dependente = $RFP['dependente_cat'];
        $limite = $RFP['idade_cat'];
        $masculino = $RFP['masc_cat'];
        $feminino = $RFP['fem_cat'];      
    }
} else {
    $disabled = '';
    $id = '';
    $descricao = '';
    $dependente = '';
    $limite = '99';
    $masculino = '';
    $feminino = '';    
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script> 
<script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<body>
    <form action="FormCategoria-Socio.php" name="frmCategoria-Socio" method="POST">
        <div class="frmhead">
            <div class="frmtext">Categoria de Sócio</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont">
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <div style="width: 100%; float: left;">
                <span>Descrição:</span>
                <input name="txtDescricao" id="txtDescricao" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="128" value="<?php echo utf8_encode($descricao); ?>"/>
            </div>
            <div style="width: 30%; float: left;">
                <span>Aceita Dependente:</span>
                <select name="txtDependente" id="txtDependente" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                    <option value="1"<?php if (!(strcmp(1, $dependente))) { echo "SELECTED";} ?>>SIM</option>
                    <option value="0"<?php if (!(strcmp(0, $dependente))) { echo "SELECTED";} ?>>NÃO</option>
                </select>                     
            </div>
            <div style="width: 30%; float: left; margin-left: 1%">
                <span>Idade Limite:</span>
                <input name="txtLimite" id="txtLimite" <?php echo $disabled; ?> type="text" class="input-medium" maxlength="3" value="<?php echo utf8_encode($limite); ?>"/>
            </div>
            <div style="width: 100%;float: left">
                <span>Sexo Masculino:</span>
                <select name="txtMasculino" id="txtMasculino" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                    <option value="null">Não Definida</option>
                    <?php $cur = odbc_exec($con, "select id_cat, nome_cat from sf_categoria_socio order by id_cat") or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) { ?>
                        <option value="<?php echo $RFP['id_cat'] ?>"<?php
                        if (!(strcmp($RFP['id_cat'], $masculino))) {
                            echo "SELECTED";
                        }
                        ?>><?php echo utf8_encode($RFP['nome_cat']) ?></option>
                    <?php } ?>
                </select>                
            </div>
            <div style="width: 100%;float: left">
                <span>Sexo Feminino:</span>
                <select name="txtFeminino" id="txtFeminino" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                    <option value="null">Não Definida</option>
                    <?php $cur = odbc_exec($con, "select id_cat, nome_cat from sf_categoria_socio order by id_cat") or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) { ?>
                        <option value="<?php echo $RFP['id_cat'] ?>"<?php
                        if (!(strcmp($RFP['id_cat'], $feminino))) {
                            echo "SELECTED";
                        }
                        ?>><?php echo utf8_encode($RFP['nome_cat']) ?></option>
                    <?php } ?>
                </select>                
            </div>
            <div style="clear:both;height: 15px;"></div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" type="submit" name="bntSave" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK" ><span class="ico-reply"></span> Cancelar</button>
                    <?php
                    }
                } else { ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <?php odbc_close($con); ?>
</body>
