<?php
include '../../Connections/configini.php';

$DescPagina = "Gestão de Folha";
$PageName = "Clientes-Folha";
$Module = "Clube";
$aColumnsX = array('Mat.', 'Nome', 'CPF/CNPJ', 'Mês', 'Ano', 'Rúbrica', 'Descrição', 'Valor', 'Categoria');
$aColumnsP = array('4', '20', '10', '5', '5', '6', '21', '7', '16');
$descricao_1 = "";
$descricao_2 = "";

$cur = odbc_exec($con, "select descricao_campo from sf_configuracao_campos_livres where id_campo = 1");
while ($RFP = odbc_fetch_array($cur)) {
    $descricao_1 = utf8_encode($RFP["descricao_campo"]);
}

$cur = odbc_exec($con, "select descricao_campo from sf_configuracao_campos_livres where id_campo = 2");
while ($RFP = odbc_fetch_array($cur)) {
    $descricao_2 = utf8_encode($RFP["descricao_campo"]);
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>        
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>                
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1><?php echo $Module; ?><small><?php echo $DescPagina; ?></small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">  
                                <div style="margin-top: 15px;float:left;">
                                    <div style="float:left">
                                        <form method="POST">
                                            <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir('I')" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                            <button id="btnExcel" class="button button-blue btn-primary" type="button" onclick="imprimir('E')" title="Exportar Excel"><span class="ico-download-3"></span></button>                                            
                                        </form>
                                    </div>
                                </div>                                
                                <div style="float: right;"> 
                                    <div style="float:left;margin-left: 0.5%; width: 23%;">
                                        <span style="display:block;">Plano com Rúbrica:</span>
                                        <select id="txtPlano" name="txtPlano[]" multiple="multiple" class="select" style="min-width:150px; min-height: 29px;" class="input-medium">
                                            <?php $cur = odbc_exec($con, "select id_contas_movimento, descricao from sf_contas_movimento where tipo = 'L' and inativa = 0 order by descricao") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id_contas_movimento'] ?>"><?php echo utf8_encode($RFP['descricao']); ?></option>
                                            <?php } ?>
                                        </select>                                            
                                    </div>                              
                                    <div style="float:left;margin-left: 0.5%; width: 23%;">
                                        <span style="display:block;">Categoria:</span>
                                        <select id="txtCategoria" name="txtCategoria[]" multiple="multiple" class="select" style="min-width:150px; min-height: 29px;" class="input-medium">
                                            <?php $cur = odbc_exec($con, "select id_cat, nome_cat from sf_categoria_socio order by nome_cat") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id_cat'] ?>"><?php echo utf8_encode($RFP['nome_cat']); ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>                              
                                    <div style="float:left; margin-left:0.5%; width: 12%;">
                                        <span style="display:block;">Validade De:</span>
                                        <input id="txt_dt_begin" type="text" class="datepicker" style="width:79px; margin-bottom:2px" value="<?php echo getData("B"); ?>">
                                    </div>            
                                    <div style="float:left; margin-left:0.5%; width: 12%;">
                                        <span style="display:block;">até:</span>
                                        <input id="txt_dt_end" type="text" class="datepicker" style="width:79px; margin-bottom:2px" value="<?php echo getData("E"); ?>" >
                                    </div>                                    
                                    <div style="float:left;margin-left: 0.5%; width: 22%;">
                                        <div width="100%">Busca:</div>
                                        <input name="txtBusca"  id="txtBusca" maxlength="100" type="text" style="width:100%;" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>"/>
                                    </div>
                                    <div style="margin-top: 15px;float:left;margin-left:0.3%;width: 1%;">
                                        <div style="float:left">
                                            <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext"><?php echo $DescPagina; ?></div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tbCliente">
                            <thead>
                                <tr>
                                    <?php for ($i = 0; $i < count($aColumnsX); $i++) { ?>
                                        <th width="<?php echo $aColumnsP[$i]; ?>%"><?php echo $aColumnsX[$i]; ?></th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                    <table class="table" cellpadding="0" cellspacing="0" width="100%" style="margin-bottom: 40px;">
                        <tr>
                            <td style="text-align: right;"><div id="lblTotParc" name="lblTotParc">Valor Total: <strong><?php echo escreverNumero(0, 1); ?></strong></div></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/highlight/jquery.highlight-4.js'></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>         
        <script type='text/javascript' src='../../js/util.js'></script>           
        <script type="text/javascript">

            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }
            
            function AbrirBox(id) {
                window.location = './../Academia/ClientesForm.php?id=' + id;
            }

            function finalFind(imp) {
                var retPrint = "";
                retPrint = '?imp=0';
                if (imp === 1) {
                    retPrint = '&imp=1';
                }
                if ($("#txtPlano").val() !== null) {
                    retPrint = retPrint + "&pln=" + $("#txtPlano").val();
                }
                if ($("#txtCategoria").val() !== null) {
                    retPrint = retPrint + "&ctg=" + $("#txtCategoria").val();
                }                
                if ($("#txt_dt_begin").val() !== "") {
                    retPrint = retPrint + "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "_");
                }
                if ($("#txt_dt_end").val() !== "") {
                    retPrint = retPrint + "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "_");
                }                     
                if ($("#txtBusca").val() !== "") {
                    retPrint = retPrint + '&txtBusca=' + $("#txtBusca").val();
                }
                return "Clientes-Folha_server_processing.php" + retPrint.replace(/\//g, "_");
            }
            
            function imprimir(tipo) {
                let label = ($("#txt_dt_begin").val() !== "" && $("#txt_dt_end").val() !== "" ? "Validade de " + $("#txt_dt_begin").val() + " até " + $("#txt_dt_end").val() : "");
                var pRel = "&NomeArq=" + "Gestão de Folha" +                        
                        "&lbl=Mat.|Nome|CPF/CNPJ|Mês|Ano|Rúbrica|Descrição|Valor|Categoria" + (tipo === "E" ? "|<?php echo $descricao_1 . "|" . $descricao_2;?>" : "") +
                        "&siz=52|170|65|35|35|41|120|51|131" + (tipo === "E" ? "|70|70" : "") +
                        "&pdf=12" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=" + label + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/Clube/" + finalFind(1); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=" + tipo + "&PathArq=GenericModelPDF.php", '_blank');
            }
            
            $("#btnfind").click(function () {
                $("#tbCliente").dataTable().fnDestroy();
                listaTable();
            });

            function listaTable() {
                myTable = $('#tbCliente').dataTable({
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "aoColumns": [
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false}],
                    "sAjaxSource": finalFind(0),
                    'oLanguage': {
                        'oPaginate': {
                            'sFirst': "Primeiro",
                            'sLast': "Último",
                            'sNext': "Próximo",
                            'sPrevious': "Anterior"
                        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                        'sLengthMenu': "Visualização de _MENU_ registros",
                        'sLoadingRecords': "Carregando...",
                        'sProcessing': "Processando...",
                        'sSearch': "Pesquisar:",
                        'sZeroRecords': "Não foi encontrado nenhum resultado"},
                    "fnInitComplete": function (oSettings, json) {
                        refreshTotal();
                    },                    
                    "sPaginationType": "full_numbers"
                });
            }
            
            function refreshTotal() {
                $('#lblTotParc').html('Valor Total: <strong>' + $('#totparc').val() + '</strong>').show('slow');
            }
            
            listaTable();
        </script>         
        <?php odbc_close($con); ?>
    </body>
</html>
