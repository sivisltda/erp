<?php include '../../Connections/configini.php'; ?>
<link rel="icon" type="image/ico" href="../../../favicon.ico"/>
<link href="../../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../../css/main.css" rel="stylesheet">
<link href="../../../css/bootbox.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript' src='../../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
<body>
    <div class="row-fluid">
        <form method="POST" id="frmEnviaDados">
            <div class="frmhead">
                <div class="frmtext">TAXA DE LIQUIDAÇÃO DE BOLETO</div>
                <div class="frmicon" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input id="txtId" name="txtId" value="" type="hidden"/>
                <div style="float: left; width: 26%;">
                    <span>Data Cadastro:</span>
                    <input id="txtData" name="txtData" class="datepicker inputCenter" type="text" value="<?php echo getData("T"); ?>">
                </div>                                                               
                <div style="float: left; width: 46%; margin-left: 1%;">
                    <span>Formas de Pagamento:</span>
                    <select id="txtForma" name="txtForma" class="select" style="width: 100%;">
                        <option value="null">Selecione</option>
                        <?php $cur = odbc_exec($con, "select id_tipo_documento,descricao from sf_tipo_documento where s_tipo in ('A','D') order by descricao") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                            <option value="<?php echo $RFP['id_tipo_documento'] ?>"><?php echo utf8_encode($RFP['descricao']) ?></option>
                        <?php } ?>                        
                    </select>
                </div>                                
                <div style="float: left; width: 26%; margin-left: 1%;">
                    <span>Valor Total:</span>
                    <input id="txtValor" name="txtValor" type="text" style="text-align: right;" class="input-medium" value="<?php echo escreverNumero($_GET["total"]); ?>"/>
                </div>                
                <div style="float: left; width: 100%;">
                    <span>Grupos de Contas:</span>
                    <select id="txtGrupo" name="txtGrupo" class="select" style="width: 100%;">
                        <option value="null">Selecione</option>
                        <?php $cur = odbc_exec($con, "select id_grupo_contas,descricao from sf_grupo_contas where deb_cre = 'D' and inativo = 0 order by descricao") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                <option value="<?php echo $RFP['id_grupo_contas'] ?>"><?php echo utf8_encode($RFP['descricao']) ?></option>
                        <?php } ?>
                    </select>
                </div>                
                <div style="float: left; width: 100%;">
                    <span>Subgrupos de Contas:</span>
                    <select id="txtConta" name="txtConta" class="select" style="width: 100%;">
                        <option value="null">Selecione</option>
                    </select>
                </div>                                
                <div style="float: left; width: 100%;">
                    <span>Fornecedor/Funcionário/Transportadora:</span>
                    <input type="hidden" id="txtFonecedor" name="txtFonecedor" value="">
                    <input type="text" id="txtFonecedorNome" value="" class="ui-autocomplete-input" autocomplete="off">
                </div>                                
                <div style="clear:both;"></div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <button class="btn btn-success" type="button" onClick="salvar();" id="btnGravarPlano" title="Gravar"><span class="ico-checkmark"></span> Gravar</button>
                    <button class="btn yellow" title="Cancelar" onClick="parent.FecharBox()"><span class="ico-reply"></span> Cancelar</button>
                </div>
            </div>
        </form>
    </div>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../../js/plugins.js'></script>
    <script type="text/javascript" src="../../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type='text/javascript' src='../../../js/plugins/datatables/fnReloadAjax.js'></script>
    <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script> 
    <script type='text/javascript' src='../../../js/jquery.priceformat.min.js'></script>
    <script type="text/javascript" src="../../js/util.js"></script>
    <script type="text/javascript">

        $("#txtData").mask(lang["dateMask"]);
        $("#txtValor").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});

        $("#txtFonecedorNome").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "../../Modulos/CRM/ajax.php",
                    dataType: "json",
                    data: {q: request.term, r: "S", t: ($("#txtTipoGrupo").val() === 'C' ? 'C' : 'PFE')},
                    success: function (data) {
                        response(data);
                    }
                });
            }, minLength: 3,
            select: function (event, ui) {
                $("#txtFonecedor").val(ui.item.id);
                $("#txtFonecedorNome").val(ui.item.value);
            }
        });
            
        $('#txtGrupo').change(function () {
            if ($(this).val()) {
                $.getJSON('../../Modulos/Contas-a-pagar/conta.ajax.php', {txtGrupo: $(this).val(), ajax: 'true'}, function (j) {
                    var options = '<option value="null">--Selecione--</option>';
                    for (var i = 0; i < j.length; i++) {
                        options += '<option value="' + j[i].id_contas_movimento + '">' + j[i].descricao + '</option>';
                    }
                    $('#txtConta').html(options).show();
                    $('#txtConta').css("display", "none");
                });
            } else {
                $('#txtConta').html('<option value="null">--Selecione--</option>');
                $('#txtConta').css("display", "none");
            }
        });

        function validar() {
            if(!moment($('#txtData').val(), lang["dmy"], true).isValid()) {                
                bootbox.alert("Preencha o campo Data Cadastro!");
                return false;
            } else if ($('#txtForma').val() === "null") {
                bootbox.alert("Preencha o campo Formas de Pagamento!");
                return false;                
            } else if ($('#txtValor').val() === "0,00") {
                bootbox.alert("Preencha o campo Valor!");
                return false;                
            } else if ($('#txtGrupo').val() === "null") {
                bootbox.alert("Preencha o campo Grupos de Contas!");
                return false;
            } else if ($('#txtConta').val() === "null") {
                bootbox.alert("Preencha o campo Subgrupos de Contas!");
                return false;
            } else if ($('#txtFonecedor').val() === "") {
                bootbox.alert("Preencha o campo Fornecedor/Funcionário/Transportadora!");
                return false;
            }
            return true;
        }

        function salvar() {
            if (validar()) {
                $.post("form/FormSolicitacao-de-Autorizacao.php",
                "btnSave=S&txtEmpresa=" + parent.$("#txtMnFilial").val() + "&" + 
                $("#frmEnviaDados").serialize()).done(function (data) {
                    if (data === "YES") {
                        parent.FecharBox();
                    } else {
                        bootbox.alert("Erro: " + data);
                    }
                });
            }
        }

    </script>
    <?php odbc_close($con); ?>
</body>
