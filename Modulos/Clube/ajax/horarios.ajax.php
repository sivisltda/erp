<?php

require_once(__DIR__ . '/../../../Connections/configini.php');

$data = $_REQUEST['dt'];
$idAgenda = $_REQUEST['ag'];
$intervalo = 0;
$return = array();

$cur = odbc_exec($con, "select adm_agen_intervalo from sf_configuracao") or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $intervalo = $RFP["adm_agen_intervalo"];
}

if (is_numeric($idAgenda) && valoresData2($data) != "null" && $intervalo > 0) {
    $periodos = array();
    $cur = odbc_exec($con, "set dateformat dmy; select DATEPART(weekday," . valoresData2($data) . ") dia_semana,
    faixa1_ini, faixa1_fim, dia_semana1, faixa2_ini, faixa2_fim, dia_semana2, 
    faixa3_ini, faixa3_fim, dia_semana3, faixa4_ini, faixa4_fim, dia_semana4, 
    faixa5_ini, faixa5_fim, dia_semana5 from sf_agenda 
    inner join sf_turnos on id_turno = cod_turno where id_agenda = " . $idAgenda) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        if (valoresHora2($RFP["faixa1_ini"]) != "null" && valoresHora2($RFP["faixa1_fim"]) != "null" && $RFP["dia_semana1"] && substr($RFP["dia_semana1"], $RFP["dia_semana"] - 1, 1) == 1) {
            $periodos[] = array($RFP["faixa1_ini"], $RFP["faixa1_fim"]);
        }
        if (valoresHora2($RFP["faixa2_ini"]) != "null" && valoresHora2($RFP["faixa2_fim"]) != "null" && $RFP["dia_semana2"] && substr($RFP["dia_semana2"], $RFP["dia_semana"] - 1, 1) == 1) {
            $periodos[] = array($RFP["faixa2_ini"], $RFP["faixa2_fim"]);
        }
        if (valoresHora2($RFP["faixa3_ini"]) != "null" && valoresHora2($RFP["faixa3_fim"]) != "null" && $RFP["dia_semana3"] && substr($RFP["dia_semana3"], $RFP["dia_semana"] - 1, 1) == 1) {
            $periodos[] = array($RFP["faixa3_ini"], $RFP["faixa3_fim"]);
        }
        if (valoresHora2($RFP["faixa4_ini"]) != "null" && valoresHora2($RFP["faixa4_fim"]) != "null" && $RFP["dia_semana4"] && substr($RFP["dia_semana4"], $RFP["dia_semana"] - 1, 1) == 1) {
            $periodos[] = array($RFP["faixa4_ini"], $RFP["faixa4_fim"]);
        }
        if (valoresHora2($RFP["faixa5_ini"]) != "null" && valoresHora2($RFP["faixa5_fim"]) != "null" && $RFP["dia_semana5"] && substr($RFP["dia_semana5"], $RFP["dia_semana"] - 1, 1) == 1) {
            $periodos[] = array($RFP["faixa5_ini"], $RFP["faixa5_fim"]);
        }
    }
    $agendamentos = array();
    $cur = odbc_exec($con, "set dateformat dmy;select data_inicio,data_fim from sf_agendamento 
    where " . valoresData2($data) . " between cast(data_inicio as date) and cast(data_fim as date) and inativo = 0 and id_agenda = " . $idAgenda);
    while ($RFP = odbc_fetch_array($cur)) {
        $agendamentos[] = array(strtotime(str_replace("/", "-", escreverDataHora($RFP["data_inicio"]))), strtotime(str_replace("/", "-", escreverDataHora($RFP["data_fim"]))));
    }
    foreach ($periodos as $value) {
        $dt_ini = strtotime(str_replace("/", "-", $data) . " " . $value[0]);
        $total = ((strtotime(str_replace("/", "-", $data) . " " . $value[1]) - $dt_ini) / ($intervalo * 60));
        for ($i = 0; $i < $total; $i++) {
            $data_ini = ($dt_ini + ($intervalo * 60 * $i));
            $data_fim = (($dt_ini - 1) + ($intervalo * 60 * ($i + 1)));
            $return[] = array(
                "hora_ini" => date('H:i', $data_ini),
                "hora_fim" => date('H:i', $data_fim),
                "inativo" => checkFreeDate($agendamentos, $data_ini, $data_fim));
        }
    }
}

function checkFreeDate($agendamentos, $dt_ini, $dt_fim) {
    foreach ($agendamentos as $value) {
        if (($dt_ini >= $value[0] && $dt_ini <= $value[1]) || ($dt_fim >= $value[0] && $dt_fim <= $value[1])) {
            return 1;
        }
    }
    return 0;
}

echo(json_encode($return));
odbc_close($con);
