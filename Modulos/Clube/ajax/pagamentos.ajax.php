<?php

require_once(__DIR__ . '/../../../Connections/configini.php');

$idAluno = $_REQUEST['txtPessoa'];
$local = array();
$query = "select sf_vendas_itens.id_item_venda,sf_produtos.descricao,
sf_vendas.data_venda 'pagamento', sum(sf_vendas_itens.quantidade) total, 
(select isnull(count(id_convite),0) from sf_convites where inativo = 0 and sf_convites.item_venda = sf_vendas_itens.id_item_venda) usados
from sf_vendas inner join sf_vendas_itens on sf_vendas_itens.id_venda = sf_vendas.id_venda inner join sf_produtos on sf_produtos.conta_produto = sf_vendas_itens.produto
where status = 'Aprovado' and cov = 'V' and sf_produtos.tipo= 'S' and ativar_convite = 1 and cliente_venda = " . $idAluno . "
group by id_item_venda,descricao,sf_vendas.data_venda
having sum(sf_vendas_itens.quantidade) > (select isnull(count(id_convite),0) from sf_convites where inativo = 0 and sf_convites.item_venda = sf_vendas_itens.id_item_venda)
order by sf_vendas_itens.id_item_venda";
$cur = odbc_exec($con, $query) or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $local[] = array('id_item_venda' => utf8_encode($RFP['id_item_venda']),
        'pagamento' => escreverData($RFP['pagamento']), 'descricao' => utf8_encode($RFP['descricao']),
        'total' => utf8_encode($RFP['total']), 'usados' => utf8_encode($RFP['usados']));
}
echo(json_encode($local));
odbc_close($con);
