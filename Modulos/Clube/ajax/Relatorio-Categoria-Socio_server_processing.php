<?php

if (!isset($_GET['pdf'])) {
    require_once(__DIR__ . '/../../../Connections/configini.php');
    require_once(__DIR__ . '/../../../util/util.php');
}

$sQtd = 0;
$sLimit = 20;
$iTotal = 0;
$imprimir = 0;
$sOrder = " ORDER BY titularidade desc ";
$query = "set dateformat dmy;";
$grupoArray = [];
$where = "";
$dependenteBoleto = "0";
$planosBoleto = "0";

function status_cracha($tipo) {
    switch ($tipo) {
        case 1:
            return "CONFECÇÃO";
        case 2:
            return "SOLICITAÇÃO";
        case 3:
            return "RECEBIMENTO";
        case 4:
            return "CRACHÁ ENTREGUE";
        default:
            return "SELECIONE";
    }
}

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = "1";
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

$cur = odbc_exec($con, "select FIN_GRUPO_BOLETO,FIN_GRUPO_BOLETO_MOD from sf_configuracao") or die(odbc_errormsg());
while ($RFP = odbc_fetch_array($cur)) {
    $dependenteBoleto = $RFP['FIN_GRUPO_BOLETO'];
    $planosBoleto = $RFP['FIN_GRUPO_BOLETO_MOD'];
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '' && $_GET['dtp'] != '') {
    if ($_GET['dtp'] == "0") {
        $where .= " and dt_cadastro between " . valoresDataHoraUnico2($_GET['dti'] . " 00:00:00") . " and " . valoresDataHoraUnico2($_GET['dtf'] . " 23:59:59") . "";
    } elseif ($_GET['dtp'] == "1") {
        $where .= " and (id_fornecedores_despesas in (select favorecido from sf_vendas_planos vp
        inner join sf_vendas_planos_mensalidade vm on vp.id_plano = vm.id_plano_mens
        where dt_cancelamento is null and vm.dt_pagamento_mens is null and vm.dt_inicio_mens between " . valoresDataHoraUnico2($_GET['dti'] . " 00:00:00") . " and " . valoresDataHoraUnico2($_GET['dtf'] . " 23:59:59") . ")";
        if ($dependenteBoleto > 0) {
            $where .= " or id_fornecedores_despesas in (select id_titular from sf_vendas_planos vp
            inner join sf_vendas_planos_mensalidade vm on vp.id_plano = vm.id_plano_mens
            inner join sf_fornecedores_despesas_dependentes on id_dependente = favorecido
            where dt_cancelamento is null and vm.dt_pagamento_mens is null and vm.dt_inicio_mens between " . valoresDataHoraUnico2($_GET['dti'] . " 00:00:00") . " and " . valoresDataHoraUnico2($_GET['dtf'] . " 23:59:59") . ")";            
        }        
        $where .= ")";
    } elseif ($_GET['dtp'] == "2") {
        $where .= " and (id_fornecedores_despesas in (select favorecido from sf_vendas_planos vp
        inner join sf_vendas_planos_mensalidade vm on vp.id_plano = vm.id_plano_mens
        where dt_cancelamento is null and vm.dt_pagamento_mens is not null and vm.dt_pagamento_mens between " . valoresDataHoraUnico2($_GET['dti'] . " 00:00:00") . " and " . valoresDataHoraUnico2($_GET['dtf'] . " 23:59:59") . ")";
        if ($dependenteBoleto > 0) {
            $where .= " or id_fornecedores_despesas in (select id_titular from sf_vendas_planos vp
            inner join sf_vendas_planos_mensalidade vm on vp.id_plano = vm.id_plano_mens
            inner join sf_fornecedores_despesas_dependentes on id_dependente = favorecido
            where dt_cancelamento is null and vm.dt_pagamento_mens is not null and vm.dt_pagamento_mens between " . valoresDataHoraUnico2($_GET['dti'] . " 00:00:00") . " and " . valoresDataHoraUnico2($_GET['dtf'] . " 23:59:59") . ")";            
        }
        $where .= ")";        
    }
}

if (isset($_GET['ti']) && $_GET['ti'] != 'null') {
    $where .= " and titularidade in (" . $_GET['ti'] . ")";
}

if (isset($_GET['st']) && $_GET['st'] != 'null') {
    $where .= " and (";
    $dataAux = explode(",", $_GET['st']);
    for ($i = 0; $i < count($dataAux); $i++) {
        $where .= ($i > 0 ? " or " : "") . " fornecedores_status = '" . getStatus($dataAux[$i]) . "'";
    }
    $where .= ")";
}

if (isset($_GET['cr']) && $_GET['cr'] != 'null') {
    $where .= " and status_cracha in (" . $_GET['cr'] . ")";
}

if (isset($_GET['tp']) && $_GET['tp'] != 'null') {
    $where .= " and tipo_socio in (" . $_GET['tp'] . ")";
}

if (isset($_GET['gr']) && $_GET['gr'] != 'null') {
    $where .= " and grupo_pessoa in (" . $_GET['gr'] . ")";
}

if (isset($_GET['tr']) && is_numeric($_GET['tr'])) {
    $where .= " and trancar = " . $_GET['tr'];
}

if (isset($_GET['tt']) && is_numeric($_GET['tt'])) {
    $where .= " and titular = " . $_GET['tt'];
}

$query1 = " from sf_fornecedores_despesas left join sf_categoria_socio on sf_categoria_socio.id_cat = sf_fornecedores_despesas.titularidade
where socio = 1 and tipo = 'C' and inativo = 0 " . $where;
$cur = odbc_exec($con, $query . "SELECT COUNT(*) total " . $query1);
while ($RFP = odbc_fetch_array($cur)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iTotal,
    "aaData" => array()
);

$campoLivre = "";
$camposBusca = [];
if (isset($_GET['pdf']) && $_GET['imp'] == "2") {
    $cur = odbc_exec($con, "select id_campo from sf_configuracao_campos_livres where ativo_campo = 1;");
    while ($RFP = odbc_fetch_array($cur)) {
        $campoLivre .= ",(select top 1 conteudo_campo from sf_fornecedores_despesas_campos_livres where campos_campos = " . 
        $RFP['id_campo'] . " and fornecedores_campos = id_fornecedores_despesas) 'campo_livre_" . $RFP['id_campo'] . "' ";
        $camposBusca[] = "campo_livre_" . $RFP['id_campo'];
    }
}
$query .= "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,id_fornecedores_despesas, razao_social, cnpj, nip,
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'telefone',
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = sf_fornecedores_despesas.id_fornecedores_despesas) 'celular',
status_cracha,titular,data_nascimento, gran_esp, tipo_sang, inscricao_estadual, depto, trabalha_empresa, validadeCarteira, profissao, om,
(select top 1 razao_social from sf_fornecedores_despesas_dependentes 
inner join sf_fornecedores_despesas pt on pt.id_fornecedores_despesas = id_titular
where id_dependente = sf_fornecedores_despesas.id_fornecedores_despesas) nome_titular,
(select top 1 id_titular from sf_fornecedores_despesas_dependentes 
where id_dependente = sf_fornecedores_despesas.id_fornecedores_despesas) id_titular,       
(select top 1 razao_social from sf_fornecedores_despesas_responsavel 
inner join sf_fornecedores_despesas pt on pt.id_fornecedores_despesas = id_responsavel
where id_dependente = sf_fornecedores_despesas.id_fornecedores_despesas) nome_responsavel,
(select top 1 id_responsavel from sf_fornecedores_despesas_responsavel 
where id_dependente = sf_fornecedores_despesas.id_fornecedores_despesas) id_responsavel,       
sf_categoria_socio.nome_cat,
sf_categoria_socio.id_cat       
" . $campoLivre . $query1 . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir;
$tpImp = $_GET['imp'];
//echo $query; exit();
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<center>" . utf8_encode($RFP['id_fornecedores_despesas']) . "</center>";
    $row[] = "<a href='javascript:void(0)' onClick='AbrirCli(" . utf8_encode($RFP['id_fornecedores_despesas']) . ")'><div id='formPQ' title='" . utf8_encode($RFP['razao_social']) . "'>" . utf8_encode($RFP['razao_social']) . "</div></a>";
    $row[] = utf8_encode($RFP['cnpj']);
    $row[] = utf8_encode($RFP['nip']);
    $row[] = utf8_encode($RFP['celular']);
    $row[] = utf8_encode($RFP['telefone']);
    $row[] = status_cracha(utf8_encode($RFP['status_cracha']));
    $row[] = escreverData($RFP['data_nascimento']);
    $row[] = utf8_encode($RFP['nome_cat']);
    $row[] = "<center>" . ($RFP['titular'] == 1 ? "SIM" : "NÃO") . "</center>";
    if (isset($_GET['pdf']) && $tpImp == "1") {
        $row[] = $RFP['id_cat'];
        $grp = array($RFP['id_cat'], (is_numeric($RFP['id_cat']) ? $RFP['id_cat'] . " - " . utf8_encode($RFP['nome_cat']) : "SEM AGRUPAMENTO"), 10);
        if (!in_array($grp, $grupoArray)) {
            $grupoArray[] = $grp;
        }
    } else if (isset($_GET['pdf']) && $tpImp == "4") {
        $row[] = $RFP['id_titular'];
        $grp = array($RFP['id_titular'], (is_numeric($RFP['id_titular']) ? $RFP['id_titular'] . " - " . utf8_encode($RFP['nome_titular']) : "SEM TITULAR"), 10);
        if (!in_array($grp, $grupoArray)) {
            $grupoArray[] = $grp;
        }        
        $_GET['imp'] = "1";        
    } else if (isset($_GET['pdf']) && $_GET['imp'] == "2") {
        $row[] = utf8_encode($RFP['gran_esp']);
        $row[] = utf8_encode($RFP['tipo_sang']);
        $row[] = utf8_encode($RFP['inscricao_estadual']);
        $row[] = utf8_encode($RFP['depto']);
        $row[] = utf8_encode($RFP['trabalha_empresa']);
        $row[] = utf8_encode($RFP['id_titular']);
        $row[] = utf8_encode($RFP['nome_titular']);
        $row[] = utf8_encode($RFP['id_responsavel']);
        $row[] = utf8_encode($RFP['nome_responsavel']);
        $row[] = escreverData($RFP['validadeCarteira']); 
        $row[] = utf8_encode($RFP['om']);
        $row[] = utf8_encode($RFP['profissao']);        
        if (isset($_GET['pdf']) && $_GET['imp'] == "2") {
            foreach ($camposBusca as $value) {
                $row[] = utf8_encode($RFP[$value]);                      
            }            
        }              
    }
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
} else {
    $output['aaGrupo'] = $grupoArray;
}
odbc_close($con);

//echo json_encode($output); exit;