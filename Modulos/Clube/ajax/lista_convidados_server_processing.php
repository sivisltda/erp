<?php

require_once(__DIR__ . '/../../../Connections/configini.php');

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => 0,
    "iTotalDisplayRecords" => 0,
    "aaData" => array()
);

$sQuery = "select id_convidado,pessoa_convidado,cnpj,inscricao_estadual,data_nascimento,tipo_sang 
from sf_eventos_convidado left join sf_fornecedores_despesas on id_convidado = id_fornecedores_despesas 
where evento_convidado = " . valoresNumericos2($_GET['id']);
$cur = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = utf8_encode($RFP["pessoa_convidado"]) . (strlen($RFP["id_convidado"]) > 0 ? " (" . $RFP["id_convidado"] . ")" : "");
    $row[] = utf8_encode($RFP["cnpj"]);
    $row[] = utf8_encode($RFP["inscricao_estadual"]);
    $row[] = escreverData($RFP["data_nascimento"]);
    $row[] = utf8_encode($RFP["tipo_sang"]);
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
