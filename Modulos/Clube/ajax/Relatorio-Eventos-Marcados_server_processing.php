<?php

if (!isset($_GET['pdf'])) {
    require_once(__DIR__ . '/../../../Connections/configini.php');
    require_once(__DIR__ . '/../../../util/util.php');
}

$sQtd = 0;
$sLimit = 20;
$iTotal = 0;
$imprimir = 0;
$sOrder = " ORDER BY data_inicio desc ";
$query = "set dateformat dmy;";
$grupoArray = [];
$where = "";
$tipo = "0";

if (is_numeric($_GET['imp']) && $_GET['imp'] > 0) {
    $imprimir = "1";
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $where .= " and data_inicio between " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']) . "";
}

if (isset($_GET['lc']) && $_GET['lc'] != 'null') {
    $where .= " and local_evento in (" . $_GET['lc'] . ")";
}

if (is_numeric($_GET['tp']) && $_GET['tp'] > 0) {
    $tipo = "1";
}

$query1 = "from sf_eventos
INNER JOIN sf_fornecedores_despesas ON pessoa_evento = id_fornecedores_despesas
INNER JOIN sf_locacoes ON local_evento = id_loc
INNER JOIN sf_agendamento ON agendamento_evento = id_agendamento " .
($tipo == "1" ? "LEFT JOIN sf_eventos_convidado ON evento_convidado = id_evento" : "") .
" where sf_agendamento.inativo = 0 " . $where;
$cur = odbc_exec($con, $query . "SELECT COUNT(*) total " . $query1);
while ($RFP = odbc_fetch_array($cur)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iTotal,
    "aaData" => array()
);

$query .= "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,
id_evento, data_inicio, data_fim, razao_social, nome_loc, limite_loc, id_loc, " .
($tipo == "1" ? " pessoa_convidado," : "") .
"(select count(id_evento_convidado) from sf_eventos_convidado where evento_convidado = id_evento) Convidados  " . $query1 . "
) as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir;
//echo $query;exit();
$cur = odbc_exec($con, $query);
$lastEvent = "0";
while ($RFP = odbc_fetch_array($cur)) {
    if($lastEvent != $RFP['id_evento']) {
        $lastEvent = $RFP['id_evento'];
        $row = array();
        $row[] = "<center>" . utf8_encode($RFP['id_evento']) . "</center>";
        $row[] = escreverDataHora($RFP['data_inicio']) . " - " . escreverDataHora($RFP['data_fim']);
        $row[] = utf8_encode($RFP['razao_social']);
        $row[] = utf8_encode($RFP['nome_loc']);
        $row[] = "<center>" . utf8_encode($RFP['limite_loc']) . "</center>";
        $row[] = "<center>" . utf8_encode($RFP['Convidados']) . "</center>";
        if (isset($_GET['pdf']) && $_GET['imp'] == "1") {
            $row[] = $RFP['id_loc'];
            $grp = array($RFP['id_loc'], utf8_encode($RFP['nome_loc']), 6);
            if (!in_array($grp, $grupoArray)) {
                $grupoArray[] = $grp;
            }
        }
        $output['aaData'][] = $row;
    }
    if($tipo == "1") {
        $row = array();
        $row[] = "";
        $row[] = "";
        $row[] = utf8_encode($RFP['pessoa_convidado']);
        $row[] = "";
        $row[] = "";        
        $row[] = "";        
        $row[] = $RFP['id_loc'];
        $output['aaData'][] = $row;
    }
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
} else {
    $output['aaGrupo'] = $grupoArray;
}
odbc_close($con);
