$("#txt_dt_begin").mask(lang["dateMask"]);

var tblFiliais = $('#tblFiliais').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 50, 100, 500],
    "bProcessing": true,
    "bServerSide": true,
    "bFilter": false,
    "aoColumns": [{"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false},
        {"bSortable": false}],
    "sAjaxSource": finalFind(0),
    'oLanguage': {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "fnDrawCallback": function () {
        //refreshTotal();
    },
    "sPaginationType": "full_numbers"
});

$("#btnfind").click(function () {
    if (validar()) {
        restoreRow("", "");
    }
});

$("#ckb_grupoFin").change(function() {
    if (validar()) {
        restoreRow("", "");
    }
});

$('#txtBanco').change(function () {
    $("txtCarteira").attr('disabled', true);
    if ($(this).val()) {
        $('#txtBancoCarteira').hide();
        $.getJSON('./../Financeiro/bancoCarteira.ajax.php?search=', {txtBanco: $(this).val(), ajax: 'true'}, function (j) {
            options = '<option value="null">Selecione</option>';
            for (var i = 0; i < j.length; i++) {
                options += '<option value="' + j[i].sf_carteiras_id + '">' + j[i].sf_carteira_descricao + '</option>';
            }
            $('#txtCarteira').html(options).show();
            $("txtCarteira").attr('disabled', false);
        });
    } else {
        $('#txtCarteira').html('<option value="null">Selecione</option>');
        $("txtCarteira").attr('disabled', false);
    }
});

$('#txtLote').change(function () {
    if ($(this).val()) {
        getPlanos();
    } else if ($('#txtPlanos').prop('multiple')) {
        $('#txtPlanos').select2().html('');
    } else {
        $('#txtPlanos').html('<option value="null">Selecione</option>');
    }
});

$('#txt_dt_begin').change(function () {
    var datex = moment($(this).val(), 'DD/MM/YYYY');
    if ($('#txtTipo').val() === "0") {
        datex.add(1, 'months').calendar();
        datex.add(-1, 'days').calendar();
    } else {
        datex.add(12, 'months').calendar();
    }
    $("#txt_dt_end").val(datex.format("DD/MM/YYYY"));
});

function getPlanos() {
    $("txtPlanos").attr('disabled', true);
    $.getJSON('planos.ajax.php', {txtTipo: $('#txtTipo').val(), txtPessoa: $('#txtAluno').val(), txtLote: $('#txtLote').val()}, function (j) {
        options = ($('#txtPlanos').prop('multiple') ? '' : '<option value="null">Selecione</option>');
        for (var i = 0; i < j.length; i++) {
            options += '<option value="' + j[i].id_plano + '">' + j[i].descricao + '</option>';
        }
        if ($('#txtPlanos').prop('multiple')) {
            $('#txtPlanos').select2().html(options);
        } else {
            $('#txtPlanos').html(options);
        }
        $("#txtPlanos").attr('disabled', false);
    });
}

$('#txtTipo').change(function () {
    $("#txtLote").val("null");
    $("#txtAluno").val("0");
    $("#txtAlunoNome").val("");
    if ($('#txtTipo').val() === "0") {
        $("#txtLote").parent().show();
        $("#txt_dt_end").attr("disabled", true);
        $("#txt_dt_begin").val(moment().add(1, 'months').startOf('month').format('DD/MM/YYYY'));
        $("#txt_dt_end").val(moment().add(1, 'months').endOf('month').format('DD/MM/YYYY'));
        $("#txtAlunoNome").parent().hide();
    } else if ($("#txtTipo").val() === "1" || $("#txtTipo").val() === "2") {
        $("#txtLote").parent().hide();
        $("#txtAlunoNome").parent().show();
        $("#txt_dt_end").attr("disabled", false);
        $("#txt_dt_begin").val(moment().startOf('month').format('DD/MM/YYYY'));
        $("#txt_dt_end").val(moment().add(12, 'months').endOf('month').format('DD/MM/YYYY'));
    }
    getPlanos();
});

$("#txtAlunoNome").autocomplete({
    source: function (request, response) {
        $.ajax({
            url: "../../Modulos/CRM/ajax.php",
            dataType: "json",
            data: {q: request.term, t: 'C'},
            success: function (data) {
                response(data);
            }
        });
    }, minLength: 3,
    select: function (event, ui) {
        $("#txtAluno").val(ui.item.id);
        $("#txtAlunoNome").val(ui.item.value);
        getPlanos();
    }
});

function finalFind(imp) {
    var retPrint = "";
    retPrint = '?imp=0';
    if (imp === 1) {
        retPrint = '?imp=1';
    }
    if ($("#txtTipo").val() !== "null") {
        retPrint += '&tp=' + $("#txtTipo").val();
    }
    if ($("#txtLote").val() !== "null") {
        retPrint += '&lt=' + $("#txtLote").val();
    }
    if ($("#txtAluno").val() !== "0") {
        retPrint += '&al=' + $("#txtAluno").val();
    }
    if ($("#txtPlanos").val() !== "null") {
        retPrint += '&pl=' + $("#txtPlanos").val();
    }
    if ($("#txtBanco").val() !== "null") {
        retPrint += '&bc=' + $("#txtBanco").val();
    }
    if ($("#txtCarteira").val() !== "null") {
        retPrint += '&cr=' + $("#txtCarteira").val();
    }
    if ($("#txtTipoBoleto").val() !== "null") {
        retPrint += '&tb=' + $("#txtTipoBoleto").val();
    }
    if ($("#txtTipoData").val() !== "null") {
        retPrint += '&td=' + $("#txtTipoData").val();
    }
    if ($('#ckb_grupoFin').is(':checked')) {
        retPrint += '&gp=1';
    }
    if ($("#txt_dt_begin").val() !== "") {
        retPrint += "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "/");
    }
    if ($("#txt_dt_end").val() !== "") {
        retPrint += "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "/");
    }
    return "Gerar-Boletos_server_processing.php" + retPrint.replace(/\//g, "_");
}

function validar() {
    if (($("#txtTipo").val() === "1" || $("#txtTipo").val() === "2") && (!$.isNumeric($("#txtAluno").val()) || $("#txtAluno").val() === "0" || $("#txtAlunoNome").val() === "")) {
        bootbox.alert('Selecione um Cliente!');
        return false;
    } else if ($("#txtPlanos").val() === "null" && !$('#ckb_grupoFin').is(':checked')) {
        bootbox.alert('Selecione um Planos!');
        return false;
    } else if ($("#txtBanco").val() === "null") {
        bootbox.alert('Selecione um Banco!');
        return false;
    } else if ($("#txtCarteira").val() === "null") {
        bootbox.alert('Selecione uma Carteira!');
        return false;
    } else if ($("#txtDataInicio").val() === "") {
        bootbox.alert('Selecione um Vencimento!');
        return false;
    }
    return true;
}

function imprimir2() {
    var pRel = "&NomeArq=" + "Gerar Boletos" +
            "&lbl=Código|Nome|Grupo|Valor|Pago|Vencimento|Nosso Número" +
            "&siz=50|210|100|80|80|80|100" +
            "&pdf=0|8" + // Colunas do server processing que não irão aparecer no pdf
            "&ipx=1&filter=" + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + "../Modulos/Clube/" + finalFind(1).replace("?", "&");
            window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
}

function imprimir() {
    var pRel = "&NomeArq=" + "Campos Faltantes" +
            "&lbl=Código|Nome|CPF|Endereço|Bairro|Cidade|Estado|CEP" +
            "&siz=50|150|70|160|90|80|40|60" +
            "&pdf=8" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + "../Modulos/Clube/" + finalFind(1).replace("?", "&"); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
}

function AbrirBox(id) {
    let url = './../Academia/ClientesForm.php?id=' + id;
    window.open(url, '_blank');
}

var nEditing = null;
var idEditing = "";
$("body").on("click", "#tblFiliais #btnAlt", function () {
    var nRow = $(this).parents('tr')[0];
    if (nEditing !== null) {
        restoreRow(nEditing, idEditing);
    } else {
        var id = $(this).attr("data-id");
        nEditing = nRow;
        idEditing = id;
        var jqTds = $('>td', nRow);
        var idSelected = $("#txtLote option:contains(" + $(jqTds[3].innerHTML).children().remove().end().text() + ")").val();
        jqTds[3].innerHTML = '<select name="txtGrupo_' + id + '" id="txtGrupo_' + id + '" class="input-medium" style="width:100%">' + $("#txtLote").html() + '</select>';
        jqTds[8].innerHTML = '<center><a href="javascript:;" data-id="' + id + '" class="btn green-haze save" title="Salvar" style=\"padding: 0px 4px;background:#5bb75b;\"><span class="ico-checkmark"></span></a> \n\
        <a href="javascript:;" data-id="' + id + '" class="btn red-haze cancel" title="Cancelar" style=\"padding: 0px 4px;background:red;\"><span class="ico-reply"></span></a><center>';
        $("#txtGrupo_" + id).val(idSelected);
    }
});

tblFiliais.on('click', '.cancel', function (e) {
    var id = $(this).attr("data-id");
    var nRow = $(this).parents('tr')[0];
    restoreRow(nRow, id);
});

tblFiliais.on('click', '.save', function (e) {
    var id = $(this).attr("data-id");
    var nRow = $(this).parents('tr')[0];
    bootbox.confirm('Confirma o alteração do registro?', function (result) {
        if (result === true) {
            var request = $.ajax({
                type: 'POST', url: 'form/gerar-BoletosForm.php',
                data: {bntGrupo: "S", txtId: id, txtGrupo: $("#txtGrupo_" + id).val()},
                dataType: 'json'
            });
            request.done(function (data) {
                if (data.trim() === "YES") {
                    restoreRow(nRow, id);
                }
            });
            request.fail(function (data, textStatus) {
                if (data.responseText === "YES") {
                    restoreRow(nRow, id);
                }
            });
        }
    });
});

function restoreRow(nRow, id) {
    nEditing = null;
    tblFiliais.fnReloadAjax(finalFind(0));
}

function getAllSelected() {
    var notas = "P|0";
    if ($('input:checkbox:checked:not(".checkall")').length > 0) {
        $('input:checkbox:checked:not(".checkall")').each(function (index, value) {
            notas += "," + value.value;
        });
    }
    return notas;
}

function GerarBol() {
    if (validar()) {
        bootbox.confirm('Confirma a geração dos boletos selecionados?', function (result) {
            if (result === true) {
                $("#source").dialog({modal: true});
                $("#progressbar").progressbar({value: 0});
                $("#pgIni").html("0");
                $("#pgFim").html($('input:checkbox:checked:not(".checkall"):visible').length);
                processar();
            } else {
                return;
            }
        });
    }
}            

function processar() {
    var PgIni = parseInt($("#pgIni").html());
    var PgFim = parseInt($("#pgFim").html());
    if ((PgIni < PgFim) && PgFim > 0 && $('#source').dialog('isOpen')) {
        let selectedRow = $('input:checkbox:checked:not(".checkall"):visible')[PgIni];
        let IdChk = selectedRow.value;
        $("#progressbar").progressbar({value: ((PgIni * 100) / PgFim)});
        $("#pgIni").html(PgIni + 1);
        $.post("form/gerar-BoletosForm.php",{bntSave: "S", txtTipo: $("#txtTipo").val(), 
            txtBanco: $("#txtBanco").val(), txtPlanos: $("#txtPlanos").val(), txtCarteira: $("#txtCarteira").val(), 
            txtDtBegin: $("#txt_dt_begin").val().replace(/\//g, "/"), txtMens: IdChk
        }).done(function (data) {            
            if (data.trim() === "YES") {
                $(selectedRow).closest("tr").removeClass($(selectedRow).closest("tr").attr("class"));
            }
            processar();
        });
    } else {
        $("#tbRetorno tbody tr.pago input[type='checkbox']").prop('checked', false).parent('span').removeClass('checked');
        $("#tbRetorno tbody tr.pago input[type='checkbox']").attr("disabled", true);
        $("#source").dialog('close');
        restoreRow("", "");
    }
}

function sicrediMonth(a){   
    switch(a){   
        case "01": return "1";
        case "02": return "2";
        case "03": return "3";
        case "04": return "4";
        case "05": return "5";
        case "06": return "6";
        case "07": return "7";
        case "08": return "8";
        case "09": return "9";
        case "10": return "O";
        case "11": return "N";
        case "12": return "D";
        default: return "";
    }
}

function ExportarRem() {
    if (validar()) {
        if ($('input:checkbox:checked:not(".checkall"):visible').length > 0) {
            bootbox.confirm('Confirma exportar o arquivo .REM?', function (result) {
                if (result === true) {
                    $.post("form/gerar-BoletosForm.php", {bntSaveFile: "S", txtTipo: $("#txtTipo").val(), 
                        txtBanco: $("#txtBanco").val(), txtMens: getAllSelected()
                    }).done(function (data) {
                        var blob = new Blob([data]);
                        var link = document.createElement('a');
                        link.href = window.URL.createObjectURL(blob);
                        if ($("#txtBanco option:selected").attr("tipo") === "14" || $("#txtBanco option:selected").attr("tipo") === "15") {
                            link.download = $("#txtBanco option:selected").attr("cedente") + sicrediMonth(moment().format("MM")) + moment().format("DD") + ".crm";
                        } else {
                            link.download = "C" + moment().format("DDMMYY") + "A.txt";
                        }
                        link.click();
                        restoreRow("", "");
                    });
                } else {
                    return;
                }
            });
        } else {
            bootbox.alert('Selecione os itens para esta operação!');
        }
    }
}

function CancelarRem() {
    if (validar()) {
        bootbox.confirm('Confirma exportar o arquivo .REM?', function (result) {
            if (result === true) {
                $.post("form/gerar-BoletosForm.php", {bntSaveFile: "S", txtTipo: $("#txtTipo").val(), 
                    txtCancel: "S", txtBanco: $("#txtBanco").val(), txtMens: getAllSelected()
                }).done(function (data) {
                    var blob = new Blob([data]);
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = "C" + moment().format("DDMMYY") + "C.txt";
                    link.click();
                    restoreRow("", "");
                });
            } else {
                return;
            }
        });
    }
}

function ImprimirBol() {
    var boletos = getAllSelected().replace(/\|/g, "-").replace(/\,/g, "|").substring(4);
    window.open('./../../Boletos/Boleto.php?id=' + boletos + '|&crt=' + $("#txtMnContrato").val(), '_blank');
}

function EnviarEmail() {
    $("#txtSendEmail").val(getAllSelected().replace(/\|/g, "-").replace(/\,/g, "|").substring(4));
    $("#source").dialog({modal: true});
    $("#progressbar").progressbar({value: 0});
    $("#pgIni").html("0");
    $("#pgFim").html($("#txtSendEmail").val().split('|').length);
    $("#txtEmailS").val(0);
    $("#txtEmailE").val(0);
    processar_consulta();
}

function processar_consulta() {
    var PgIni = parseInt($("#pgIni").html());
    var PgFim = parseInt($("#pgFim").html());
    if ((PgIni < PgFim) && PgFim > 0 && $('#source').dialog('isOpen')) {
        var IdChk = $("#txtSendEmail").val().split('|')[PgIni];
        $("#progressbar").progressbar({value: ((PgIni * 100) / PgFim)});
        $("#pgIni").html(PgIni + 1);
        $.post("form/gerar-BoletosForm.php", {bntSendEmail: "S", txtId: IdChk}).done(function (d) {
            if (d !== null) {
                var ret = JSON.parse(d);
                if (ret[0].qtd_total !== undefined) {
                    var qtderro = ret[0].qtd_total - ret[0].qtd_sucesso;
                    trataResultado(ret[0].qtd_sucesso, qtderro);
                } else {
                    trataResultado(0, 0);
                }
            }
            processar_consulta();
        });
    } else {       
        bootbox.alert("Foram enviado(s)" + (textToNumber($("#txtEmailS").val()) + textToNumber($("#txtEmailE").val())) + " email(s):<br><br><b>* Sucesso(s):" + $("#txtEmailS").val() + "</b><br><b style='color:red'>* Falha(s):" + $("#txtEmailE").val() + "</b>");        
        restoreRow("", "");
        $("#source").dialog('close');
    }
}

function trataResultado(qtdS, qtdE) {
    $("#txtEmailS").val(parseInt($("#txtEmailS").val()) + qtdS);
    $("#txtEmailE").val(parseInt($("#txtEmailE").val()) + qtdE);
}

function AbrirBoxBoleto(id) {
    abrirTelaBox("FormBoletos.php?id=" + id, 290, 390);
}

function FecharBox() {
    restoreRow("", "");
    $("#newbox").remove();
}

function RetornoCobranca() {
    if ($("#txtMnContrato").val() === "165") {
        window.location = './../Clube/Baixar-Layout.php';        
    } else {
        window.location = './../Contas-a-pagar/Retorno-Banco.php';
    }
}

getPlanos();