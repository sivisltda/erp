var chartData;
var retPrint = "";
var i = 0;

$("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);

$(document).ready(function () {
    $("#btnfind").click(function () {
        tbLista.fnReloadAjax(finalFind(0));
    });
});

var tbLista = $("#example").dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bSort": false,
    "sAjaxSource": finalFind(0),
    "bFilter": false,
    "oLanguage": {
        "oPaginate": {
            "sFirst": "Primeiro",
            "sLast": "Último",
            "sNext": "Próximo",
            "sPrevious": "Anterior"
        }, "sEmptyTable": "Não foi encontrado nenhum resultado",
        "sInfo": "Visualização do registro _START_ ao _END_ de _TOTAL_",
        "sInfoEmpty": "Visualização do registro 0 ao 0 de 0",
        "sInfoFiltered": "(filtrado de _MAX_ registros totais)",
        "sLengthMenu": "Visualização de _MENU_ registros",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sSearch": "Pesquisar:",
        "sZeroRecords": "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers",
    "fnInitComplete": function (oSettings, json) {
    }
});

function finalFind(op) {
    var retPrint = '';
    if ($("#txtTipo").val() !== null) {
        retPrint += "&dtp=" + $("#txtTipo").val();
    }    
    if ($("#txt_dt_begin").val() !== "") {
        retPrint += "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "/");
    }
    if ($("#txt_dt_end").val() !== "") {
        retPrint += "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "/");
    }    
    if ($("#txtTitularidade").val() !== null) {
        retPrint += "&ti=" + $("#txtTitularidade").val();
    }
    if ($("#txtStatus").val() !== null) {
        retPrint += "&st=" + $("#txtStatus").val();
    }
    if ($("#txtCracha").val() !== null) {
        retPrint += "&cr=" + $("#txtCracha").val();
    }
    if ($("#txtGrupo").val() !== null) {
        retPrint += "&gr=" + $("#txtGrupo").val();
    }
    if ($("#ckbTipo").val() !== null) {
        retPrint += "&tp=" + $("#ckbTipo").val();
    }
    if ($("#ckbTrancar").val() !== "null") {
        retPrint += "&tr=" + $("#ckbTrancar").val();
    }
    if ($("#ckbTitular").val() !== "null") {
        retPrint += "&tt=" + $("#ckbTitular").val();
    }
    return "ajax/Relatorio-Categoria-Socio_server_processing.php?imp=" + op + retPrint;
}

function printPDF(imp) {
    let itens = "";
    for (let i = 0; i < $("#example").attr("campoLivre").split("|").length; ++i) {
        if ($("#example").attr("campoLivre").length > 0) {
            itens += "|100";
        }
    }
    var pRel = "&NomeArq=" + "Categorias de Sócios" +
            "&lbl=" + "Código|Nome|CPF|NIP|Telefone|Celular|Crachá|Dt.Nasc." + (imp === 2 ? "|Categoria|Titular|GRAD/ESP|Tipo Sanguíneo|Rg.|Depto.|Nome da Empresa|Cod.Titular|Nome do Titular|Cod.Resp.Legal|Nome do Resp.Legal|Validade da Carteira|OM|Profissão" + 
            ($("#example").attr("campoLivre").length > 0 ? "|" + $("#example").attr("campoLivre") : "") : "") + 
            "&siz=" + "50|195|85|75|75|80|85|55" + (imp === 2 ? "|100|50|50|100|100|100|100|50|150|50|150|70|100|100" + itens : "") +
            "&pdf=" + "40" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + "" + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + "../Modulos/Clube/" + finalFind(1).replace("?", "&"); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&imp=" + imp + "&PathArq=GenericModelPDF.php", '_blank');
}

function AbrirCli(id) {
    window.open("./../Academia/ClientesForm.php?id=" + id, '_blank');         
}