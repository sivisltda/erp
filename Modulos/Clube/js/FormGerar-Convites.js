$("#txt_dt_begin, #txt_dt_end, #txtDataInicio, #txtDataFim").mask(lang["dateMask"]);
$("#btnImprimirConvites, #bntDeleteConvites").attr("disabled", true);

listaTable();

function atendimento() {
    $("#block").toggle("slow");
    limpar();
}

function TeclaKey(event) {
    if (event.keyCode === 13) {
        $("#btnfind").click();
    }
}

function finalFind(imp) {
    var retPrint = "";
    retPrint = '?imp=0';
    if (imp === 1) {
        retPrint = '?imp=1';
    }
    if ($("#txt_dt_begin").val() !== "") {
        retPrint += "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "/");
    }
    if ($("#txt_dt_end").val() !== "") {
        retPrint += "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "/");
    }
    if ($("#txtAluno").val() !== "") {
        retPrint += "&ps=" + $("#txtAluno").val();
    }
    if ($("#txtStatus").val() !== "") {
        retPrint += '&st=' + $("#txtStatus").val();
    }
    if ($("#txtAgendamento").length > 0) {
        retPrint += '&ag=' + $("#txtAgendamento").val();
    }
    if ($("#txtBusca").val() !== "") {
        retPrint += '&txtBusca=' + $("#txtBusca").val();
    }
    return "../../Modulos/Clube/Gerar-Convites_server_processing.php" + retPrint.replace(/\//g, "_");
}

function imprimir() {
    var pRel = "&NomeArq=" + "Gerar Convites" +
            "&lbl=Código|Val.Inicio|Val.Final|Documento|Nome|Operador|PG" +
            "&siz=50|70|70|100|260|110|40" +
            "&pdf=0|8" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + "../Modulos/Clube/" + finalFind(1).replace("?", "&"); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
}

function listaTable() {
    $('#example').dataTable({
        "iDisplayLength": 20,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "bFilter": false,
        "aoColumns": [{"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false},
            {"bSortable": false}],
        "sAjaxSource": finalFind(0),
        'oLanguage': {
            'oPaginate': {
                'sFirst': "Primeiro",
                'sLast': "Último",
                'sNext': "Próximo",
                'sPrevious': "Anterior"
            }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
            'sLengthMenu': "Visualização de _MENU_ registros",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "fnDrawCallback": function () {
            refreshTotal();
        },
        "sPaginationType": "full_numbers"
    });
}

$("#txtAlunoNome").autocomplete({
    source: function (request, response) {
        $.ajax({
            url: "../../Modulos/CRM/ajax.php",
            dataType: "json",
            data: {q: request.term, t: 'C'},
            success: function (data) {
                response(data);
            }
        });
    }, minLength: 3,
    select: function (event, ui) {
        $("#txtAluno").val(ui.item.id);
        $("#txtAlunoNome").val(ui.item.value);
        getPagamentos(ui.item.id, 0);
    }
});

function limpar() {
    $('#txtId').val("");
    if ($.isNumeric($("#txtDocumento").val())) {
        $("#txtDocumento").val(leftPad((textToNumber($("#txtDocumento").val()) + 1), $("#txtDocumento").val().length));
    } else {
        $("#txtDocumento").val("");
    }
    $("#txtPagamento").val("null");
}

function leftPad(numero, comprimento) { 
    numero = numero.toString(); 
    while (numero.length < comprimento)        
        numero = "0" + numero;    
    return numero;
}

function validar() {
    if (!$.isNumeric($("#txtAluno").val()) || $("#txtAluno").val() === "0" || $("#txtAlunoNome").val() === "") {
        bootbox.alert('Selecione um Responsável!');
        return false;
    } else if ($("#txtDataInicio").val() === "") {
        bootbox.alert('Preencha o campo Data Inicio corretamente!');
        return false;
    } else if ($("#txtDataFim").val() === "") {
        bootbox.alert('Preencha o campo Data Fim corretamente!');
        return false;
    } else if ($("#txtDocumento").val() === "") {
        bootbox.alert('Preencha o campo Documento corretamente!');
        return false;
    } else if ($("#txtNome").val() === "") {
        bootbox.alert('Preencha o campo Nome corretamente!');
        return false;
    }
    return true;
}

function excluirConvite(id) {
    bootbox.confirm('Confirma a exclusão do registro?', function (result) {
        if (result === true) {
            $.post("../../Modulos/Clube/form/cupomConviteForm.php", {del: id}).done(function (data) {
                if (data.trim() === "YES") {
                    $("#btnfind").click();
                } else {
                    bootbox.alert("Erro ao excluir registro!");
                }
            });
        } else {
            return;
        }
    });
}

function AbrirBox(id) {
    $.getJSON('../../Modulos/Clube/form/cupomConviteForm.php', {id: id, ajax: 'true'}, function (j) {
        for (var i = 0; i < j.length; i++) {
            $('#txtId').val(j[i].id);
            $('#txtDataInicio').val(j[i].dt_ini);
            $("#txtDataFim").val(j[i].dt_fim);
            $("#txtDocumento").val(j[i].documento);
            $("#txtNome").val(j[i].nome_convidado);
            getPagamentos(j[i].id_fornecedor, j[i].item_venda);
        }
    });
    $("#block").show("slow");
}

function getPagamentos(pessoa, selecionado) {
    $.getJSON('../../Modulos/Clube/ajax/pagamentos.ajax.php', {txtPessoa: pessoa, ajax: 'true'}, function (j) {
        let conteudo = "<option value=\"null\">Gratuito</option>";
        for (var i = 0; i < j.length; i++) {
            conteudo += "<option value=\"" + j[i].id_item_venda + "\">" + j[i].id_item_venda + " - [" + j[i].pagamento + "] " + j[i].descricao + " (" + (j[i].total - j[i].usados) + ")</option>";
        }
        $("#txtPagamento").html(conteudo);
        if (selecionado > 0) {
            $("#txtPagamento").val(selecionado);
        }
    });
}

function refreshTotal() {
    $("table :checkbox").click(function (event) {
        if ($('input:checkbox:checked:not(".checkall")').length > 0) {
            $("#btnImprimirConvites, #bntDeleteConvites").attr("disabled", false);
        } else {
            $("#btnImprimirConvites, #bntDeleteConvites").attr("disabled", true);
        }
    });
}

$("#btnImprimirConvites").click(function () {
    let id = "";
    $('input:checkbox:checked:not(".checkall")').each(function (index) {
        id += $(this).val() + "|";
    });
    window.open("../../Modulos/Clube/cupom_convite.php?id=" + id + "&emp=" + $("#txtMnFilial").val(), '_blank');
});

$("#bntDeleteConvites").click(function () {
    let id = "";
    $('input:checkbox:checked:not(".checkall")').each(function (index) {
        id += $(this).val() + "|";
    });
    excluirConvite(id);
});

function imprimirConvite(id) {
    window.open("../../Modulos/Clube/cupom_convite.php?id=" + id + "&emp=" + $("#txtMnFilial").val(), '_blank');
}

function Iniciar() {
    if (validar()) {
        $("#source").dialog({modal: true});
        $("#progressbar").progressbar({value: 0});
        $("#pgIni").html("0");
        $("#pgFim").html($("#txtQuantidade").val());
        $("#txtTotalSucesso, #txtTotalErro").val(0);
        processar();
    }
}

function processar() {
    var PgIni = parseInt($("#pgIni").html());
    var PgFim = parseInt($("#pgFim").html());
    if ((PgIni < PgFim) && PgFim > 0 && $('#source').dialog('isOpen')) {
        $("#progressbar").progressbar({value: ((PgIni * 100) / PgFim)});
        $("#pgIni").html(PgIni + 1);
        $.post("../../Modulos/Clube/form/cupomConviteForm.php", "bntSave=S" + 
            "&txtAluno=" + $("#txtAluno").val() + 
            ($("#txtAgendamento").length > 0 ? "&txtAgendamento=" + $("#txtAgendamento").val() : "") + 
            "&" + $("#frmGerar-Convites").serialize()).done(function (data) {
            if ($.isNumeric(data.trim())) {
                limpar();
                processar();
            } else {
                $("#source").dialog('close');
                $("#btnfind").click();                    
                bootbox.alert(data.trim() + "!");                    
            }
        });
    } else {
        $("#source").dialog('close');
        getPagamentos($("#txtAluno").val(), 0);
        $("#btnfind").click();
    }
}