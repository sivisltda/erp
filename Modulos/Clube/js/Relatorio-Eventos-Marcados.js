var chartData;
var retPrint = "";
var i = 0;

$("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);

$(document).ready(function () {
    $("#btnfind").click(function () {
        tbLista.fnReloadAjax(finalFind(0));
    });
});

var tbLista = $("#example").dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bSort": false,
    "sAjaxSource": finalFind(0),
    "bFilter": false,
    "oLanguage": {
        "oPaginate": {
            "sFirst": "Primeiro",
            "sLast": "Último",
            "sNext": "Próximo",
            "sPrevious": "Anterior"
        }, "sEmptyTable": "Não foi encontrado nenhum resultado",
        "sInfo": "Visualização do registro _START_ ao _END_ de _TOTAL_",
        "sInfoEmpty": "Visualização do registro 0 ao 0 de 0",
        "sInfoFiltered": "(filtrado de _MAX_ registros totais)",
        "sLengthMenu": "Visualização de _MENU_ registros",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sSearch": "Pesquisar:",
        "sZeroRecords": "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers",
    "fnInitComplete": function (oSettings, json) {
    }
});

function finalFind(op) {
    var retPrint = '';
    if ($("#txt_dt_begin").val() !== "") {
        retPrint += "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "/");
    }
    if ($("#txt_dt_end").val() !== "") {
        retPrint += "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "/");
    }
    if ($("#txtLocacoes").val() !== null) {
        retPrint += "&lc=" + $("#txtLocacoes").val();
    }
    if ($("#ckbTipo").val() !== "null") {
        retPrint += "&tp=" + $("#ckbTipo").val();
    }
    return "ajax/Relatorio-Eventos-Marcados_server_processing.php?imp=" + op + retPrint;
}

function printPDF(imp) {
    var pRel = "&NomeArq=" + "Eventos Marcados" +
            "&lbl=" + "Código|Agendamento|Cliente/Prospect|Locações|Limite|Convidados" + 
            "&siz=" + "50|150|190|150|80|80" + 
            "&pdf=" + "10" + // Colunas do server processing que não irão aparecer no pdf
            "&filter=" + "" + //Label que irá aparecer os parametros de filtro
            "&PathArqInclude=" + "../Modulos/Clube/" + finalFind(1).replace("?", "&"); // server processing
    window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&imp=" + imp + "&PathArq=GenericModelPDF.php", '_blank');
}