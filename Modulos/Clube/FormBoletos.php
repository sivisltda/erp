<?php
include '../../Connections/configini.php';

$id = "";
$nome = "";
$plano = "";
$nosso_numero = "";
$valorMen = 0;
$valorAdd = 0;
$valorDes = 0;
$valor = 0;
$vencimento = "";
$disabled = "";

if (is_numeric($_GET['id'])) {
    $cur = odbc_exec($con, "select id_mens, razao_social, descricao, dt_inicio_mens, 
    dbo.VALOR_REAL_MENSALIDADE(id_mens) valor_mens_real, dbo.FU_VALOR_ACESSORIOS(id_plano) valor_mens_aces, valor_mens,         
    bol_nosso_numero, id_item_venda_mens, bol_data_parcela 
    from sf_vendas_planos_mensalidade inner join sf_vendas_planos on sf_vendas_planos.id_plano = sf_vendas_planos_mensalidade.id_plano_mens
    inner join sf_produtos on sf_produtos.conta_produto = sf_vendas_planos.id_prod_plano
    inner join sf_fornecedores_despesas on id_fornecedores_despesas = favorecido
    left join sf_boleto on sf_boleto.id_referencia = sf_vendas_planos_mensalidade.id_mens
    where id_mens = " . $_GET['id']);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_mens']);
        $nome = utf8_encode($RFP['razao_social']);
        $plano = utf8_encode($RFP['descricao']);
        $nosso_numero = utf8_encode($RFP['bol_nosso_numero']);
        $valorMen = escreverNumero($RFP['valor_mens']);
        $valorAdd = escreverNumero($RFP['valor_mens_aces']);
        $valorDes = escreverNumero($RFP['valor_mens'] - $RFP['valor_mens_real']);
        $valor = escreverNumero($RFP['valor_mens_real']);        
        $vencimento = escreverData(($RFP['bol_data_parcela'] != "" ? $RFP['bol_data_parcela'] : $RFP['dt_inicio_mens']));
        $disabled = is_numeric($RFP['id_item_venda_mens']) ? "disabled" : "";
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script> 
<script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<body>
    <form action="FormBoletos.php" name="frmBoletos" method="POST">
        <div class="frmhead">
            <div class="frmtext">Boleto</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont" style="height: 139px;">
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <input name="txtValorMen" id="txtValorMen" value="<?php echo $valorMen; ?>" type="hidden"/>
            <input name="txtValorAdd" id="txtValorAdd" value="<?php echo $valorAdd; ?>" type="hidden"/>
            <input name="txtValorDes" id="txtValorDes" value="<?php echo $valorDes; ?>" type="hidden"/>
            <div style="width: 38%;float: left">
                <span>Nosso Número:</span>
                <input name="txtNossoNumero" id="txtNossoNumero" type="text" class="input-medium" disabled maxlength="128" value="<?php echo $nosso_numero; ?>"/>
            </div>
            <div style="width: 30%;float: left; margin-left: 1%">
                <span>Valor:</span>
                <input name="txtValor" id="txtValor" type="text" <?php echo $disabled; ?> class="input-medium" maxlength="128" value="<?php echo $valor; ?>"/>
            </div>
            <div style="width: 30%;float: left; margin-left: 1%">
                <span>Vencimento:</span>
                <input name="txtVencimento" id="txtVencimento" type="text" <?php echo $disabled; ?> class="datepicker input-medium" maxlength="128" value="<?php echo $vencimento; ?>"/>
            </div>
            <div style="width: 100%;float: left">
                <span>Nome:</span>
                <input name="txtNome" id="txtNome" type="text" class="input-medium" disabled maxlength="128" value="<?php echo $nome; ?>"/>
            </div>
            <div style="width: 100%;float: left">
                <span>Planos:</span>
                <input name="txtPlano" id="txtPlano" type="text" class="input-medium" disabled maxlength="128" value="<?php echo $plano; ?>"/>
            </div>            
            <div style="clear:both;"></div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <button class="btn green" onClick="Salvar();" <?php echo $disabled; ?> type="button" name="bntSave" id="bntOK"><span class="ico-checkmark"></span> Gravar</button>
                <button class="btn yellow" onClick="parent.FecharBox();" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>   
    <script language="javascript">
        
        $("#txtVencimento").mask(lang["dateMask"]);
        $("#txtValor").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});    
        
        function Salvar() {
            bootbox.confirm('Confirma a alteração desse boleto?', function (result) {
                if (result === true) {
                    $.post("form/gerar-BoletosForm.php", {bntRefresh: "S", txtId: $("#txtId").val(),
                        txtValor: $("#txtValor").val(), txtValorMen: $("#txtValorMen").val(),
                        txtValorAdd: $("#txtValorAdd").val(), txtValorDes: $("#txtValorDes").val(),
                        txtVencimento: $("#txtVencimento").val()
                    }).done(function (data) {
                        parent.FecharBox();
                    });
                } else {
                    return;
                }
            });
        }        
    </script>
    <?php odbc_close($con); ?>
</body>
