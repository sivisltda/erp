<?php
include '../../Connections/configini.php';

$DescPagina = "Exceção de Vencimento";
$PageName = "Excecao-Vencimento";
$Module = "Clube";
$aColumnsX = array('Código','Data', 'Descrição');
$aColumnsP = array('10','15', '70');

if (isset($_POST["bntSave"])) {
    odbc_exec($con, "update sf_configuracao set adm_excecao_vencimento = '" . valoresCheck("ckbd1") . valoresCheck("ckbd2") . valoresCheck("ckbd3") . valoresCheck("ckbd4") . valoresCheck("ckbd5") . valoresCheck("ckbd6") . valoresCheck("ckbd7") . "';");
    echo "YES"; exit;
}

if (is_numeric($_GET['Del'])) {
    odbc_exec($con, "DELETE FROM sf_excecao_vencimento WHERE id =" . $_GET['Del']);
    echo "<script>window.top.location.href = 'Excecao-Vencimento.php'; </script>";
}

$dia_semana = "";
$cur = odbc_exec($con, "select adm_excecao_vencimento from sf_configuracao");
while ($aRow = odbc_fetch_array($cur)) {
    $dia_semana = $aRow['adm_excecao_vencimento']; 
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link rel="stylesheet" href="../../css/bootbox.css" type="text/css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>                       
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1><?php echo $Module; ?><small><?php echo $DescPagina; ?></small></h1>
                    </div>                    
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="margin-top: 15px;float:left;">
                                    <div style="float:left">
                                        <form method="POST">
                                            <button  class="button button-green btn-primary" type="button" onClick="AbrirBox(0, 0)"><span class="ico-file-4 icon-white"></span></button>
                                            <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir('tbVariaveis')" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                        </form>
                                    </div>
                                </div>
                                <div style="float: right;width: 380px;">
                                    <div style="float:left; margin-left:0.5%;">
                                        <span style="display:block;">De:</span>
                                        <input id="txt_dt_begin" type="text" class="datepicker" style="width:79px; margin-bottom:2px" value="<?php echo getData("B"); ?>">
                                    </div>            
                                    <div style="float:left; margin-left:0.5%;">
                                        <span style="display:block;">até:</span>
                                        <input id="txt_dt_end" type="text" class="datepicker" style="width:79px; margin-bottom:2px" value="<?php echo getData("E"); ?>" >
                                    </div>                                    
                                    <div style="float:left ;margin-left: 0.5%;">
                                        <div width="100%">Busca:</div>
                                        <input name="txtBusca"  id="txtBusca" maxlength="100" type="text" style="width:175px;" onkeypress="TeclaKey(event)" value="<?php echo $txtBusca; ?>"/>
                                    </div>
                                    <div style="margin-top: 15px;float:left;margin-left:0.3%;">
                                        <div style="float:left">
                                            <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Dias variáveis</div>
                    </div>
                    <div class="boxtable" style="margin-bottom: 0px;">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tbVariaveis">
                            <thead>
                                <tr>
                                    <?php for ($i = 0; $i < count($aColumnsX); $i++) { ?>
                                        <th width="<?php echo $aColumnsP[$i]; ?>%"><?php echo $aColumnsX[$i]; ?>:</th>
                                    <?php } ?>
                                    <th width="5%"><center>Ação:</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="margin-top: 15px;float:left;">
                                    <div style="float:left">
                                        <form method="POST">
                                            <button  class="button button-green btn-primary" type="button" onClick="AbrirBox(0, 1)"><span class="ico-file-4 icon-white"></span></button>
                                            <button id="btnPrint" class="button button-blue btn-primary" type="button" onclick="imprimir('tbFixo')" title="Imprimir"><span class="ico-print icon-white"></span></button>
                                        </form>
                                    </div>
                                </div>                    
                            </div>                    
                        </div>                    
                    </div>                    
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext">Dias Fixos por Mês</div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tbFixo">
                            <thead>
                                <tr>
                                    <?php for ($i = 0; $i < count($aColumnsX); $i++) { ?>
                                        <th width="<?php echo $aColumnsP[$i]; ?>%"><?php echo $aColumnsX[$i]; ?>:</th>
                                    <?php } ?>
                                    <th width="5%"><center>Ação:</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>                        
                        <div style="clear:both"></div>
                    </div>                         
                    <div style="clear:both"></div>                    
                    <div class="boxhead">
                        <div class="boxtext">Dias Fixos da Semana</div>
                    </div>
                    <div class="boxtable">
                        <form action="Excecao-Vencimento.php" method="POST">
                            <center>
                                <input id="ckbd1" name="ckbd1" type="checkbox" value="1" <?php echo (substr($dia_semana, 0, 1) == 1 ? "checked" : ""); ?>/> Domingo
                                <input id="ckbd2" name="ckbd2" type="checkbox" value="1" <?php echo (substr($dia_semana, 1, 1) == 1 ? "checked" : ""); ?>/> Segunda-feira
                                <input id="ckbd3" name="ckbd3" type="checkbox" value="1" <?php echo (substr($dia_semana, 2, 1) == 1 ? "checked" : ""); ?>/> Terça-feira
                                <input id="ckbd4" name="ckbd4" type="checkbox" value="1" <?php echo (substr($dia_semana, 3, 1) == 1 ? "checked" : ""); ?>/> Quarta-feira
                                <input id="ckbd5" name="ckbd5" type="checkbox" value="1" <?php echo (substr($dia_semana, 4, 1) == 1 ? "checked" : ""); ?>/> Quinta-feira
                                <input id="ckbd6" name="ckbd6" type="checkbox" value="1" <?php echo (substr($dia_semana, 5, 1) == 1 ? "checked" : ""); ?>/> Sexta-feira
                                <input id="ckbd7" name="ckbd7" type="checkbox" value="1" <?php echo (substr($dia_semana, 6, 1) == 1 ? "checked" : ""); ?>/> Sábado
                                <br><br>
                                <button class="button" style="border:0; background:#006568" type="button" id="bntSave" name="bntSave"><span class="ico-ok icon-white"></span> Salvar</button>                      
                            </center>                              
                        </form>                                                  
                        <div style="clear:both"></div>                        
                    </div>                                                       
                </div>
            </div>
        </div>       
        <div class="dialog" id="source" title="Source"></div>        
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type="text/javascript" src="../../js/util.js"></script>     
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>        
        <script type="text/javascript">
            
            function TeclaKey(event) {
                if (event.keyCode === 13) {
                    $("#btnfind").click();
                }
            }

            function finalFind(imp, table) {
                var retPrint = "";
                retPrint = '?imp=0';
                if (imp === 1) {
                    retPrint = '?imp=1';
                }
                if ($("#txtBusca").val() !== "" && table === "tbVariaveis") {
                    retPrint += '&txtBusca=' + $("#txtBusca").val();
                }
                if ($("#txt_dt_begin").val() !== "" && table === "tbVariaveis") {
                    retPrint += "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "/");
                }
                if ($("#txt_dt_end").val() !== "" && table === "tbVariaveis") {
                    retPrint += "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "/");
                }
                retPrint += "&tip=" + (table === "tbVariaveis" ? 0 : 1);                
                return retPrint.replace(/\//g, "_");
            }

            function AbrirBox(id, tp) {
                if (id > 0) {
                    var myId = "&id=" + id;
                } else {
                    var myId = "";
                }
                abrirTelaBox("FormExcecao-Vencimento.php?tp=" + tp + myId, 260, 350);
            }
            
            function FecharBox() {
                var oTable = $('#tbVariaveis').dataTable();
                oTable.fnDraw(false);
                var oTable2 = $('#tbFixo').dataTable();
                oTable2.fnDraw(false);
                $("#newbox").remove();
            }

            function imprimir(table) {
                var pRel = "&NomeArq=" + "Exceção de Vencimentos" +
                        "&lbl=Código|Data|Descrição" +
                        "&siz=100|150|450" +
                        "&pdf=3" + // Colunas do server processing que não irão aparecer no pdf
                        "&filter=Período de " + $("#txt_dt_begin").val() + " até " + $("#txt_dt_end").val() + //Label que irá aparecer os parametros de filtro
                        "&PathArqInclude=" + "../Modulos/Clube/Excecao-Vencimento_server_processing.php" + finalFind(1, table).replace("?", "&"); // server processing
                window.open("../../util/ImpressaoPdf.php?id=" + pRel + "&tpImp=I&PathArq=GenericModelPDF.php", '_blank');
            }
            
            $(document).ready(function () {
                listaTable("tbVariaveis");
                $("#btnfind").click(function () {
                    $("#tbVariaveis").dataTable().fnDestroy();
                    listaTable("tbVariaveis");
                });                
                listaTable("tbFixo");
            });
            
            function listaTable(table) {
                $("#" + table).dataTable({
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "bFilter": false,
                    "aoColumns": [
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false},
                        {"bSortable": false}],
                    "sAjaxSource": "Excecao-Vencimento_server_processing.php" + finalFind(0, table),
                    'oLanguage': {
                        'oPaginate': {
                            'sFirst': "Primeiro",
                            'sLast': "Último",
                            'sNext': "Próximo",
                            'sPrevious': "Anterior"
                        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                        'sLengthMenu': "Visualização de _MENU_ registros",
                        'sLoadingRecords': "Carregando...",
                        'sProcessing': "Processando...",
                        'sSearch': "Pesquisar:",
                        'sZeroRecords': "Não foi encontrado nenhum resultado"},
                    "sPaginationType": "full_numbers"
                });
            }
            
            $('#bntSave').click(function () {
                $.post("Excecao-Vencimento.php", "bntSave=S&" + $("#bntSave").closest("form").serialize()).done(function (data) {
                    if (data.trim() === "YES") {
                        bootbox.alert("Salvo com Sucesso!");
                    } else {
                        bootbox.alert(data + "!");
                    }           
                });
            });
        </script>         
        <?php odbc_close($con); ?>
    </body>
</html>
