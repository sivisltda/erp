<span id="block" style="display: none;width: 100%;">
    <form id="frmGerar-Convites" method="POST">
        <div class="boxhead">
            <div class="boxtext">Convites</div>
        </div>        
        <div class="boxtable" style="background: #F6F6F6;">
            <input name="txtId" id="txtId" value="" type="hidden"/>        
            <div style="width: 100%;float: left">
                <div style="width:10%; float:left;">
                    <span>Validade:</span>
                    <input name="txtDataInicio" id="txtDataInicio" class="datepicker inputCenter" type="text" value="">
                </div>
                <div style="width:10%; float:left;margin-left:1%;">
                    <span>Até:</span>
                    <input name="txtDataFim" id="txtDataFim" class="datepicker inputCenter" type="text" value="">
                </div>
                <div style="width:10%; float:left;margin-left:1%;">
                    <span>Doc.:</span>
                    <input name="txtDocumento" id="txtDocumento" type="text" class="input-medium" maxlength="128" value=""/>                    
                </div>
                <div style="width: 25%;float: left;margin-left:1%;">
                    <span>Nome:</span>
                    <input name="txtNome" id="txtNome" type="text" class="input-medium" maxlength="128" value=""/>
                </div>
                <div style="width: 20%;float: left;margin-left:1%;">
                    <span>Pagamento:</span>
                    <select name="txtPagamento" id="txtPagamento" class="input-medium" style="width:100%">
                        <option value="null">Gratuito</option>                                           
                    </select>                    
                </div>
                <div style="width:10%; float:left;margin-left:1%;">
                    <span>Quantidade.:</span>
                    <input name="txtQuantidade" id="txtQuantidade" type="text" class="input-medium" maxlength="128" value="1"/>                    
                </div>                
                <div style="width: 9%; float: left; margin-left:1%; margin-top: 11px;">
                    <center>                        
                        <button class="btn btn-success" type="button" id="btnSave" name="btnSave" onclick="Iniciar();"><span class="ico-checkmark"></span></button>
                        <button class="btn yellow" type="button" id="btnCancelar" name="btnCancelar" onclick="limpar();atendimento();"><span class="ico-reply"></span></button>                    
                    </center>                    
                </div>                            
            </div>
            <div style="clear:both"></div>            
        </div>
    </form>
</span>
<div class="boxhead">
    <div class="boxtext">
    <button onclick="selectConvite(0,'','','',0,'');" class="button button-yellow" style="float: left;margin-top: 2px;margin-right: 4px;" type="button" title="Convites"><span class="ico-plus icon-white"></span></button>
    <span id="lblGerarConvites">Gerar Convites</span>
    <?php if ($ckb_clb_excluir_convites_ > 0) { ?>
    <button id="bntDeleteConvites" class="button button-red" style="float: right;margin-top: 2px;margin-right: 4px;" type="button" name="bntDeleteConvites" title="Excluir Convites"><span class="ico-remove icon-white"></span></button>    
    <?php } ?>
    <button id="btnImprimirConvites" class="button button-turquoise" style="float: right;margin-top: 2px;margin-right: 4px;" type="button" name="btnImprimirConvites" title="Imprimir Convites"><span class="ico-print icon-white"></span></button>
    </div>                        
</div>
<div class="boxtable">
    <table class="table" cellpadding="0" cellspacing="0" width="100%" id="example">
        <thead>                                
            <tr>
                <th width="4%" style="top:0"><input id="checkAll" type="checkbox" class="checkall"/></th>                
                <th width="8%">Código</th>
                <th width="10%">Val.Inicio</th>
                <th width="10%">Val.Final</th>
                <th width="14%">Documento</th>
                <th width="30%">Nome</th>
                <th width="12%">Operador</th>
                <th width="7%">PG</th>
                <th width="5%"><center>Ação:</center></th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
    <div style="clear:both"></div>
</div>
<div class="dialog" id="source" title="Processando" style="display: none;">
    <div id="progressbar"></div>
    <div style="display: flex;align-items: center;justify-content: center;margin-top: 20px;">
        <div id="pgIni">0</div><div style="padding: 5px;">até</div><div id="pgFim">0</div>
    </div>
</div>
