<?php
include '../../Connections/configini.php';

$DescPagina = "Gerar Boletos";
$PageName = "Gerar-Boletos";
$Module = "Clube";
$aColumnsX = array('Código', 'Nome', 'Grupo', 'Valor', 'Pago', 'Vencimento', 'Nosso Número');
$aColumnsP = array('8', '26', '12', '8', '8', '12', '17');
$DateBegin = escreverDataSoma(getData("B"), " +1 month");
$DateEnd = escreverDataSoma(getData("E"), " +1 month");

$grupoDep = 0;
$grupoFin = 0;
$cur = odbc_exec($con, "select FIN_GRUPO_BOLETO,FIN_GRUPO_BOLETO_MOD from sf_configuracao");
while ($aRow = odbc_fetch_array($cur)) {
    $grupoDep = $aRow['FIN_GRUPO_BOLETO'];
    $grupoFin = $aRow['FIN_GRUPO_BOLETO_MOD'];    
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/bootbox.css"/>        
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>  
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <style type="text/css">
            .mynewtxt {
                float: left;
                margin-left: 5px;
                font-size: 11px;
                margin-top: -4px;
            }
        </style>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1><?php echo ($mdl_aca_ == 0 ? $Module : "Financeiro"); ?><small><?php echo $DescPagina; ?></small></h1>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="boxfilter block">
                                <div style="float: right;width: 100%;">
                                    <div style="width:3%; float:left; margin-top: 15px;">
                                        <div style="float:left">
                                            <div class="btn-group">
                                                <button class="button button-green btn-primary dropdown-toggle" style="width: 36px;" data-toggle="dropdown"><span style="margin-top: 8px;" class="caret icon-white"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a onclick="GerarBol()">Gerar Boletos</a></li>
                                                    <li><a onclick="ExportarRem()">Exportar .REM</a></li>
                                                    <li><a onclick="ImprimirBol()">Imprimir Boletos</a></li>
                                                    <li><a onclick="EnviarEmail()">Enviar por Email</a></li>
                                                    <li><a onclick="RetornoCobranca()">Retorno de Cobrança</a></li>
                                                    <?php if ($ckb_clb_cboleto_ > 0) { ?>
                                                    <li><a onclick="CancelarRem()">Cancelar .REM</a></li>                                                    
                                                    <?php } ?>
                                                </ul>
                                            </div>                                            
                                        </div>
                                    </div>                                    
                                    <div style="width:8%; float:left;">
                                        <span>Tipo:</span>
                                        <select name="txtTipo" id="txtTipo" class="input-medium" style="width:100%">
                                            <option value="0">Lote</option>
                                            <option value="1">Individual</option>
                                            <?php if ($grupoDep > 0) { ?>
                                            <option value="2">Dependente</option>
                                            <?php } ?>
                                        </select>
                                    </div>    
                                    <div style="width:18%; float:left; margin-left:0.5%;">
                                        <span>Grupo de Cliente:</span>
                                        <select name="txtLote" id="txtLote" class="input-medium" style="width:100%">
                                            <option value="null">Selecione</option>                                            
                                            <?php
                                            $cur = odbc_exec($con, "select id_grupo,descricao_grupo from sf_grupo_cliente where tipo_grupo = 'C' and inativo_grupo = 0 order by descricao_grupo") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id_grupo']; ?>"><?php echo utf8_encode($RFP['descricao_grupo']); ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>                                    
                                    <div style="width:18%; float:left; margin-left:0.5%; display: none;">
                                        <span>Nome:</span>
                                        <input name="txtAluno" id="txtAluno" value="0" type="hidden"/>
                                        <input name="txtAlunoNome" id="txtAlunoNome" value="" type="text" class="inputbox" style="width:100%" />
                                    </div>                                       
                                    <div style="width:15%; float:left; margin-left:0.5%;">
                                        <span>Planos:</span>
                                        <select name="txtPlanos" id="txtPlanos" style="width:100%" <?php echo ($mdl_seg_ > 0 ? "multiple=\"multiple\" class=\"select\"" : ""); ?>></select>
                                    </div>                                    
                                    <div style="width:11%; float:left; margin-left:0.5%;">
                                        <span>Banco:</span>
                                        <select name="txtBanco" id="txtBanco" class="input-medium" style="width:100%">
                                            <option value="null" tipo="null" cedente="null">Selecione</option>
                                            <?php $cur = odbc_exec($con, "select id_bancos, tipo_bol, cedente, razao_social from sf_bancos where ativo = 0 order by razao_social") or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) { ?>
                                                <option value="<?php echo $RFP['id_bancos']; ?>" tipo="<?php echo $RFP['tipo_bol']; ?>" cedente="<?php echo $RFP['cedente']; ?>"><?php echo utf8_encode($RFP['razao_social']); ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div style="width:10%; float:left; margin-left:0.5%;">
                                        <span>Carteira:</span>
                                        <select name="txtCarteira" id="txtCarteira" class="input-medium" style="width:100%">
                                            <option value="null">Selecione</option>
                                        </select>
                                    </div> 
                                    <div style="width:10%; float:left; margin-left:0.5%;">
                                        <span>Tipo:</span>
                                        <select name="txtTipoBoleto" id="txtTipoBoleto" class="input-medium" style="width:100%">
                                            <option value="0">Todos</option>
                                            <option value="1">Gerados</option>
                                            <option value="2">Não gerados</option>
                                            <option value="3">Exportados</option>
                                            <option value="4">Não exportados</option>                                         
                                            <option value="5">Enviado</option>                                            
                                            <option value="6">Não enviado</option>                                            
                                            <option value="7">Enviado c/ erro</option>
                                            <option value="8">Liquidados</option>
                                            <option value="9">Não liquidados</option>
                                        </select>
                                    </div>
                                    <div style="width:5%; float:left; margin-left:0.5%;">
                                        <span>Data de:</span>
                                        <select name="txtTipoData" id="txtTipoData" class="input-medium" style="width:100%">
                                            <option value="0">Venc.</option>
                                            <option value="1">Cad.</option>
                                        </select>
                                    </div>                                    
                                    <div style="width: 6%; float:left; margin-left:0.5%;">
                                        <span style="display:block;">de:</span>
                                        <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>">
                                    </div>                                     
                                    <div style="width: 6%; float:left; margin-left:0.5%;">
                                        <span style="display:block;">até:</span>
                                        <input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" disabled>
                                    </div>                                     
                                    <div style="width:3%; float:left; margin-left:0.5%; margin-top: 15px;">
                                        <div style="float:left">
                                            <button class="button button-turquoise btn-primary" type="button" name="btnfind" id="btnfind" title="Buscar"><span class="ico-search icon-white"></span></button>                                                                                     
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>                   
                    <div class="boxhead">
                        <div class="boxtext">
                            <div class="mynewtxt"><input id="ckb_grupoFin" value="1" type="checkbox" class="input-medium" <?php echo ($grupoFin == "1" ? "checked" :""); ?>><span style="vertical-align: sub;">Agrupar boleto</span></div>
                            <?php echo $DescPagina; ?>
                            <button id="btnImprimirConvites" class="button button-red" onclick="imprimir();" style="float: right;margin-top: 2px;margin-right: 4px;" type="button" title="Cadastros com Campos Faltantes"><span class="ico-print icon-white"></span></button>                                                                
                            <button id="btnImprimirConvites" class="button button-turquoise" onclick="imprimir2();" style="float: right;margin-top: 2px;margin-right: 4px;" type="button" title="Imprimir"><span class="ico-print icon-white"></span></button>                                                                
                        </div>                        
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblFiliais">
                            <thead>                                
                                <tr>
                                    <th width="4%" style="top:0"><input id="checkAll" type="checkbox" class="checkall"/></th>
                                    <?php for ($i = 0; $i < count($aColumnsX); $i++) { ?>
                                        <th width="<?php echo $aColumnsP[$i]; ?>%"><?php echo $aColumnsX[$i]; ?>:</th>
                                    <?php } ?>
                                    <th width="5%"><center>Ação:</center></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>  
        <input name="txtEmailS" id="txtEmailS" value="" type="hidden"/>
        <input name="txtEmailE" id="txtEmailE" value="" type="hidden"/>
        <input name="txtSendEmail" id="txtSendEmail" value="" type="hidden"/>        
        <div class="dialog" id="source" style="display: none;" title="Processando">
            <div id="progressbar"></div>
            <div style="display: flex;align-items: center;justify-content: center;margin-top: 20px;">
                <div id="pgIni">0</div><div style="padding: 5px;">até</div><div id="pgFim">0</div>            
            </div>            
        </div>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type="text/javascript" src="../../js/moment.min.js"></script>        
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/fnReloadAjax.js'></script>        
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>     
        <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>  
        <script type='text/javascript' src='js/FormGerar-Boletos.js'></script>                
        <?php odbc_close($con); ?>
    </body>
</html>
