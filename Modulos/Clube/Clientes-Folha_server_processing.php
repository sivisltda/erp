<?php

require_once(__DIR__ . '/../../Connections/configini.php');
$aColumns = array('id_fornecedores_despesas', 'razao_social', 'cnpj', 'nome_cat', 'descricao', 'tempo_producao');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhere = "";
$sWhereX = "";
$sOrder = " ORDER BY " . $aColumns[1] . " asc ";
$sOrder2 = " ORDER BY " . $aColumns[1] . " asc, total asc ";
$sLimit = 20;
$imprimir = 0;
$valor = 0;
$campos_livres_1 = 0;
$campos_livres_2 = 0;

$cur = odbc_exec($con, "select id_campo from sf_configuracao_campos_livres where id_campo = 1");
while ($RFP = odbc_fetch_array($cur)) {
    $campos_livres_1 = utf8_encode($RFP["id_campo"]);
}

$cur = odbc_exec($con, "select id_campo from sf_configuracao_campos_livres where id_campo = 2");
while ($RFP = odbc_fetch_array($cur)) {
    $campos_livres_2 = utf8_encode($RFP["id_campo"]);
}

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i])], 0) . " " . $_GET['sSortDir_' . $i] . ", ";
            $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i])], 1) . " " . $_GET['sSortDir_' . $i] . ", ";
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY " . $aColumns[1] . " asc";
        $sOrder2 = " ORDER BY " . $aColumns[1] . " asc, total asc";
    }
}

if ($_GET['dti'] != '' && $_GET['dtf'] != '') {
    $sWhereX .= " and dt_inicio_mens between " . valoresData2($_GET['dti']) . " and " . valoresData2($_GET['dtf']);
}  

if (isset($_GET['pln']) && $_GET['pln'] != 'null') {
    $sWhereX .= " and conta_movimento in (" . $_GET['pln'] . ")";
}

if (isset($_GET['ctg']) && $_GET['ctg'] != 'null') {
    $sWhereX .= " and titularidade in (" . $_GET['ctg'] . ")";
}

if ($_GET['txtBusca'] != "") {
    $sWhereX .= " and (razao_social like '%" . $_GET['txtBusca'] . "%')";
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($i == 0) {
        $colunas = $aColumns[$i];
    } else {
        $colunas = $colunas . "," . $aColumns[$i];
    }
}

$table = "--titular do plano que não seja dependente
select id_fornecedores_despesas, razao_social, cnpj, dt_inicio_mens,
gp.id_contas_movimento tempo_producao, gp.descricao, dbo.VALOR_REAL_MENSALIDADE(id_mens) valor, nome_cat
from sf_fornecedores_despesas c
inner join sf_vendas_planos vp on c.id_fornecedores_despesas = vp.favorecido
inner join sf_produtos p on p.conta_produto = vp.id_prod_plano
inner join sf_contas_movimento gp on gp.id_contas_movimento = p.conta_movimento
inner join sf_vendas_planos_mensalidade vm on vp.id_plano = vm.id_plano_mens
left join sf_categoria_socio ct on ct.id_cat = c.titularidade
where id_fornecedores_despesas > 0 and vp.dt_cancelamento is null and dt_pagamento_mens is null
and id_fornecedores_despesas not in (select id_dependente from sf_fornecedores_despesas_dependentes) 
and id_fornecedores_despesas not in (select id_dependente from sf_fornecedores_despesas_responsavel)
" . $sWhereX . "
--dependente
union all
select id_fornecedores_despesas, razao_social, cnpj, dt_inicio_mens,
gp.id_contas_movimento, gp.descricao, dbo.VALOR_REAL_MENSALIDADE(id_mens) valor, nome_cat
from sf_fornecedores_despesas c
inner join sf_fornecedores_despesas_dependentes cd on cd.id_titular = c.id_fornecedores_despesas
inner join sf_vendas_planos vp on cd.id_dependente = vp.favorecido
inner join sf_produtos p on p.conta_produto = vp.id_prod_plano
inner join sf_contas_movimento gp on gp.id_contas_movimento = p.conta_movimento
inner join sf_vendas_planos_mensalidade vm on vp.id_plano = vm.id_plano_mens
left join sf_categoria_socio ct on ct.id_cat = c.titularidade
where id_fornecedores_despesas > 0 and vp.dt_cancelamento is null and dt_pagamento_mens is null " . $sWhereX . "
--planos dos responsaveis
union all
select id_fornecedores_despesas, razao_social, cnpj, dt_inicio_mens,
gp.id_contas_movimento, gp.descricao, dbo.VALOR_REAL_MENSALIDADE(id_mens) valor, nome_cat
from sf_fornecedores_despesas c
inner join sf_fornecedores_despesas_responsavel cr on cr.id_responsavel = c.id_fornecedores_despesas
inner join sf_vendas_planos vp on cr.id_dependente = vp.favorecido
inner join sf_produtos p on p.conta_produto = vp.id_prod_plano
inner join sf_contas_movimento gp on gp.id_contas_movimento = p.conta_movimento
inner join sf_vendas_planos_mensalidade vm on vp.id_plano = vm.id_plano_mens
left join sf_categoria_socio ct on ct.id_cat = c.titularidade
where id_fornecedores_despesas > 0 and vp.dt_cancelamento is null and dt_pagamento_mens is null " . $sWhereX . "
--planos dos dependentes dos responsaveis
union all
select id_fornecedores_despesas, razao_social, cnpj, dt_inicio_mens,
gp.id_contas_movimento, gp.descricao, dbo.VALOR_REAL_MENSALIDADE(id_mens) valor, nome_cat
from sf_fornecedores_despesas c
inner join sf_fornecedores_despesas_responsavel cr on cr.id_responsavel = c.id_fornecedores_despesas
inner join sf_fornecedores_despesas_dependentes cd on cd.id_titular = cr.id_dependente
inner join sf_vendas_planos vp on cd.id_dependente = vp.favorecido
inner join sf_produtos p on p.conta_produto = vp.id_prod_plano
inner join sf_contas_movimento gp on gp.id_contas_movimento = p.conta_movimento
inner join sf_vendas_planos_mensalidade vm on vp.id_plano = vm.id_plano_mens
left join sf_categoria_socio ct on ct.id_cat = c.titularidade
where id_fornecedores_despesas > 0 and vp.dt_cancelamento is null and dt_pagamento_mens is null " . $sWhereX . "
) as x group by id_fornecedores_despesas, razao_social, cnpj, month(dt_inicio_mens), year(dt_inicio_mens), tempo_producao, descricao, nome_cat";    

$sQuery = "set dateformat dmy;SELECT COUNT(id_fornecedores_despesas) total, sum(valor) valor from (select " . $colunas . ",month(dt_inicio_mens) mes, year(dt_inicio_mens) ano, sum(valor) valor from (" . $table . ") as y";
//echo $sQuery; exit;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
    $iFilteredTotal = $RFP['total'];
    $valor = $RFP['valor'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

$i = 0;
$sQuery1 = "set dateformat dmy;SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row, " . $colunas . ",month(dt_inicio_mens) mes, year(dt_inicio_mens) ano, sum(valor) total,
(select conteudo_campo from sf_fornecedores_despesas_campos_livres where campos_campos = " . $campos_livres_1 . " and fornecedores_campos = id_fornecedores_despesas) campo_livre_1,
(select conteudo_campo from sf_fornecedores_despesas_campos_livres where campos_campos = " . $campos_livres_2 . " and fornecedores_campos = id_fornecedores_despesas) campo_livre_2,
(select grupo_observacao from sf_contas_movimento gpx where gpx.id_contas_movimento = tempo_producao) grupo_observacao
 from (" . $table . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder2;
//echo $sQuery1; exit;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $row[] = "<center>" . utf8_encode($aRow[$aColumns[0]]) . "</center>" . ($i == 0 ? "<input type='hidden' name='totparc' id='totparc' value='" . escreverNumero($valor, 1) . "'/>" : "");
    $row[] = "<a href=\"../Academia/ClientesForm.php?id=" . utf8_encode($aRow[$aColumns[0]]) . "\" target=\"_blank\">" . utf8_encode($aRow[$aColumns[1]]) . "</a>";
    $row[] = "<center>" . utf8_encode($aRow[$aColumns[2]]) . "</center>";
    $row[] = "<center>" . $aRow["mes"] . "</center>";
    $row[] = "<center>" . $aRow["ano"] . "</center>";
    $row[] = "<center>" . utf8_encode($aRow["grupo_observacao"]) . "</center>";
    $row[] = utf8_encode($aRow[$aColumns[4]]);
    $row[] = escreverNumero($aRow["total"], 1);    
    $row[] = utf8_encode($aRow[$aColumns[3]]);
    if (isset($_GET['tpImp']) && $_GET['tpImp'] == "E") {
        $row[] = utf8_encode($aRow["campo_livre_1"]);
        $row[] = utf8_encode($aRow["campo_livre_2"]);
    }
    $output['aaData'][] = $row;
    $i++;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);