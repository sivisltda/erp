<?php include "./../../Connections/configini.php";

if (is_numeric($_GET["id"])) {
    $query = "select id_fornecedores_despesas, razao_social, data_nascimento, dt_cadastro, Observacao,
    endereco, bairro, cidade_nome cidade, estado_sigla estado, cep,  fornecedores_status,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas = A.id_fornecedores_despesas) telefone,
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = A.id_fornecedores_despesas) telefone_celular,
    inscricao_estadual, cnpj, titular, 
    (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = A.id_fornecedores_despesas) email, 
    (select max(dbo.FU_PLANO_FIM(id_plano)) from sf_vendas_planos where dt_cancelamento is null and favorecido = A.id_fornecedores_despesas) validade,
    nip, depto, nome_cat, gran_esp, om, tel_om
    from sf_fornecedores_despesas A left join tb_estados on estado = estado_codigo 
    left join tb_cidades on cidade = cidade_codigo left join sf_categoria_socio on id_cat = A.titularidade
    where id_fornecedores_despesas = " . $_GET["id"];
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP["id_fornecedores_despesas"]);
        $nome = utf8_encode($RFP["razao_social"]);
        $dataNasc = escreverData($RFP["data_nascimento"]);
        $dataIni = escreverData($RFP["dt_cadastro"]);
        $obs = utf8_encode($RFP["Observacao"]);
        $endereco = utf8_encode($RFP["endereco"]);
        $bairro = utf8_encode($RFP["bairro"]);
        $uf = utf8_encode($RFP["estado"]);
        $cidade = utf8_encode($RFP["cidade"]);
        $cep = utf8_encode($RFP["cep"]);
        $status = utf8_encode($RFP["fornecedores_status"]);     
        $telRes = utf8_encode($RFP["telefone"]);
        $telCel = utf8_encode($RFP["telefone_celular"]);
        $Rg = utf8_encode($RFP["inscricao_estadual"]);
        $cpf = utf8_encode($RFP["cnpj"]);
        $local = ($RFP["titular"] == "1" ? "TITULAR" : "DEPENDENTE");
        $email = utf8_encode($RFP["email"]);
        $nip = utf8_encode($RFP["nip"]);
        $setor = utf8_encode($RFP["depto"]);
        $tipo = utf8_encode($RFP["nome_cat"]);
        $validade = escreverData($RFP["validade"]);
        $graduacao = utf8_encode($RFP["gran_esp"]);
        $om = utf8_encode($RFP["om"]);
        $telom = utf8_encode($RFP["tel_om"]);
    }
    $dependentes = array();
    $query = "select id_fornecedores_despesas, razao_social,data_nascimento,dt_cadastro,
    data_inclusao,(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = A.id_fornecedores_despesas) email, 
    nome_cat,fornecedores_status,Observacao
    from sf_fornecedores_despesas_dependentes inner join sf_fornecedores_despesas A on id_fornecedores_despesas = id_dependente
    left join sf_categoria_socio on id_cat = A.titularidade where id_titular = " . $_GET["id"];
    $cur = odbc_exec($con, $query);
    while ($RFP = odbc_fetch_array($cur)) {
        $row = array();
        $row["id"] = utf8_encode($RFP["id_fornecedores_despesas"]);
        $row["razao_social"] = utf8_encode($RFP["razao_social"]);
        $row["data_nascimento"] = escreverData($RFP["data_nascimento"]);        
        $row["dt_cadastro"] = escreverData($RFP["dt_cadastro"]);        
        $row["data_inclusao"] = escreverData($RFP["data_inclusao"]);        
        $row["email"] = utf8_encode($RFP["email"]);        
        $row["nome_cat"] = utf8_encode($RFP["nome_cat"]);        
        $row["fornecedores_status"] = utf8_encode($RFP["fornecedores_status"]);        
        $row["Observacao"] = utf8_encode($RFP["Observacao"]);        
        $dependentes[] = $row;
    }
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
            body {
                margin:0px;
                padding:0px;
            }
            .titulo {
                font: 14px Calibri;
                font-weight: Bold;
                padding-left: 5px;
                background-color: lightgrey;
            }
        </style>
    </head>
    <body>
        <div id="main" style="width: 700px; overflow:hidden;">
            <table width="100%" border="1" cellpadding="2" cellspacing="0">
                <tr>
                    <td width="500px" rowspan="2">
                        <table width="100%" cellpadding="0" border="0" cellspacing="0" style="border:0px solid white;padding-bottom: 15px !important;">
                            <tr>
                                <td width="200px">
                                    <?php if (file_exists("./../Pessoas/" . $_SESSION["contrato"] . "/Empresa/logo_" . $filial_orc . ".jpg")) { ?>
                                        <div style="line-height: 5px;">&nbsp;</div><img src ="./../Pessoas/<?php echo $_SESSION["contrato"]; ?>/Empresa/logo_<?php echo $filial_orc; ?>.jpg" width="150" height="50" align="middle" />
                                    <?php } else { ?>
                                        <div style="line-height: 5px;">&nbsp;</div>
                                    <?php } ?>
                                </td>                        
                                <td width="265px" >
                                    <div style="text-align:right;font-size: 8px;"><?php echo $_SESSION["cabecalho"]; ?></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="200px" colspan="2" align="center" style="line-height: 40px;font-size:17px;"><b>Relatório NIP</b></td>
                </tr>
                <tr>
                    <td style="padding-left:5px">Data:</td>
                    <td align="center" ><?php echo getData("T"); ?></td>
                </tr>
            </table>
            <br><br>
            <table width="700px" style="font-size: 10px;line-height: 20px;">
                <tr style="vertical-align: middle;line-height: 20px;">
                    <td class="titulo" style="width: 70px;">Proprietário:</td>
                    <td class="titulo" style="width: 80px;">Matrícula:</td>
                    <td class="titulo" style="width: 60px;"><?php echo $id; ?></td>
                    <td class="titulo" style="width: 120px;">Nome:</td>
                    <td class="titulo" style="width: 370px;" colspan="4"><?php echo $nome; ?></td>
                </tr>
                <tr style="vertical-align: middle;line-height: 20px;">
                    <td><b>Nascimento:</b></td>
                    <td align="center"><?php echo $dataNasc; ?></td>
                    <td><b>Dt. Início:</b></td>
                    <td align="center"><?php echo $dataIni; ?></td>
                    <td style="width: 70px;"><b>Observação:</b></td>
                    <td style="width: 300px;" colspan="3"><?php echo $obs; ?></td>
                </tr>
                <tr style="vertical-align: middle;line-height: 20px;">
                    <td><b>End:</b></td>
                    <td colspan="3"><?php echo $endereco; ?></td>                  
                    <td><b>Bairro:</b></td>
                    <td><?php echo $bairro; ?></td>
                    <td style="width: 70px;"><b>UF:</b></td>
                    <td style="width: 130px;"><?php echo $uf; ?></td>
                </tr>
                <tr style="vertical-align: middle;line-height: 20px;">
                    <td><b>Cidade:</b></td>
                    <td colspan="3"><?php echo $cidade; ?></td>
                    <td><b>CEP:</b></td>
                    <td align="center"><?php echo $cep; ?></td>                    
                    <td><b>Situação:</b></td>
                    <td><?php echo $status; ?></td>
                </tr>
                <tr style="vertical-align: middle;line-height: 20px;">
                    <td><b>Tel. Res:</b></td>
                    <td><?php echo $telRes; ?></td>
                    <td><b>Celular:</b></td>
                    <td><?php echo $telCel; ?></td>
                    <td><b>RG:</b></td>
                    <td><?php echo $Rg; ?></td>
                    <td><b>CPF:</b></td>
                    <td><?php echo $cpf; ?></td>
                </tr>
                <tr style="vertical-align: middle;line-height: 20px;">
                    <td><b>Local:</b></td>
                    <td colspan="3"><?php echo $local; ?></td>               
                    <td><b>E-mail:</b></td>
                    <td colspan="3"><?php echo $email; ?></td>
                </tr>
                <tr style="vertical-align: middle;line-height: 20px;">
                    <td><b>Nip/Siape:</b></td>
                    <td><?php echo $nip; ?></td>
                    <td><b>Setor:</b></td>
                    <td><?php echo $setor; ?></td>
                    <td><b>Tipo:</b></td>
                    <td><?php echo $tipo; ?></td>
                    <td><b>Validade:</b></td>
                    <td><?php echo $validade; ?></td>                    
                </tr>
                <tr style="vertical-align: middle;line-height: 20px;">
                    <td><b>Graduação:</b></td>
                    <td><?php echo $graduacao; ?></td>
                    <td><b>OM:</b></td>
                    <td><?php echo $om; ?></td>
                    <td><b>Tel. OM:</b></td>
                    <td><?php echo $telom; ?></td>
                    <td></td>
                    <td></td>
                </tr>                
            </table> 
            <?php foreach ($dependentes as $row) { ?>
            <br><br>
            <table width="700px" style="font-size: 10px;line-height: 20px;">
                <tr style="vertical-align: middle;line-height: 20px;">
                    <td class="titulo" style="width: 70px;">Dependente:</td>
                    <td class="titulo" style="width: 80px;"></td>
                    <td class="titulo" style="width: 60px;"></td>
                    <td class="titulo" style="width: 120px;"></td>
                    <td class="titulo" style="width: 370px;" colspan="4"></td>
                </tr>
                <tr style="vertical-align: middle;line-height: 20px;">
                    <td><b>Matrícula:</b></td>
                    <td align="center"><?php echo $row["id"]; ?></td>
                    <td><b>Nome:</b></td>
                    <td colspan="3"><?php echo $row["razao_social"]; ?></td>
                    <td><b>Nascimento:</b></td>
                    <td><?php echo $row["data_nascimento"]; ?></td>                    
                </tr>                                
                <tr style="vertical-align: middle;line-height: 20px;">
                    <td><b>Dt. Admissão:</b></td>
                    <td colspan="3"><?php echo $row["data_inclusao"]; ?></td>               
                    <td><b>E-mail:</b></td>
                    <td colspan="3"><?php echo $row["email"]; ?></td>
                </tr>
                <tr style="vertical-align: middle;line-height: 20px;">
                    <td><b>Tipo:</b></td>
                    <td><?php echo $row["nome_cat"]; ?></td>
                    <td><b>Validade:</b></td>
                    <td><?php echo $validade; ?></td>
                    <td><b>Situação:</b></td>
                    <td colspan="3"><?php echo $row["fornecedores_status"]; ?></td>                    
                </tr>                            
                <tr style="vertical-align: middle;line-height: 20px;">
                    <td><b>Observação:</b></td>
                    <td colspan="3"><?php echo $row["Observacao"]; ?></td> 
                    <td><b>Dt.Início:</b></td>
                    <td colspan="3"><?php echo $row["dt_cadastro"]; ?></td>                    
                </tr>                            
            </table>  
            <?php } ?>
        </div>
    </body>
</html>