<?php include "../../Connections/configini.php";
$DateBegin = getData("B");
$DateEnd = getData("E");
$campoLivre = "";
$cur = odbc_exec($con, "select descricao_campo from sf_configuracao_campos_livres where ativo_campo = 1;");
while ($RFP = odbc_fetch_array($cur)) {
    $campoLivre .= utf8_encode($RFP['descricao_campo']) . "|";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>                
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">      
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1>Clube<small>Relatório de Sócios</small></h1>                    
                    </div>
                    <div class="row-fluid">                        
                        <div class="" style="display:flex; flex-direction: row;align-items: center;justify-content: space-evenly;">
                            <div style="width: 13%">
                                <span style="display:block;">Titularidade:</span>
                                <select name="itemsPlano[]" id="txtTitularidade" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                    <?php $cur = odbc_exec($con, "select id_cat, nome_cat from sf_categoria_socio order by nome_cat") or die(odbc_errormsg());
                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                        <option value="<?php echo $RFP['id_cat'] ?>"><?php echo utf8_encode($RFP['nome_cat']) ?></option>
                                    <?php } ?>
                                </select>  
                            </div>
                            <div style="width: 10%">
                                <span style="display:block;">Status do Cliente:</span>
                                <select name="itemsStatus[]" id="txtStatus" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                    <option value="1">Ativo</option>
                                    <option value="4">Ativo Credencial</option>
                                    <option value="14">Ativo Abono</option>
                                    <?php if ($mdl_seg_ == 0) { ?>
                                    <option value="5">Ativo Ausente 1</option>
                                    <option value="6">Ativo Ausente 2</option>
                                    <option value="7">Ativo Ausente 3</option>
                                    <option value="8">Ativo Ausente 4</option>
                                    <option value="9">Ativo Ausente 5</option>
                                    <?php } ?>
                                    <option value="12">Ativo Em Aberto</option>                                    
                                    <option value="2">Suspenso</option>
                                    <option value="16">Cancelado</option>
                                    <option value="3">Inativo</option>
                                    <?php if ($mdl_srs_ == 1) { ?>
                                    <option value="10">Serasa</option>
                                    <?php } if ($mdl_clb_ == 1 && $mdl_seg_ == 0) { ?>
                                    <option value="11">Dependente</option>
                                    <option value="13">Desligados</option>
                                    <option value="15">Desligado Em Aberto</option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div style="width: 12%">
                                <span style="display:block;">Crachá Entregue:</span>
                                <select name="itemsCracha[]" id="txtCracha" multiple="multiple" class="select" style="width:100%" class="input-medium">                                    
                                    <option value="null">Selecione</option>
                                    <option value="1">CONFECÇÃO</option>            
                                    <option value="2">SOLICITAÇÃO</option>
                                    <option value="3">RECEBIMENTO</option>            
                                    <option value="4">CRACHÁ ENTREGUE</option>
                                </select>
                            </div> 
                            <div style="width: 9%">
                                <span style="display:block;">Tipo de Sócio:</span>
                                <select name="ckbTipo[]" id="ckbTipo" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                    <?php $cur = odbc_exec($con, "select id_tipo, descricao_tipo from sf_tipo_socio order by descricao_tipo") or die(odbc_errormsg());
                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                        <option value="<?php echo $RFP['id_tipo'] ?>"><?php echo utf8_encode($RFP['descricao_tipo']) ?></option>
                                    <?php } ?>
                                </select>  
                            </div>                            
                            <div style="width: 14%">
                                <span style="display:block;">Grupo de Cliente:</span>
                                <select name="itemsGrupo[]" id="txtGrupo" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                    <?php $cur = odbc_exec($con, "select id_grupo,descricao_grupo from dbo.sf_grupo_cliente where tipo_grupo = 'C' and inativo_grupo = 0 order by descricao_grupo") or die(odbc_errormsg());
                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                        <option value="<?php echo $RFP['id_grupo'] ?>"><?php echo utf8_encode($RFP['descricao_grupo']) ?></option>
                                    <?php } ?>
                                </select>  
                            </div>   
                            <div style="width: 7%">
                                <span>Desligado:</span>
                                <select class="select" id="ckbTrancar" name="ckbTrancar" style="width:100%;">                                   
                                    <option value="null">Selecione</option>
                                    <option value="0">NÃO</option>
                                    <option value="1">SIM</option>
                                </select>
                            </div>                            
                            <div style="width: 7%">
                                <span>Titular:</span>
                                <select class="select" id="ckbTitular" name="ckbTitular" style="width:100%;">                                   
                                    <option value="null">Selecione</option>
                                    <option value="0">NÃO</option>
                                    <option value="1">SIM</option>
                                </select>
                            </div>                            
                            <div style="width: 10%">
                                <span>Data de:</span>
                                <select class="select" id="txtTipo" name="txtTipo" style="width:100%;">                                   
                                    <option value="0" selected>Cadastro</option>
                                    <option value="1">Vencimento</option>
                                    <option value="2">Pagamento</option>
                                </select>
                            </div>                            
                            <div style="width: 13%">
                                <span style="display:block;">Data:</span>
                                <span id="sprytextfield1"><input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial"></span>
                                <span id="sprytextfield2"><input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final"></span>                                                                                   
                            </div>               
                            <button id="btnfind" name="btnfind" class="button button-turquoise btn-primary" type="button" style="margin-top: 11px;"><span class="ico-search icon-white"></span></button>                                                    
                        </div>                        
                        <hr style="margin: 10px 0">
                        <div class="boxhead">
                            <div class="boxtext" style="position:relative">
                                Categorias de Sócios
                                <div style="top:4px; right:2px; line-height:1; position:absolute">
                                    <button type="button" title="Imprimir (Categoria)" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printPDF(1);">
                                        <span class="ico-print icon-white"></span>
                                    </button>
                                    <button type="button" title="Imprimir (Titular-Dependente)" class="button button-green btn-primary" style="margin:0; line-height:17px" onclick="printPDF(4);">
                                        <span class="ico-print icon-white"></span>
                                    </button>
                                    <button type="button" title="Exportar Excel" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printPDF(2);">
                                        <span class="ico-download-3 icon-white"></span>
                                    </button>
                                    <button type="button" title="Exportar Fotos" class="button button-red btn-primary" style="margin:0; line-height:17px" onclick="printPDF(3);">
                                        <span class="ico-download-3 icon-white"></span>
                                    </button>
                                    <button type="button" title="Exportar Remessa" class="button btn-inverse btn-primary" style="margin:0; line-height:14px" onclick="window.open(finalFind(1).replace('ajax/Relatorio-Categoria-Socio_server_processing', 'form/gerar-Layout'),'_blank');">
                                        <span class="ico-download-3 icon-white"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="boxtable">
                            <table id="example" class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" campoLivre="<?php echo substr($campoLivre, 0, -1); ?>">
                                <thead>
                                    <tr>
                                        <th width="7%"><center>Código</center></th>
                                        <th width="16%">Nome</th>
                                        <th width="12%">CPF</th>
                                        <th width="8%">NIP</th>                           
                                        <th width="9%"><center>Telefone</center></th>                                            
                                        <th width="10%"><center>Celular</center></th>                                            
                                        <th width="9%">Crachá</th>                                                                                        
                                        <th width="9%">Dt.Nasc.</th>                                                                                        
                                        <th width="12%">Categoria</th>                                         
                                        <th width="8%"><center>Titular</center></th>                                        
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            <div style="clear:both"></div>                                
                        </div>  
                    </div>
                </div>
            </div>
        </div>              
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>        
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>        
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script> 
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>   
        <script type='text/javascript' src='js/Relatorio-Categoria-Socio.js'></script>
    </body>
    <?php odbc_close($con); ?>
</html>