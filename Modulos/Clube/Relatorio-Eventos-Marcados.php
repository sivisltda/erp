<?php include "../../Connections/configini.php";
$DateBegin = getData("B");
$DateEnd = getData("E");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/main.css" rel="stylesheet" type="text/css"/>                
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
    </head>
    <body>
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">
            <?php include("../../menuLateral.php"); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">      
                    <div class="page-header">
                        <div class="icon"> <span class="ico-arrow-right"></span> </div>
                        <h1>Clube<small>Eventos Marcados</small></h1>                    
                    </div>
                    <div class="row-fluid">                        
                        <div class="" style="display:flex; flex-direction: row;align-items: center;justify-content: flex-end;">
                            <div style="width: 16%; margin-left: 1%;">
                                <span style="display:block;">Locações:</span>
                                <select name="itemsLocacoes[]" id="txtLocacoes" multiple="multiple" class="select" style="width:100%" class="input-medium">
                                    <?php $cur = odbc_exec($con, "select id_loc, nome_loc from sf_locacoes order by id_loc") or die(odbc_errormsg());
                                    while ($RFP = odbc_fetch_array($cur)) { ?>
                                        <option value="<?php echo $RFP['id_loc'] ?>"><?php echo utf8_encode($RFP['nome_loc']) ?></option>
                                    <?php } ?>
                                </select>  
                            </div>                                                        
                            <div style="width: 15%; margin-left: 1%;">
                                <span>Tipo:</span>
                                <select class="select" id="ckbTipo" name="ckbTipo" style="width:100%;">                                   
                                    <option value="0">Sintético</option>
                                    <option value="1">Analítico</option>
                                </select>
                            </div>                                                        
                            <div style="width: 17%; margin-left: 1%;">
                                <span style="display:block;">Dt.Evento:</span>
                                <span id="sprytextfield1"><input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data inicial"></span>
                                até
                                <span id="sprytextfield2"><input type="text" class="datepicker" style="width:79px; margin-bottom:2px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final"></span>                                                                                   
                            </div>               
                            <button id="btnfind" name="btnfind" class="button button-turquoise btn-primary" type="button" style="margin-top: 11px;"><span class="ico-search icon-white"></span></button>                                                    
                        </div>                        
                        <hr style="margin: 10px 0">
                        <div class="boxhead">
                            <div class="boxtext" style="position:relative">
                                Eventos Marcados
                                <div style="top:4px; right:2px; line-height:1; position:absolute">
                                    <button type="button" title="Imprimir" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printPDF(1);">
                                        <span class="ico-print icon-white"></span>
                                    </button>
                                    <button type="button" title="Exportar Excel" class="button button-blue btn-primary" style="margin:0; line-height:17px" onclick="printPDF(2);">
                                        <span class="ico-download-3 icon-white"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="boxtable">
                            <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="example">
                                <thead>
                                    <tr>
                                        <th width="7%"><center>Código</center></th>
                                        <th width="25%">Agendamento</th>
                                        <th width="27%">Cliente/Prospect</th>
                                        <th width="25%">Locações</th>                                                                                                                                                                                    
                                        <th width="8%"><center>Limite</center></th>                                        
                                        <th width="8%"><center>Convidados</center></th>                                        
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            <div style="clear:both"></div>                                
                        </div>  
                    </div>
                </div>
            </div>
        </div>              
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>        
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>        
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/fnReloadAjax.js"></script> 
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>        
        <script type="text/javascript" src="../../js/util.js"></script>   
        <script type='text/javascript' src='js/Relatorio-Eventos-Marcados.js'></script>
    </body>
    <?php odbc_close($con); ?>
</html>