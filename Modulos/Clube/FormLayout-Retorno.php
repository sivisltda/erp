<?php
include '../../Connections/configini.php';
$disabled = 'disabled';
if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_layout set " .
                        "descricao = " . valoresTexto("txtDescricao") . "," .
                        "origem = 'B'," .
                        "grupo = " . valoresSelect("txtGrupo") . "," .
                        "id_produto = " . valoresSelect("txtPlano") . "," .                
                        "tipo_documento = " . valoresSelect("txtTipoDocumento") . "," .
                        "id_campo_livre = " . valoresSelect("txtCampoLivre") . "," .
                        "tipo = " . valoresTexto("txtTipo") . "," .
                        "extensao = " . valoresTexto("txtExtensao") . "," .
                        "inativo = " . valoresCheck("txtInativo") . "," .
                        "taxa = " . valoresNumericos("txtTaxa") .
                        " where id = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_layout (descricao,origem,grupo,id_produto,tipo_documento,id_campo_livre,tipo,extensao,inativo,taxa) values (" .
                        valoresTexto("txtDescricao") . "," .
                        "'B'," .
                        valoresSelect("txtGrupo") . "," .
                        valoresSelect("txtPlano") . "," .                
                        valoresSelect("txtTipoDocumento") . "," .
                        valoresSelect("txtCampoLivre") . "," .
                        valoresTexto("txtTipo") . "," .
                        valoresTexto("txtExtensao") . "," .
                        valoresCheck("txtInativo") . "," .
                        valoresNumericos("txtTaxa") . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id from sf_layout order by id desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
    if (isset($_POST['txtContatosTotal'])) {
        $max = $_POST['txtContatosTotal'];
        $notIn = '0';
        for ($i = 0; $i <= $max; $i++) {
            if (isset($_POST['txtIid_' . $i]) && is_numeric($_POST['txtIid_' . $i])) {
                $notIn .= "," . $_POST['txtIid_' . $i];
            }
        }
        odbc_exec($con, "delete from sf_layout_item where id_layout = " . $_POST['txtId'] . " and id not in (" . $notIn . ")");
        for ($i = 0; $i <= $max; $i++) {
            if (isset($_POST['txtIid_' . $i])) {
                if (is_numeric($_POST['txtIid_' . $i])) {                    
                    odbc_exec($con, "update sf_layout_item set id_layout = " . $_POST['txtId'] . ",ordinal = " . $i . "," . 
                    "descricao = " . valoresTexto('txtIDesc_' . $i) . ",campo = " . valoresTexto('txtICampo_' . $i) . "," . 
                    "tamanho = " . valoresTexto('txtITamanho_' . $i) . ",completar = " . valoresTexto('txtIComplemento_' . $i) . "," . 
                    "completar_direcao = '',separador = " . valoresTexto('txtISeparador_' . $i) . "," . 
                    "data_formato = " . valoresTexto('txtIDataFormato_' . $i) . 
                    " WHERE id = " . $_POST['txtIid_' . $i]) or die(odbc_errormsg());                    
                } else {
                    odbc_exec($con, "insert into sf_layout_item(id_layout,ordinal," .
                    "descricao,campo,tamanho,completar,completar_direcao,separador,data_formato) values(" .
                    $_POST['txtId'] . "," . $i . "," . valoresTexto('txtIDesc_' . $i) . 
                    "," . valoresTexto('txtICampo_' . $i) . "," . valoresTexto('txtITamanho_' . $i) . 
                    "," . valoresTexto('txtIComplemento_' . $i) . ",''," . 
                    valoresTexto('txtISeparador_' . $i) . "," . valoresTexto('txtIDataFormato_' . $i) . ")") or die(odbc_errormsg());
                }
            }
        }
    }
}
if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_layout WHERE id = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}
if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}
if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_layout where id = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id']);
        $descricao = utf8_encode($RFP['descricao']);
        $grupoPessoa = utf8_encode($RFP['grupo']);        
        $tipoDocumento = utf8_encode($RFP['tipo_documento']);
        $campos_livres = utf8_encode($RFP['id_campo_livre']);
        $tipo = utf8_encode($RFP['tipo']);
        $plano = utf8_encode($RFP['id_produto']);        
        $extensao = utf8_encode($RFP['extensao']);
        $inativo = utf8_encode($RFP['inativo']);
        $taxa = escreverNumero($RFP['taxa']);
    }
} else {
    $disabled = '';
    $id = '';
    $descricao = '';
    $grupoPessoa = '';    
    $tipoDocumento = '';
    $campos_livres = '';
    $tipo = '';
    $plano = '';    
    $extensao = '';
    $inativo = '';
    $taxa = '0,00';
}
if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}

function getStatusCampo($server) {
    switch ($server) {
        case "id_fornecedores_despesas":
            return "Matrícula";        
        case "matricula":
            return "Campo Livre";
        case "razao_social":
            return "Nome";
        case "dt_inicio_mens":
            return "Data Vencimento";
        case "valor_mens":
            return "Valor Parcela";
        case "cpf":
            return "CPF";
        case "nosso_numero":
            return "Nosso Número";
        case "valor_custo":
            return "Valor Custo";
        default:
            return "Outro";
    }
}
?>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<link href="../../../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script> 
<script src="../../../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<body>
    <form action="FormLayout-Retorno.php" name="frmLayout-Retorno" method="POST">
        <div class="frmhead">
            <div class="frmtext">Layout de Retorno</div>
            <div class="frmicon" onClick="parent.FecharBox()">
                <span class="ico-remove"></span>
            </div>
        </div>
        <div class="frmcont">
            <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
            <div style="width: 28%; float:left;">
                <span>Descrição:</span>
                <input id="txtDescricao" name="txtDescricao" type="text" value="<?php echo $descricao; ?>" <?php echo $disabled; ?> style="width:100%"/>
            </div>            
            <div style="width: 34%;float: left; margin-left: 1%;">
                <span>Plano:</span>
                <select id="txtPlano" name="txtPlano" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                    <option value="null">Selecione</option>
                    <?php $cur = odbc_exec($con, "select conta_produto, descricao from sf_produtos where tipo = 'C' and inativa = 0 order by descricao") or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) { ?>
                        <option value="<?php echo $RFP['conta_produto']; ?>"<?php
                        if (!(strcmp($RFP['conta_produto'], $plano))) {
                            echo "SELECTED";
                        }
                        ?>><?php echo utf8_encode($RFP['descricao']); ?>
                        </option>
                    <?php } ?>                    
                </select>                
            </div>                  
            <div style="width: 23%;float: left; margin-left: 1%;">
                <span>Grupo:</span>
                <select id="txtGrupo" name="txtGrupo" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                    <option value="null">Selecione</option>
                    <?php $cur = odbc_exec($con, "select id_grupo,descricao_grupo from dbo.sf_grupo_cliente where tipo_grupo = 'C' and inativo_grupo = 0 order by descricao_grupo") or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) { ?>
                        <option value="<?php echo $RFP['id_grupo']; ?>"<?php
                        if (!(strcmp($RFP['id_grupo'], $grupoPessoa))) {
                            echo "SELECTED";
                        }
                        ?>><?php echo utf8_encode($RFP['descricao_grupo']); ?>
                        </option>
                    <?php } ?>                    
                </select>                
            </div>
            <div style="width: 12%; float:left; margin-left: 1%;">
                <span>Taxa:</span>
                <input id="txtTaxa" name="txtTaxa" type="text" value="<?php echo $taxa; ?>" <?php echo $disabled; ?> style="width:100%"/>
            </div>            
            <div style="width: 18%;float: left;">
                <span>Tipo:</span>
                <select id="txtTipo" name="txtTipo" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                    <option value="M" <?php echo ($tipo == "M" ? "selected" : ""); ?>>Mensalidade</option>                    
                    <option value="B" <?php echo ($tipo == "B" ? "selected" : ""); ?>>Boleto</option>                    
                </select>                
            </div>
            <div style="width: 30%;float: left; margin-left: 1%;">
                <span>Forma de Pagamento:</span>
                <select id="txtTipoDocumento" name="txtTipoDocumento" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                    <option value="null">Selecione</option>
                    <?php $cur = odbc_exec($con, "select id_tipo_documento, descricao from sf_tipo_documento where s_tipo in ('A','C') and inativo = 0") or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) { ?>
                        <option value="<?php echo $RFP['id_tipo_documento']; ?>"<?php
                        if (!(strcmp($RFP['id_tipo_documento'], $tipoDocumento))) {
                            echo "SELECTED";
                        }
                        ?>><?php echo utf8_encode($RFP['descricao']); ?>
                        </option>
                    <?php } ?>                    
                </select>                
            </div>
            <div style="width: 24%;float: left; margin-left: 1%;">
                <span>Campo Livre:</span>
                <select id="txtCampoLivre" name="txtCampoLivre" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                    <option value="null">Selecione</option>
                    <?php $cur = odbc_exec($con, "select id_campo, descricao_campo from sf_configuracao_campos_livres order by descricao_campo") or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) { ?>
                        <option value="<?php echo $RFP['id_campo']; ?>"<?php
                        if (!(strcmp($RFP['id_campo'], $campos_livres))) {
                            echo "SELECTED";
                        }
                        ?>><?php echo utf8_encode($RFP['descricao_campo']); ?>
                        </option>
                    <?php } ?>                    
                </select>                
            </div>            
            <div style="width: 12%;float: left; margin-left: 1%;">
                <span>Extensão:</span>
                <select id="txtExtensao" name="txtExtensao" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                    <option value="txt" <?php echo ($extensao == "txt" ? "selected" : ""); ?>>.txt</option>                    
                    <option value="xls" <?php echo ($extensao == "xls" ? "selected" : ""); ?>>.xls</option>                    
                    <option value="csv" <?php echo ($extensao == "csv" ? "selected" : ""); ?>>.csv</option>                    
                </select>                
            </div>
            <div style="width: 12%;float: left; margin-left: 1%;">
                <span>Inativo:</span>
                <select id="txtInativo" name="txtInativo" class="input-medium" <?php echo $disabled; ?> style="width:100%">
                    <option value="0" <?php echo ($inativo == "0" ? "selected" : ""); ?>>Não</option>                    
                    <option value="1" <?php echo ($inativo == "1" ? "selected" : ""); ?>>Sim</option>                    
                </select>                
            </div>            
            <div style="width: 100%; float: left;">
                <input id="txtIdItemEdit" value="" type="hidden"/>
                <span>Campos:</span>
                <div style="width:100%; float:left">
                    <div style="background:#F1F1F1; border:1px solid #DDD; border-bottom:0; padding:10px 10px 0">
                        <input id="txtIdItem" value="" type="hidden"/>
                        <div style="float:left; width:20%;">
                            <input id="txtDescricaoItem" placeholder="Descrição" type="text" class="input-medium" maxlength="256" autocomplete="off" <?php echo $disabled; ?>>
                        </div>
                        <div style="float:left; width:24%; margin-left:0.5%;">
                            <select id="txtCampoItem" class="input-medium" style="width:100%" <?php echo $disabled; ?>>                                
                                <option value="">Outro</option>
                                <option value="id_fornecedores_despesas">Matrícula</option>
                                <option value="matricula">Campo Livre</option>
                                <option value="nosso_numero">Nosso Número</option>
                                <option value="cpf">CPF</option>
                                <option value="razao_social">Nome</option>
                                <option value="dt_inicio_mens">Data Vencimento</option>
                                <option value="valor_mens">Valor Parcela</option>
                                <option value="valor_custo">Valor Custo</option>
                            </select>
                        </div>   
                        <div style="float:left; width:8%; margin-left:0.5%;">
                            <input id="txtComplementoItem" placeholder="Inicio" type="text" class="input-medium" maxlength="512" autocomplete="off" <?php echo $disabled; ?>>
                        </div>                        
                        <div style="float:left; width:11%; margin-left:0.5%;">
                            <input id="txtTamanhoItem" placeholder="Tamanho" type="text" class="input-medium" maxlength="3" autocomplete="off" <?php echo $disabled; ?>>
                        </div>                        
                        <div style="float:left; width:11%; margin-left:0.5%;">
                            <select id="txtSeparador" class="input-medium" style="width:100%" <?php echo $disabled; ?>>                                
                                <option value="">Sem</option>                                
                                <option value=".">.</option>
                                <option value=",">,</option>
                                <option value="-">-</option>
                                <option value="/">/</option>
                                <option value="_">_</option>
                            </select>
                        </div>                                                                      
                        <div style="float:left; width:17%; margin-left:0.5%;">
                            <select id="txtDataFormato" class="input-medium" style="width:100%" <?php echo $disabled; ?>>                                
                                <option value="">Sem</option>                           
                                <option value="ddmmaaaa">ddmmaaaa</option>
                                <option value="aaaammdd">aaaammdd</option>                                
                                <option value="ddmmaa">ddmmaa</option>
                                <option value="aammdd">aammdd</option>                                
                                <option value="mmaaaa">mmaaaa</option>                            
                                <option value="aaaamm">aaaamm</option>
                                <option value="mmaa">mmaa</option>                            
                                <option value="aamm">aamm</option>
                            </select>
                        </div>                                                                      
                        <div style="float:left; width:5%; margin-left:0.5%;">
                            <button type="button" onclick="addCampos();" class="btn dblue" style="height: 26px;background-color: #308698 !important;" <?php echo $disabled; ?>>
                                <span style="margin-top: 4px;" class="ico-plus icon-white"></span>
                            </button>                            
                        </div>                        
                        <div style="clear:both"></div>
                    </div>
                    <div style="background:#F1F1F1; border:1px solid #DDD; border-top:0; padding:5px 10px 10px">
                        <div id="divContatos" style="height:180px; background:#FFF; border:1px solid #DDD; overflow-y:scroll;">
                            <?php
                            $i = 0;
                            if (is_numeric($id)) {
                                $cur = odbc_exec($con, "select * from sf_layout_item where id_layout = " . $id . " order by ordinal") or die(odbc_errormsg());
                                while ($RFP = odbc_fetch_array($cur)) { $i++; ?>
                                    <div id="tabela_linha_<?php echo $i; ?>" style="padding:2px; border-bottom:1px solid #DDD; overflow: hidden;">
                                        <input id="txtIid_<?php echo $i; ?>" name="txtIid_<?php echo $i; ?>" value="<?php echo utf8_encode($RFP["id"]);?>" type="hidden">
                                        <input id="txtIDesc_<?php echo $i; ?>" name="txtIDesc_<?php echo $i; ?>" value="<?php echo utf8_encode($RFP["descricao"]);?>" type="hidden">
                                        <input id="txtICampo_<?php echo $i; ?>" name="txtICampo_<?php echo $i; ?>" value="<?php echo utf8_encode($RFP["campo"]);?>" type="hidden">
                                        <input id="txtITamanho_<?php echo $i; ?>" name="txtITamanho_<?php echo $i; ?>" value="<?php echo utf8_encode($RFP["tamanho"]);?>" type="hidden">
                                        <input id="txtIComplemento_<?php echo $i; ?>" name="txtIComplemento_<?php echo $i; ?>" value="<?php echo utf8_encode($RFP["completar"]);?>" type="hidden">
                                        <input id="txtISeparador_<?php echo $i; ?>" name="txtISeparador_<?php echo $i; ?>" value="<?php echo utf8_encode($RFP["separador"]);?>" type="hidden">
                                        <input id="txtIDataFormato_<?php echo $i; ?>" name="txtIDataFormato_<?php echo $i; ?>" value="<?php echo utf8_encode($RFP["data_formato"]);?>" type="hidden">
                                        <div id="tabela_desc_<?php echo $i; ?>" style="line-height:27px; float:left"><?php echo " * <a href=\"javascript:void(0)\" onclick=\"editCampos(" . $i . ")\">" . utf8_encode($RFP["descricao"]) . "</a> [" . getStatusCampo($RFP["campo"]) . "][" . $RFP["completar"] . "] tamanho: [" . $RFP["tamanho"] . "][" . $RFP["separador"] . "][" . $RFP["data_formato"] . "]"; ?></div> 
                                        <div style="width: 5%; padding: 4px 0px 3px; float: right; cursor: pointer;" onclick="return removeLinha('<?php echo $i; ?>');">
                                            <span class="ico-remove" style="font-size:20px"></span>
                                        </div>                                
                                    </div> 
                                    <?php
                                }
                            } ?>
                            <input id="txtContatosTotal" name="txtContatosTotal" type="hidden" value="<?php echo $i; ?>"/>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
        <div class="frmfoot">
            <div class="frmbtn">
                <?php if ($disabled == '') { ?>
                    <button class="btn green" onclick="return validar();" type="submit" name="bntSave" id="bntOK" ><span class="ico-checkmark"></span> Gravar</button>
                    <?php if ($_POST['txtId'] == '') { ?>
                        <button class="btn yellow" onClick="parent.FecharBox()" id="bntOK"><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn yellow" type="submit" name="bntAlterar" id="bntOK" ><span class="ico-reply"></span> Cancelar</button>
                        <?php
                    }
                } else { ?>
                    <button class="btn green" type="submit" name="bntNew" id="bntOK" value="Novo"><span class="ico-plus-sign"> </span> Novo</button>
                    <button class="btn green" type="submit" name="bntEdit" id="bntOK" value="Alterar"> <span class="ico-edit"> </span> Alterar</button>
                    <button class="btn red" type="submit" name="bntDelete" id="bntOK" value="Excluir" onClick="return confirm('Deseja deletar esse registro?')" ><span class=" ico-remove"> </span> Excluir</button>
                <?php } ?>
            </div>
        </div>
    </form>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src="../../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src="../../../js/plugins/select/select2.min.js"></script>
    <script type='text/javascript' src='../../../js/plugins.js'></script>
    <script type="text/javascript" src="../../../js/GoBody/datatables/media/js/jquery.dataTables.min.js" ></script>
    <script type='text/javascript' src='../../../js/plugins/datatables/fnReloadAjax.js'></script>
    <script type="text/javascript" src="../../../js/plugins/bootbox/bootbox.js"></script>
    <script type="text/javascript" src="../../../js/moment.min.js"></script>
    <script type="text/javascript" src="../../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../../js/jquery.priceformat.min.js'></script>    
    <script type="text/javascript" src="../../js/util.js"></script>
    <script type="text/javascript">
        
        $("#txtTaxa").priceFormat({prefix: "", centsSeparator: lang["centsSeparator"], thousandsSeparator: lang["thousandsSeparator"]});

        function addCampos() {
            if ($("#txtDescricaoItem").val() !== "") {
                if($.isNumeric($("#txtIdItemEdit").val())) {
                    let id = $("#txtIdItemEdit").val();
                    $("#txtIid_" + id).val($("#txtIdItem").val());
                    $("#txtIDesc_" + id).val($("#txtDescricaoItem").val());
                    $("#txtICampo_" + id).val($("#txtCampoItem").val());
                    $("#txtITamanho_" + id).val($("#txtTamanhoItem").val());
                    $("#txtIComplemento_" + id).val($("#txtComplementoItem").val());
                    $("#txtISeparador_" + id).val($("#txtSeparador").val());                                       
                    $("#txtIDataFormato_" + id).val($("#txtDataFormato").val());                                       
                    $("#tabela_desc_" + id).html(" * <a href=\"javascript:void(0)\" onclick=\"editCampos(" + id + ")\">" + $("#txtDescricaoItem").val() + "</a> [" + $("#txtCampoItem option:selected").html() + "][" + $("#txtComplementoItem").val() + "] tamanho: [" + $("#txtTamanhoItem").val() + "][" + $("#txtSeparador").val() + "][" + $("#txtDataFormato").val() + "]");
                } else {
                    let id = parseInt($("#txtContatosTotal").val()) + 1;                
                    let linha = "<div id=\"tabela_linha_" + id + "\" style=\"padding:2px; border-bottom:1px solid #DDD; overflow: hidden;\">" +                        
                        "<input id=\"txtIid_" + id + "\" name=\"txtIid_" + id + "\" value=\"" + $("#txtIdItem").val() + "\" type=\"hidden\">" +
                        "<input id=\"txtIDesc_" + id + "\" name=\"txtIDesc_" + id + "\" value=\"" + $("#txtDescricaoItem").val() + "\" type=\"hidden\">" +                    
                        "<input id=\"txtICampo_" + id + "\" name=\"txtICampo_" + id + "\" value=\"" + $("#txtCampoItem").val() + "\" type=\"hidden\">" +                    
                        "<input id=\"txtITamanho_" + id + "\" name=\"txtITamanho_" + id + "\" value=\"" + $("#txtTamanhoItem").val() + "\" type=\"hidden\">" +
                        "<input id=\"txtIComplemento_" + id + "\" name=\"txtIComplemento_" + id + "\" value=\"" + $("#txtComplementoItem").val() + "\" type=\"hidden\">" +                                                             
                        "<input id=\"txtISeparador_" + id + "\" name=\"txtISeparador_" + id + "\" value=\"" + $("#txtSeparador").val() + "\" type=\"hidden\">" +                     
                        "<input id=\"txtIDataFormato_" + id + "\" name=\"txtIDataFormato_" + id + "\" value=\"" + $("#txtDataFormato").val() + "\" type=\"hidden\">" +                     
                        "<div id=\"tabela_desc_" + id + "\" style=\"line-height:27px; float:left\"> * <a href=\"javascript:void(0)\" onclick=\"editCampos(" + id + ")\">" + $("#txtDescricaoItem").val() + "</a> [" + $("#txtCampoItem option:selected").html() + "][" + $("#txtComplementoItem").val() + "] tamanho: [" + $("#txtTamanhoItem").val() + "][" + $("#txtSeparador").val() + "][" + $("#txtDataFormato").val() + "]</div>" +
                        "<div style=\"width: 5%; padding: 4px 0px 3px; float: right; cursor: pointer;\" onclick=\"return removeLinha('" + id + "');\">" +
                        "<span class=\"ico-remove\" style=\"font-size:20px\"></span></div></div>";
                    $("#divContatos").append(linha);
                    $("#txtContatosTotal").val(id);
                }
                $("#txtIdItemEdit").val("");                
            }
        }
        
        function editCampos(id) {
            $("#txtIdItemEdit").val(id);
            $("#txtIdItem").val($("#txtIid_" + id).val());
            $("#txtDescricaoItem").val($("#txtIDesc_" + id).val());
            $("#txtCampoItem").val($("#txtICampo_" + id).val());
            $("#txtTamanhoItem").val($("#txtITamanho_" + id).val());
            $("#txtComplementoItem").val($("#txtIComplemento_" + id).val());
            $("#txtSeparador").val($("#txtISeparador_" + id).val());            
            $("#txtDataFormato").val($("#txtIDataFormato_" + id).val());          
        }

        function validar() {
            if ($('#txtDescricao').val() === "") {
                bootbox.alert("Preencha o campo Descrição!");
                return false;           
            } else if ($('#txtTipoDocumento').val() === "null") {
                bootbox.alert("Preencha o campo Forma de Pagamento!");
                return false;
            } else if ($('#txtTipo').val() === "null") {
                bootbox.alert("Preencha o campo Tipo!");
                return false;
            } else if ($('#txtExtensao').val() === "") {
                bootbox.alert("Preencha o campo Extensão!");
                return false;
            } else if ($('#txtInativo').val() === "") {
                bootbox.alert("Preencha o campo Inativo!");
                return false;
            }
            return true;
        }

        function removeLinha(id) {
            $("#tabela_linha_" + id).remove();
        }

    </script>
<?php odbc_close($con); ?>
</body>
