<?php
include './../../Connections/configini.php';

$imprimir = 0;
if ($_GET["Del"] != "") {
    odbc_exec($con, "DELETE FROM sf_mensagens_fornecedores WHERE id_fornecedores_despesas = " . $_GET["Del"]);
    odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_endereco_entrega WHERE fornecedores_despesas = " . $_GET["Del"]);
    odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = " . $_GET["Del"]);
    odbc_exec($con, "DELETE FROM sf_fornecedores_despesas WHERE id_fornecedores_despesas = " . $_GET["Del"]);
    echo "<script>window.top.location.href = 'Gerenciador-ClientesSer.php'; </script>";
}
if (isset($_POST["btnPrint"])) {
    $imprimir = 1;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">                
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
        <style>
            .fancybox-wrap, .fancybox-skin, .fancybox-outer, .fancybox-inner, .fancybox-image,  .fancybox-wrap object, .fancybox-nav, .fancybox-nav span, .fancybox-tmp {
                padding: 0;
                margin: 0;
                border: 0;
                outline: none;
                vertical-align: top;
                background:url(../../img/fundosForms/formBancos.PNG);
            }            
            #example td {
                padding: 5px;
                line-height: 16px;
            }
            .select2-choice { height: 29px !important; }
            .select2-choice span { font-size:13px; line-height: 29px !important; }
        </style>
    </head>
    <body>
        <?php if ($imprimir == 0) { ?>
            <div id="loader"><img src="../../img/loader.gif"/></div>
        <?php } ?>
        <div class="wrapper">
            <?php
            if ($imprimir == 0) {
                include("../../menuLateral.php");
            }
            ?>
            <div class="body">
                <?php
                if ($imprimir == 0) {
                    include("../../top.php");
                }
                ?>
                <div class="content">
                    <?php
                    if ($imprimir !== 0) {
                        $visible = "hidden";
                    }
                    ?>
                    <div class="page-header" <?php echo $visible; ?>>
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Serviços<small>Clientes</small></h1>
                    </div>
                    <div class="row-fluid" <?php echo $visible; ?>>
                        <div class="span12">
                            <div class="block">
                                <form method="POST" action="Gerenciador-ClientesSer.php">
                                    <button name="btnPrint" class="button button-blue btn-primary" type="submit"><span class="ico-print icon-white"></span></button>
                                    <select name="txtEstado" id="txtEstado" class="select input-medium" style="width:200px" onChange="MM_jumpMenu('parent', this, 0)">
                                        <option value="Gerenciador-ClientesSer.php">Selecione o estado</option>
                                        <?php
                                        $cur = odbc_exec($con, "select estado_codigo,estado_nome from tb_estados where estado_codigo > 0 order by estado_nome") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) {
                                            ?>
                                            <option value="Gerenciador-ClientesSer.php?es=<?php echo $RFP["estado_codigo"] ?>"<?php
                                            if (!(strcmp($RFP["estado_codigo"], $_GET["es"]))) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP["estado_nome"]) ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                    <select name="txtCidade" id="txtCidade" class="select input-medium" style="width:200px" onChange="MM_jumpMenu('parent', this, 0)">
                                        <option value="Gerenciador-ClientesSer.php">Selecione a cidade</option>
                                        <?php
                                        if ($_GET["es"] != "") {
                                            $sql = "select cidade_codigo,cidade_nome from tb_cidades where cidade_codigo > 0 AND cidade_codigoEstado = " . $_GET["es"] . " ORDER BY cidade_nome";
                                            $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                                            while ($RFP = odbc_fetch_array($cur)) {
                                                ?>
                                                <option value="Gerenciador-ClientesSer.php?es=<?php echo $_GET["es"]; ?>&ci=<?php echo $RFP["cidade_codigo"]; ?>"<?php
                                                if (!(strcmp($RFP["cidade_codigo"], $_GET["ci"]))) {
                                                    echo "SELECTED";
                                                }
                                                ?>><?php echo utf8_encode($RFP["cidade_nome"]) ?>
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <select name="jumpMenu2" id="jumpMenu2" class="select input-medium" style="width:200px" onChange="MM_jumpMenu('parent', this, 0)">
                                        <option value="Gerenciador-ClientesSer.php" >--Selecione--</option>
                                        <?php
                                        $cur = odbc_exec($con, "select id_contas_movimento,sf_grupo_contas.descricao d1, sf_contas_movimento.descricao d2 from sf_contas_movimento 
                                                left join sf_grupo_contas on id_grupo_contas = grupo_conta where tipo = 'C' order by sf_grupo_contas.descricao,sf_contas_movimento.descricao") or die(odbc_errormsg());
                                        while ($RFP = odbc_fetch_array($cur)) { ?>
                                            <option value="Gerenciador-ClientesSer.php?id=<?php echo $RFP["id_contas_movimento"]; ?><?php
                                            if ($_GET["es"] != "") {
                                                echo "&es=" . $_GET["es"];
                                            }
                                            ?><?php
                                            if ($_GET["ci"] != "") {
                                                echo "&ci=" . $_GET["ci"];
                                            }
                                            ?>"<?php
                                            if (!(strcmp($RFP["id_contas_movimento"], $_GET["id"]))) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP["d1"]) . " - " . utf8_encode($RFP["d2"]); ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead" <?php echo $visible; ?>>
                        <div class="boxtext">Clientes</div>
                    </div>
                    <div <?php
                    if ($imprimir == 0) {
                        echo "class=\"boxtable\"";
                    }
                    ?>>
                            <?php
                            if ($imprimir == 1) {
                                $titulo_pagina = "RELATÓRIO DE CONSULTA<br/>CLIENTES DE SERVIÇOS";
                                include "../Financeiro/Cabecalho-Impressao.php";
                            }
                            ?>
                        <table <?php
                        if ($imprimir == 0) {
                            echo "class=\"table\"";
                        } else {
                            echo "border=\"1\"";
                        }
                        ?> style="float:none" cellpadding="0" cellspacing="0" width="100%" id="example">
                            <thead>
                                <tr id="formPQ">
                                    <th width="3%"></th>
                                    <th width="5%">Cód.</th>
                                    <th width="30%">Nome Fantasia/Razão Social/Nome:</th>
                                    <th width="10%">Endereço:</th>
                                    <th width="15%">Tel.:</th>
                                    <th width="12%">E-mail:</th>
                                    <th width="7%">Status:</th>
                                    <th width="8%"><center>Categoria:</center></th>
                            </tr>
                            </thead>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/jquery.mousewheel.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/charts.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>        
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            
            function AbrirBox(opc){
            $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:700px; height:330px; position:absolute; top:50%; left:50%; margin-left:-358px; margin-top:-175px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:330px' src='DetalheCalendario.php?id=" + opc + "&idx=0'></iframe></div></div></div></div></div>");
            }
            
            function FecharBox(opc){
            $("#newbox").remove();
            var oTable = $("#example").dataTable();
            oTable.fnDraw(false);
            }
            
            function MM_jumpMenu(targ, selObj, restore){ //v3.0
            if (selObj.options[selObj.selectedIndex].value !== "null") {
            eval(targ + ".location='" + selObj.options[selObj.selectedIndex].value + "'");
            if (restore) selObj.selectedIndex = 0;
            }
            }

            $(document).ready(function(){
            $("#example").dataTable({
            "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": "Gerenciador-Clientes_server_processing.php?imp=<?php echo $imprimir; ?><?php
                            if ($_GET['id'] != '' || $_GET['es'] != '' || $_GET['ci'] != '') {
                                echo '&id=' . $_GET['id'] . '&es=' . $_GET['es'] . '&ci=' . $_GET['ci'];
                            }
                        ?>",
                    "oLanguage": {
                    "oPaginate": {
                    "sFirst":       "Primeiro",
                            "sLast":        "Último",
                            "sNext":        "Próximo",
                            "sPrevious":    "Anterior"
                    }, "sEmptyTable":    "Não foi encontrado nenhum resultado",
                            "sInfo":            "Visualização do registro _START_ ao _END_ de _TOTAL_",
                            "sInfoEmpty":       "Visualização do registro 0 ao 0 de 0",
                            "sInfoFiltered":    "(filtrado de _MAX_ registros totais)",
                            "sLengthMenu":      "Visualização de _MENU_ registros",
                            "sLoadingRecords":  "Carregando...",
                            "sProcessing":      "Processando...",
                            "sSearch":          "Pesquisar:",
                            "sZeroRecords":     "Não foi encontrado nenhum resultado"},
                    "sPaginationType":  "full_numbers",
                <?php if ($imprimir == 1) { ?>
                    "fnInitComplete": function(oSettings, json){ window.print(); window.history.back(); }
                <?php } ?>
            });
            });
        </script>     
    </body>
    <?php if ($imprimir == 1) { ?>
        <script type="text/javascript">
                $(window).load(function(){
                $(".body").css("margin-left", 0);
                $("#example_length").remove();
                $("#example_filter").remove();
                $("#example_paginate").remove();
                $("#formPQ > th").css("background-image", "none");
                });
        </script>
        <?php
    }
    odbc_close($con);
    ?>
</html>