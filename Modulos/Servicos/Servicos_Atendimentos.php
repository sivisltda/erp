<?php
include './../../Connections/configini.php';

$imprimir = 0;
if ($_GET['Del'] != '') {
    odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_endereco_entrega WHERE fornecedores_despesas = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = " . $_GET['Del']);
    odbc_exec($con, "DELETE FROM sf_fornecedores_despesas WHERE id_fornecedores_despesas = " . $_GET['Del']);
    echo "<script>window.top.location.href = 'Servicos_Atendimentos.php'; </script>";
}
if ($_GET['Delx'] != '') {
    odbc_exec($con, "DELETE FROM sf_telemarketing_item WHERE id_telemarketing = " . $_GET['Delx']);
    odbc_exec($con, "DELETE FROM sf_mensagens_telemarketing WHERE id_tele = " . $_GET['Delx']);
    odbc_exec($con, "DELETE FROM sf_telemarketing WHERE id_tele = " . $_GET['Delx']);
    if ($_GET['page'] != '') {
        echo "<script>window.top.location.href = '../" . $_GET['page'] . "'; </script>";
    } else {
        echo "<script>window.top.location.href = 'Servicos_Atendimentos.php?Cli=" . $_GET['gb'] . "'; </script>";
    }
}
if ($_GET['Cls'] != '') {
    odbc_exec($con, "UPDATE sf_telemarketing SET pendente = 0 WHERE id_tele = " . $_GET['Cls']);
    echo "<script>window.top.location.href = 'Servicos_Atendimentos.php?Cli=" . $_GET['gb'] . "'; </script>";
}
if (isset($_POST['btnPrint'])) {
    $imprimir = 1;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>                      
    </head>
    <body>
        <?php if ($imprimir == 0) { ?>
            <div id="loader"><img src="../../img/loader.gif"/></div><?php } ?>
        <div class="wrapper">
            <?php
            if ($imprimir == 0) {
                include('../../menuLateral.php');
            }
            ?>
            <div class="body">
                <?php
                if ($imprimir == 0) {
                    include("../../top.php");
                }
                ?>
                <div class="content">
                    <?php if ($imprimir == 0) { ?>
                        <div class="page-header">
                            <div class="icon"> <span class="ico-arrow-right"></span> </div>
                            <h1>Serviços<small>Serviços</small></h1>                    
                        </div>
                    <?php } ?>
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext" style="text-align: left;margin-left: 10px;">Prospects / Clientes
                            <?php if ($imprimir == 0) { ?>
                                <div style="float: right;margin-right: 10px;margin-top: -3px;">
                                    <input type="text" name="txtDesc" id="txtDesc" class="inputbox" value="" size="10" style="width:300px; color:#000 !important"/>
                                </div>
                            <?php } ?>                            
                        </div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblCli">
                            <thead>
                                <tr>
                                    <th width="25%">Nome/Razão Social:</th>                                               
                                    <th width="25%">Endereço:</th>                                                
                                    <th width="12%">Bairro:</th>                                                
                                    <th width="12%">Cidade:</th>                                                
                                    <th width="6%">Telefone:</th>
                                    <th width="15%">Contato:</th>
                                    <?php if ($imprimir == 0) { ?>
                                        <th width="5%"><center>Ação:</center></th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $whereCli = "";
                                $razao_social = "";
                                $nomeContato = "";
                                if (is_numeric($_GET['Cli'])) {
                                    $whereCli = " and id_fornecedores_despesas = " . $_GET['Cli'];
                                    $cur = odbc_exec($con, "select *, (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato from sf_fornecedores_despesas left join sf_tipo_documento on forma_pagamento = id_tipo_documento left join tb_estados on estado = estado_codigo left join tb_cidades on cidade = cidade_codigo where tipo in ('P','C') " . $whereCli);
                                    while ($RFP = odbc_fetch_array($cur)) {
                                        $nomeContato = utf8_encode($RFP['contato']);
                                        if (utf8_encode($RFP['nome_fantasia']) != "") {
                                            $razao_social = utf8_encode($RFP['razao_social']) . " (" . utf8_encode($RFP['nome_fantasia']) . ")";
                                        } else {
                                            $razao_social = utf8_encode($RFP['razao_social']);
                                        } ?>               
                                        <tr>
                                            <?php if ($imprimir == 0 && $_GET['Cli'] == '') { ?>
                                                <td><a title="Atendimento" href='Servicos_Atendimentos.php?Cli=<?php echo $RFP['id_fornecedores_despesas']; ?>' ><center><img src="../../img/1367947965_schedule.png" width="16" height="16"></center></a></td>                                                    
                                            <?php } ?>   
                                            <td><a href="javascript:void(0)" onClick="<?php
                                                if ($RFP['tipo'] == 'P') {
                                                    echo "AbrirBox(" . $RFP['id_fornecedores_despesas'] . ",'',2)";
                                                } else {
                                                    echo "AbrirBox(" . $RFP['id_fornecedores_despesas'] . ",'',3)";
                                                }
                                                ?>"><div id="formPQ"><?php echo $razao_social; ?></div></a></td>
                                            <td><div id="formPQ" title="<?php echo utf8_encode($RFP['endereco']) . "," . utf8_encode($RFP['numero']) . " " . utf8_encode($RFP['cep']); ?>"><?php echo utf8_encode($RFP['endereco']) . "," . utf8_encode($RFP['numero']) . " " . utf8_encode($RFP['cep']); ?></div></td>
                                            <td><div id="formPQ" title="<?php echo utf8_encode($RFP['bairro']); ?>"><?php echo utf8_encode($RFP['bairro']); ?></div></td>
                                            <td><div id="formPQ" title="<?php echo utf8_encode($RFP['cidade_nome']) . "-" . utf8_encode($RFP['estado_sigla']); ?>"><?php echo utf8_encode($RFP['cidade_nome']) . "-" . utf8_encode($RFP['estado_sigla']); ?></div></td>
                                            <td><div id="formPQ" title="<?php echo utf8_encode($RFP['telefone_contato']); ?>"><?php echo utf8_encode($RFP['telefone_contato']); ?></div></td>
                                            <td><div id="formPQ" title="<?php echo $nomeContato; ?>"><center><?php echo $nomeContato; ?></center></div></td>
                                            <?php if ($imprimir == 0) { ?>
                                                <td><center>
                                            <?php if ($ckb_adm_cli_ == 1 && $RFP['id_fornecedores_despesas']) { ?>
                                                <a title="Excluir" href='Servicos_Atendimentos.php?Del=<?php echo $RFP['id_fornecedores_despesas']; ?>' ><input name='' type='image' onClick="return confirm('Deseja deletar esse registro?')" src="../../img/1365123843_onebit_33 copy.PNG" width='18' height='18'   value='Enviar'></a>
                                            <?php } ?>
                                        </center></td>
                                    <?php } ?>
                                    </tr>
                                    <?php
                                }
                            } ?>                                                                                                                                                              
                            </tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>                    
                    <div style="clear:both"></div>
                    <div class="boxhead">
                        <div class="boxtext" style="text-align: left;margin-left: 10px;">Ordem de Serviços de <?php echo $razao_social; ?>
                        <?php if ($imprimir == 0 && $_GET['Cli'] > 0) { ?>
                            <div style="float: right;margin-right: 10px;margin-top: -3px;">
                                <button type="button" class="button button-blue btn-primary" style="padding: 3px 7px 3px 7px;" onClick="AbrirBox(0,<?php echo $_GET['Cli']; ?>, 0)"><span class="icon-plus icon-white"></span></button>
                            </div>                                        
                        <?php } ?>
                        </div>
                    </div>
                    <div class="boxtable">
                        <table class="table" cellpadding="0" cellspacing="0" width="100%" id="tblLista">
                            <thead>
                                <tr>
                                    <th width="9%"><center>Data/ Hora</center></th>
                                    <th width="9%"><center>Núm. OS</center></th>
                                    <th width="9%">Atendente</th>
                                    <th width="5%"><center>Pendente</center></th>
                                    <th width="9%">Encaminhado</th>
                                    <th width="9%"><center>Próx. Cont.</center></th>
                                    <th width="13%"><center>Periodo para Cont.</center></th>  
                                    <th width="32%">Relato</th>
                                    <?php if ($imprimir == 0) { ?>
                                        <th width="5%"><center>Ação</center></th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div style="clear:both"></div>
                    </div>                                      
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
        <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type='text/javascript' src='../../js/plugins.js'></script>
        <script type='text/javascript' src='../../js/charts.js'></script>
        <script type='text/javascript' src='../../js/actions.js'></script>
        <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>
        <script type='text/javascript' src='../../js/plugins/highlight/jquery.highlight-4.js'></script>
        <script type='text/javascript' src='../../js/plugins/other/faq.js'></script>   
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            $(document).ready(function () {
                            
                $("#txtDesc").autocomplete({
                source: function (request, response) {
                    $.ajax({
                    url: "./../CRM/ajax.php",
                        dataType: "json",
                        data: {
                        q: request.term,
                            t: "C','P"
                        },
                        success: function (data) {
                        response(data);
                        }
                    });
                }, minLength: 3,
                    select: function (event, ui) {
                        $(window.document.location).attr('href', "Servicos_Atendimentos.php?Cli=" + ui.item.id);
                    }
                });

                $('#tblLista').dataTable({
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": "Servicos_Atendimentos_server_processing.php<?php echo "?imp=" . $imprimir . "&Cli=" . $_GET["Cli"]; ?>",
                    'oLanguage': {
                    'oPaginate': {
                    'sFirst': "Primeiro",
                            'sLast': "Último",
                            'sNext': "Próximo",
                            'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                            'sLengthMenu': "Visualização de _MENU_ registros",
                            'sLoadingRecords': "Carregando...",
                            'sProcessing': "Processando...",
                            'sSearch': "Pesquisar:",
                            'sZeroRecords': "Não foi encontrado nenhum resultado"},
                    "sPaginationType": "full_numbers",
            <?php if ($imprimir == 1) { ?>
                    "fnInitComplete": function (oSettings, json) {
                    window.print(); window.history.back(); }
            <?php } ?>
                });
            });

            function AbrirBox(id, gb, op) {
                if (gb !== "" && id === 0) {
                    $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:667px; height:505px; position:absolute; top:50%; left:50%; margin-left:-333px; margin-top:-252px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:505px' src='../CRM/FormTelemarketing.php?gb=" + gb + "&jn=s'></iframe></div></div></div></div></div>");
                } else if (gb !== "" && op === 0) {
                    $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:667px; height:505px; position:absolute; top:50%; left:50%; margin-left:-333px; margin-top:-252px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:505px' src='./../CRM/FormTelemarketing.php?id=" + id + "&gb=" + gb + "&jn=s'></iframe></div></div></div></div></div>");
                } else if (gb !== "" && op === 1) {
                    $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:667px; height:505px; position:absolute; top:50%; left:50%; margin-left:-333px; margin-top:-252px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:505px' src='./../CRM/FormTelemarketing.php?id=" + id + "&gb=" + gb + "&jn=s&fx=s'></iframe></div></div></div></div></div>");
                } else if (gb === "" && op === 2) {
                    $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:717px; height:563px; position:absolute; top:50%; left:50%; margin-left:-358px; margin-top:-282px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:493px' src='./../CRM/FormGerenciador-Prospects.php?id=" + id + "'></iframe></div></div></div></div></div>");
                } else if (gb === "" && op === 3) {
                    $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:717px; height:563px; position:absolute; top:50%; left:50%; margin-left:-358px; margin-top:-282px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:563px' src='./../Comercial/FormGerenciador-Clientes.php?id=" + id + "'></iframe></div></div></div></div></div>");
                }
            }
            
            function FecharBox() {
                var oTable = $('#tblLista').dataTable();
                oTable.fnDraw(false);
                $("#newbox").remove();
            }
            
            <?php if ($imprimir == 1) { ?>
                $(window).load(function () {
                $(".body").css("margin-left", 0);
                window.print();
                window.history.back();
                });
            <?php } ?>
        </script>
        <?php odbc_close($con); ?>
    </body>
</html>