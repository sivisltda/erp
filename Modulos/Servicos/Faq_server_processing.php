<?php

include './../../Connections/configini.php';

$aColumns = array('id_faq', 'data_pub', 'titulo', 'login_user', 'usuario', 'nome_especificacao', 'conteudo');
$table = "sf_faq left join sf_usuarios on sf_usuarios.id_usuario = sf_faq.usuario left join sf_telemarketing_especificacao on sf_telemarketing_especificacao.id_especificacao = sf_faq.especificacao";
$PageName = "Faq";
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY " . $aColumns[0] . " desc ";
$sOrder2 = " ORDER BY " . $aColumns[0] . " desc ";
$sLimit = 20;
$imprimir = 0;
$id = 0;

if (is_numeric($_GET['id'])) {
    $sWhereX = " and (id_faq = " . $_GET['id'] . " or id_faq_rel = " . $_GET['id'] . ")";
    $toReturnAnd = "&id=" . $_GET['id'];
    $id = $_GET['id'];
} else {
    $sWhereX = " and id_faq_rel is null ";
}

if (isset($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) < count($aColumns) - 1) {
                $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 0) . " " . $_GET['sSortDir_' . $i] . ", ";
                $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i]) + 1], 1) . " " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY " . $aColumns[0] . " desc";
        $sOrder2 = " ORDER BY " . $aColumns[0] . " desc";
    }
}

if ($_GET['sSearch'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= prepareCollum($aColumns[$i], 0) . " LIKE '%" . utf8_decode($_GET['sSearch']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($i == 0) {
        $colunas = $aColumns[$i];
    } else {
        $colunas = $colunas . "," . $aColumns[$i];
    }
}

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row," . $colunas . "
from " . $table . " where " . $aColumns[0] . " > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . " or 1 = " . $imprimir . " " . $sOrder2;
$cur = odbc_exec($con, $sQuery1);

$sQuery = "SELECT COUNT(*) total from " . $table . " where " . $aColumns[0] . " > 0 $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total from " . $table . " where " . $aColumns[0] . " > 0 $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    if ($id > 0) {
        $size = "70";
    } else {
        $size = "40";
    }
    if ($aRow[$aColumns[4]] != '' && file_exists("./../../Pessoas/" . $contrato . "/" . $aRow[$aColumns[4]] . ".png")) {
        $img = "<img src=\"./../../Pessoas/" . $contrato . "/" . $aRow[$aColumns[4]] . ".png\" width=\"" . $size . "px\" align=\"left\" style=\"border-left:solid 3px #FFF; float:left;\"/><br>";
    } else {
        $img = "<img src=\"./../../img/dmitry_m.gif\" width=\"" . $size . "px\" align=\"left\" style=\"border-left:solid 3px #FFF; float:left;\"/><br>";
    }
    if ($aRow[$aColumns[4]] == $_SESSION["id_usuario"]) {
        $row[] = "<center>" . $img . $aRow[$aColumns[3]] . "<br><a title=\"" . escreverDataHora($aRow[$aColumns[1]]) . "\" href=\"javascript:void(0)\" onClick=\"AbrirBox(" . $aRow[$aColumns[0]] . ")\">" . escreverDataHora($aRow[$aColumns[1]]) . "</a></center>";
    } else {
        $row[] = "<center>" . $img . $aRow[$aColumns[3]] . "<br><strong>" . escreverDataHora($aRow[$aColumns[1]]) . "</strong></center>";
    }
    if ($id > 0) {
        $row[] = "<div>" . utf8_encode($aRow[$aColumns[6]]) . "</div>";
    } else {
        $row[] = "<div id='formPQ' title='" . utf8_encode($aRow[$aColumns[2]]) . "'><a href=\"" . $PageName . ".php?id=" . $aRow[$aColumns[0]] . "\" \">" . utf8_encode($aRow[$aColumns[2]]) . "</a></div>";
        $row[] = "<center><div id='formPQ' title='" . utf8_encode($aRow[$aColumns[5]]) . "'>" . utf8_encode($aRow[$aColumns[5]]) . "</div></center>";
    }
    if ($imprimir == 0) {
        if ($aRow[$aColumns[4]] == $_SESSION["id_usuario"]) {
            $row[] = "<center><a title='Excluir' onClick=\"return confirm('Deseja deletar esse registro?');\" href='" . $PageName . ".php?Del=" . $aRow[$aColumns[0]] . $toReturnAnd . "'><input name='' type='image' src='../../img/1365123843_onebit_33 copy.PNG' width='18' height='18' value='Enviar'></a></center>";
        } else {
            $row[] = "";
        }
    }
    $output['aaData'][] = $row;
}
echo json_encode($output);
odbc_close($con);