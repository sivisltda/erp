<?php
include './../../Connections/configini.php';

$PegaURL = "";
$contrato2 = "";
$adm_associado = "Cliente";

if ($_GET["id"] != "") {
    if (is_numeric($_GET["id"])) {
        $PegaURL = $_GET["id"];
    }
}
if ($_GET["crt"] != "") {
    if (is_numeric($_GET["crt"])) {
        $contrato2 = $_GET["crt"];
    }
}

$cur = odbc_exec($con, "select * from dbo.sf_configuracao");
while ($RFP = odbc_fetch_array($cur)) {
    $adm_associado = utf8_encode($RFP['adm_associado']);    
}

if ($PegaURL != "" && $contrato2 != "") {
    $cur = odbc_exec($con, "select id_tele,u.login_user,para_pessoa,data_tele,pendente,proximo_contato,t.obs,id_fornecedores_despesas,tipo,us.login_user login_user_fechamento,data_tele_fechamento,obs_fechamento,especificacao,
    garantia,atendimento_servico,nota_fiscal,data_nf,valor_orcamento,relato,dias_garantia from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa
    LEFT JOIN sf_usuarios us on us.id_usuario = t.de_pessoa_fechamento
    LEFT JOIN sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = t.pessoa where id_tele = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_tele']);
        $data = escreverData($RFP['data_tele']);
        $ano = escreverData($RFP['data_tele'], 'Y');
        $login_user = utf8_encode($RFP['login_user']);
        $garantia = utf8_encode($RFP['garantia']);
        $nota_fiscal = utf8_encode($RFP['nota_fiscal']);
        $data_nota = escreverData($RFP['data_nf']);
        $taxaOrc = utf8_encode($RFP['valor_orcamento']);
        $obs = utf8_encode($RFP['obs']);
        $relato = utf8_encode($RFP['relato']);
        $obsFechamento = utf8_encode($RFP['obs_fechamento']);
        $para = utf8_encode($RFP['para_pessoa']);
        $contato = utf8_encode($RFP['id_fornecedores_despesas']);
        $dias = utf8_encode($RFP['dias_garantia']);
    }
} else {
    $id = "";
    $data = "";
    $ano = "";
    $login_user = "";
    $garantia = "";
    $nota_fiscal = "";
    $data_nota = "";
    $taxaOrc = "";
    $obs = "";
    $relato = "";
    $obsFechamento = "";
    $para = "";
    $contato = "";
    $dias = "";
}
?>
<html>	
    <head>
        <title>SIVIS - Ordem de Serviço</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="./../../css/ordem.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>
    </head>
    <body>
        <?php for ($i = 1; $i <= 2; $i++) { ?>
            <div class="divide" <?php echo ($i > 1 ? "style='margin-left: 3%'" : "");?>>
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="200px" rowspan="3">
                            <table width="100%" cellpadding="0" border="0" cellspacing="0" style="border:0px solid white;padding-bottom: 15px !important;">
                                <tr>
                                    <td width="100px" style="border: 0;">
                                        <?php if (file_exists("./../../Pessoas/" . $_SESSION["contrato"] . "/Empresa/logo_" . $filial_orc . ".jpg")) { ?>
                                            <div style="line-height: 5px;">&nbsp;</div><img src ="./../../Pessoas/<?php echo $_SESSION["contrato"]; ?>/Empresa/logo_<?php echo $filial_orc; ?>.jpg" width="150" height="50" align="middle" />
                                        <?php } else { ?>
                                            <div style="line-height: 5px;">&nbsp;</div>
                                        <?php } ?>
                                    </td>
                                    <td width="265px" style="border: 0;">
                                        <div style="text-align:right;font-size: 8px;"><?php echo $_SESSION["cabecalho"]; ?></div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td colspan="2" class="titulo" style="background-color: white;">ORDEM DE SERVIÇO</td>
                    </tr>
                    <tr>
                        <td style="width:16.5%;height:18px;">O.S. Nº:</td>
                        <td style="width:16.5%;height:18px;"><?php echo $id . "/" . $ano; ?></td>
                    </tr>
                    <tr>
                        <td style="height:18px;">Data:</td>
                        <td style="height:18px;"><?php echo $data; ?></td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td colspan="4" class="titulo">CLIENTE</td>
                    </tr>
                    <tr>
                        <td colspan="4"><?php
                            $cur = odbc_exec($con, "select razao_social,endereco,numero,complemento,bairro,cidade_nome,estado_sigla,cep,cnpj,inscricao_estadual from sf_fornecedores_despesas left join tb_estados on tb_estados.estado_codigo = sf_fornecedores_despesas.estado left join tb_cidades on tb_cidades.cidade_codigo = sf_fornecedores_despesas.cidade where id_fornecedores_despesas = '" . $contato . "' order by razao_social") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) {
                                echo utf8_encode($RFP["razao_social"]) . "<br>" . utf8_encode($RFP["endereco"]) . "," . utf8_encode($RFP["numero"]) . " " . utf8_encode($RFP["complemento"]) . " - " . utf8_encode($RFP["bairro"]) . "<br>" . utf8_encode($RFP["cidade_nome"]) . " - " . utf8_encode($RFP["estado_sigla"]) . " - CEP: " . utf8_encode($RFP["cep"]) . "<br>CNPJ: " . utf8_encode($RFP["cnpj"]) . " * IE: " . utf8_encode($RFP["inscricao_estadual"]) . "<br>";
                            } ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><b>Responsável:</b><span style="margin-left: 8px;"><?php echo $login_user; ?></span></td>
                    </tr>
                    <tr>
                        <td colspan="4" class="titulo">ESPECIFICAÇÃO</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="height:40px">
                            <?php
                            $cur = odbc_exec($con, "select t.id_especificacao,nome_especificacao,ti.id_item from sf_telemarketing_especificacao t inner join sf_telemarketing_item ti on t.id_especificacao = ti.id_especificacao and ti.id_telemarketing = '" . $PegaURL . "' order by nome_especificacao");
                            while ($RFP = odbc_fetch_array($cur)) {
                                echo utf8_encode($RFP['nome_especificacao']) . "<br>";
                            } ?></td>
                    </tr>
                    <tr>
                        <td colspan="3"  class="titulo" style="width: 50%">RELATO DO ATENDIMENTO</td>
                        <td colspan="1"  class="titulo">OBSERVAÇÕES</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="height:70px"><?php echo $relato; ?></td>
                        <td colspan="1" style="height:70px"><?php echo $obs; ?></td>
                    </tr>
                    <tr>
                        <td colspan="4" class="titulo">HISTÓRICO DE ATENDIMENTO</td>
                    </tr>
                    <tr>
                        <td> Data </td>
                        <td> De </td>
                        <td> Para </td>
                        <td> Mensagem </td>
                    </tr>
                    <?php
                    $sql = "SELECT id,data,de.login_user,de.id_usuario,para.login_user login_userp,para.id_usuario id_usuariop,mensagem
                    FROM sf_mensagens_telemarketing INNER JOIN sf_usuarios de ON de.id_usuario = sf_mensagens_telemarketing.id_usuario_de
                    LEFT JOIN sf_usuarios para ON para.id_usuario = sf_mensagens_telemarketing.id_usuario_para
                    WHERE id_tele = " . $id . " order by data desc";
                    $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                    while ($RFP = odbc_fetch_array($cur)) {
                        echo '<tr><td style="width: 131px;border-right: 0;border-bottom: 0px;border-top: 0px;">' . escreverDataHora($RFP['data']) . '</td>' .
                        '<td style="width: 93px;border:0">' . utf8_encode($RFP['login_user']) . '</td>' .
                        '<td style="width: 134px;border:0">' . utf8_encode($RFP['login_userp']) . '</td>' .
                        '<td style="border-left:0;border-bottom: 0px;border-top: 0px;">' . utf8_encode($RFP['mensagem']) . '</td></tr>';
                    } ?>
                    <tr><td style="border-top: 0;" colspan="4"></td></tr>
                </table>
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td colspan="5" class="titulo">GARANTIA</td>
                    </tr>
                    <tr>
                        <td style="width:16.5%"><b>Garantia?</b></td>
                        <td style="width:16.5%"><?php echo ($garantia == 1 ? "Sim" : "Não"); ?></td>
                        <td style="width:34%">Nota Fiscal Nº / Data</td>
                        <td style="width:16.5%"><?php echo $nota_fiscal; ?></td>
                        <td style="width:16.5%"><?php echo $data_nota; ?></td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td colspan="2" class="titulo">Condições Comerciais</td>
                    </tr>
                    <tr>
                        <td style="width:33%">Previsão para o Orçamento:</td>
                        <td style="width:67%; text-align:center"><?php echo $dias; ?> Dias Úteis</td>
                    </tr>
                    <tr>
                        <td>Taxa de Orçamento:</td>
                        <td style="text-align:center"><?php echo escreverNumero($taxaOrc); ?></td>
                    </tr>
                    <tr>
                        <td>Técnico Responsável:</td>
                        <td style="text-align:center">
                            <?php
                            $cur = odbc_exec($con, "select id_usuario,login_user,nome from sf_usuarios where id_usuario = '" . $para . "' order by nome") or die(odbc_errormsg());
                            while ($RFP = odbc_fetch_array($cur)) {
                                echo utf8_encode($RFP['nome']);
                            } ?>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="titulo">SOLUÇÃO</td>
                    </tr>
                    <tr>
                        <td style="height:70px"><?php echo $obsFechamento; ?></td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="height:50px; width:33%"></td>
                        <td style="height:50px; width:34%"></td>
                        <td style="height:50px; width:33%"></td>
                    </tr>
                    <tr>
                        <td style="text-align:center">Visto Atendente</td>
                        <td style="text-align:center">Visto Superior</td>
                        <td style="text-align:center">Visto <?php echo $adm_associado;?></td>
                    </tr>
                    <tr>
                        <td style="height:25px; text-align:center; vertical-align:bottom">____/____/________</td>
                        <td style="height:25px; text-align:center; vertical-align:bottom">____/____/________</td>
                        <td style="height:25px; text-align:center; vertical-align:bottom">____/____/________</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align:right">Página 1/1</td>
                    </tr>
                </table>
                <div class="rodapeOrdem">Gerado pelo sistema SIVIS Finance - www.sivis.com.br</div>
            </div>
        <?php } ?>
    </body>
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type="text/javascript">
        $(window).load(function () {
            window.print();
            setTimeout(function () {
                window.close();
            }, 500);
        });
    </script>    
    <?php odbc_close($con); ?>    
</html>