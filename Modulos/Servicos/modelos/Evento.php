<?php
$PegaURL = "";
$contrato2 = "";
$adm_associado = "Cliente";

if ($_GET["id"] != "") {
    if (is_numeric($_GET["id"])) {
        $PegaURL = $_GET["id"];
    }
}
if ($_GET["crt"] != "") {
    if (is_numeric($_GET["crt"])) {
        $contrato2 = $_GET["crt"];
    }
}

$cur = odbc_exec($con, "select * from dbo.sf_configuracao");
while ($RFP = odbc_fetch_array($cur)) {
    $adm_associado = utf8_encode($RFP['adm_associado']);    
}

if ($PegaURL != "" && $contrato2 != "") {
    $cur = odbc_exec($con, "select id_tele,u.nome login_user,para_pessoa,data_tele,pendente,proximo_contato,t.obs,id_fornecedores_despesas,tipo,
    us.login_user login_user_fechamento,data_tele_fechamento,obs_fechamento,especificacao, id_especificacao,
    garantia,atendimento_servico,nota_fiscal,data_nf,valor_orcamento,relato,dias_garantia,id_venda,id_veiculo,empresa 
    from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa
    LEFT JOIN sf_usuarios us on us.id_usuario = t.de_pessoa_fechamento
    LEFT JOIN sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = t.pessoa where id_tele = " . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_tele']);
        $data = escreverData($RFP['proximo_contato']);
        $ano = escreverData($RFP['data_tele'], 'Y');
        $login_user = utf8_encode($RFP['login_user']);
        $garantia = utf8_encode($RFP['garantia']);
        $nota_fiscal = utf8_encode($RFP['nota_fiscal']);
        $data_nota = escreverData($RFP['data_nf']);
        $taxaOrc = escreverNumero($RFP['valor_orcamento']);
        $obs = utf8_encode($RFP['obs']);
        $relato = utf8_encode($RFP['relato']);
        $obsFechamento = utf8_encode($RFP['obs_fechamento']);
        $para = utf8_encode($RFP['para_pessoa']);
        $contato = utf8_encode($RFP['id_fornecedores_despesas']);
        $empresa = utf8_encode($RFP["empresa"]);
        $dias = utf8_encode($RFP['dias_garantia']);
        $id_venda = utf8_encode($RFP['id_venda']);
        $id_veiculo = utf8_encode($RFP['id_veiculo']);
        $id_especificacao = utf8_encode($RFP['id_especificacao']);
    }
    $totProdutos = 0;
    $totServicos = 0;
    if ($id_venda !== "") {
        $cur = odbc_exec($con, "select sum(case when cm.tipo = 'P' then 1 else 0 end) Produtos,
        sum(case when cm.tipo = 'S' then 1 else 0 end) Servicos from sf_vendas_itens vi left join sf_contas_movimento gc on vi.grupo = gc.id_contas_movimento inner join sf_produtos cm
        on cm.conta_produto = vi.produto inner join sf_vendas v on v.id_venda = vi.id_venda where
        vi.id_venda = " . $id_venda);
        while ($RFP = odbc_fetch_array($cur)) {
            $totProdutos = $RFP["Produtos"];
            $totServicos = $RFP["Servicos"];
        }
    }
} else {
    $id = "";
    $data = "";
    $ano = "";
    $login_user = "";
    $garantia = "";
    $nota_fiscal = "";
    $data_nota = "";
    $taxaOrc = escreverNumero(0);
    $obs = "";
    $relato = "";
    $obsFechamento = "";
    $para = "";
    $contato = "";
    $dias = "";
    $id_veiculo = "";
    $id_especificacao = "";
}

if (is_numeric($empresa)) {
    $cur = odbc_exec($con, "select * from sf_filiais where numero_filial = " . $empresa);
    while ($RFP = odbc_fetch_array($cur)) {
        $cpf_cnpj = utf8_encode($RFP["cnpj"]);
        $cedente = utf8_encode($RFP["razao_social_contrato"]);
        $nome_fantasia = utf8_encode($RFP["nome_fantasia_contrato"]);
        $inscricao = utf8_encode($RFP["inscricao_estadual"]);
        $endereco = utf8_encode($RFP['endereco']) . " n°" . utf8_encode($RFP['numero']) . "," . utf8_encode($RFP['bairro']);
        $cidade_uf = utf8_encode($RFP['cidade_nome']) . " " . utf8_encode($RFP['estado']);
        $cep = utf8_encode($RFP['cep']);
        $telefone = utf8_encode($RFP['telefone']);
        $site = utf8_encode($RFP['site']);
    }
}

?>
<html>	
    <head>
        <title>SIVIS - Evento</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
            body {
                margin:0px;
                padding:0px;
            }
            table {
                font: 10px Calibri;
                margin-bottom: 10px;
                border: 1px solid #999;
                border-collapse: collapse;
                vertical-align: middle;
            }
            .titulo {
                font: 14px Calibri;
                font-weight: Bold;
                padding: 2px;
                padding-left: 5px;
                text-align: center;
                background-color: lightgrey;
            }
        </style>
    </head>
    <body>
        <div>
            <table width="100%" border="1" cellpadding="2" cellspacing="0">
                <tr>
                    <td width="500px" rowspan="3">
                        <table width="100%" cellpadding="0" border="0" cellspacing="0" style="border:0px solid white;">
                            <tr>
                                <td width="200px">
                                    <div style="line-height: 1px;"></div>
                                    <?php if (file_exists("./../Pessoas/" . $_SESSION["contrato"] . "/Empresa/logo_" . $filial_orc . ".jpg")) { ?>
                                        <img style="width: 160px;" src ="./../Pessoas/<?php echo $_SESSION["contrato"]; ?>/Empresa/logo_<?php echo $filial_orc; ?>.jpg" align="middle" />
                                    <?php } else if (file_exists("./../Pessoas/" . $_SESSION["contrato"] . "/Empresa/logo_" . $filial_orc . ".png")) { ?>
                                        <img style="width: 160px;" src ="./../Pessoas/<?php echo $_SESSION["contrato"]; ?>/Empresa/logo_<?php echo $filial_orc; ?>.png" align="middle" />                                        
                                    <?php } ?>
                                </td>
                                <td width="285px"><span style="text-align:right;font-size: 8px;">
                                    <div style="line-height: 0px;"></div>
                                    <?php echo (strlen($cedente) > 0 ? $cedente . "<br>" : ""); ?>
                                    <?php echo (strlen($endereco) > 0 ? $endereco . "<br>" : ""); ?>
                                    <?php echo (strlen($cidade_uf) > 0 ? $cidade_uf : ""); ?>
                                    <?php echo (strlen($cep) > 0 ? " - CEP: " . $cep : ""); ?>
                                    <br>
                                    <?php echo (strlen($telefone) > 0 ? $telefone . "<br>" : ""); ?>
                                    <?php echo (strlen($site) > 0 ? $site . "<br>" : ""); ?></span>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="200px" colspan="2" align="center" style="line-height: 25px;font-size:16px;"><b>ABERTURA DE EVENTO</b></td>
                </tr>
                <tr>
                    <td width="100px" style="padding-left:5px">Nº:</td>
                    <td width="100px" align="center"><?php echo $id . "/" . $ano; ?></td>
                </tr>
                <tr>
                    <td style="padding-left:5px">Data Prazo:</td>
                    <td align="center"><?php echo $data; ?></td>
                </tr>
            </table>
            <br><br>
            <table width="100%" border="1" cellspacing="0">
                <tr style="">
                    <td class="titulo">Associado</td>
                </tr>
                <tr>
                    <td><?php
                        $cur = odbc_exec($con, "select razao_social,endereco,numero,complemento,bairro,cidade_nome,estado_sigla,cep,cnpj,inscricao_estadual,juridico_tipo 
                        from sf_fornecedores_despesas left join tb_estados on tb_estados.estado_codigo = sf_fornecedores_despesas.estado 
                        left join tb_cidades on tb_cidades.cidade_codigo = sf_fornecedores_despesas.cidade 
                        where id_fornecedores_despesas = '" . $contato . "' order by razao_social") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) {
                            echo utf8_encode($RFP["razao_social"]) . "<br>" . utf8_encode($RFP["endereco"]) . "," . utf8_encode($RFP["numero"]) . " " . utf8_encode($RFP["complemento"]) . " - Bairro: " . utf8_encode($RFP["bairro"]) . " - " . utf8_encode($RFP["cidade_nome"]) . " - " . utf8_encode($RFP["estado_sigla"]) . " - CEP: " . utf8_encode($RFP["cep"]) . "<br>" . 
                            ($RFP["juridico_tipo"] == "J" ? "CNPJ" : "CPF") . ": " . utf8_encode($RFP["cnpj"]) . " * " . 
                            ($RFP["juridico_tipo"] == "J" ? "IE" : "Identidade") . ": " . utf8_encode($RFP["inscricao_estadual"]);
                        } ?>
                    </td>
                </tr>
                <tr style="">
                    <td class="titulo">Veículo</td>
                </tr>                
                <tr>
                    <td>
                        <?php if (is_numeric($id_veiculo)) {
                            $cur = odbc_exec($con, "select placa, marca, modelo, ano_modelo, renavam, chassi 
                            from sf_fornecedores_despesas_veiculo where id = " . $id_veiculo);
                            while ($RFP = odbc_fetch_array($cur)) {
                                echo "<br>Placa: " . utf8_encode($RFP['placa']) . " (" . utf8_encode($RFP['marca']) . ") " . utf8_encode($RFP['modelo']) . " - " . utf8_encode($RFP['ano_modelo']) . "<br/>";
                                echo "Renavam: " . utf8_encode($RFP['renavam']) . " - Chassi: " . utf8_encode($RFP['chassi']);
                            }
                        } ?>
                    </td>
                </tr>
                <tr>
                    <td><b>Responsável:  </b><span style="margin-left: 8px;"><?php echo $login_user; ?></span></td>
                </tr>
                <tr>
                    <td class="titulo">Ação do Evento</td>
                </tr>
                <tr>
                    <td style="height:30px"><?php
                    if (is_numeric($id_especificacao)) {
                        $cur = odbc_exec($con, "select id_especificacao, nome_especificacao 
                        from sf_telemarketing_especificacao where id_especificacao = " . $id_especificacao . " order by nome_especificacao");
                        while ($RFP = odbc_fetch_array($cur)) {
                            echo utf8_encode($RFP['nome_especificacao']) . "<br/>";
                        } 
                    }?>
                    </td>
                </tr>
                <tr>
                    <td class="titulo">Descrição da Ocorrência</td>
                </tr>
                <tr>
                    <td style="height:30px"><?php echo str_replace("\n", "<br>", $relato); ?></td>
                </tr>
                <tr>
                    <td class="titulo">Observação da Ocorrência</td>
                </tr>
                <tr>
                    <td style="height:30px"><?php echo str_replace("\n", "<br>", $obs); ?></td>
                </tr>                
                <tr>
                    <td class="titulo">Solução</td>
                </tr>
                <tr>
                    <td style="height:30px"><?php echo str_replace("\n", "<br>", $obsFechamento); ?></td>
                </tr>
                <tr>
                    <td class="titulo">Histórico de Atendimento</td>
                </tr>
                <?php
                $sql = "SELECT id,data,de.login_user,de.id_usuario,para.login_user login_userp,para.id_usuario id_usuariop,mensagem
                FROM sf_mensagens_telemarketing INNER JOIN sf_usuarios de ON de.id_usuario = sf_mensagens_telemarketing.id_usuario_de
                LEFT JOIN sf_usuarios para ON para.id_usuario = sf_mensagens_telemarketing.id_usuario_para
                WHERE id_tele = " . $id . " and privado = 0 order by data desc";
                $cur = odbc_exec($con, $sql) or die(odbc_errormsg());
                while ($RFP = odbc_fetch_array($cur)) {
                    echo "<tr><td>" . escreverDataHora($RFP['data']) . " De: " . utf8_encode($RFP['login_user']) . " Para: " . utf8_encode($RFP['login_userp']) . " </td></tr>
                    <tr><td style=\"height: 30px;\">" . str_replace("\n", "<br>", utf8_encode($RFP['mensagem'])) . "</td></tr>";
                } ?>
            </table>
            <br><br>
            <?php if ($totProdutos > 0) { ?>
                <table width="700px" border="1">
                    <tr style="background-color: lightgrey">
                        <td class="titulo" colspan="6">Produtos</td>
                    </tr>
                    <tr>
                        <td width="33px" align="center"><b>Item</b></td>
                        <td width="58px" align="center"><b>Qde.</b></td>
                        <td width="35px" align="center"><b>Und.</b></td>
                        <td width="374px" align="center"><b>Descrição</b></td>
                        <td width="100px" align="center"><b>Val.Unit.</b></td>
                        <td width="100px" align="center"><b>Val.Total</b></td>
                    </tr>
                    <?php
                    if ($id_venda != "") {
                        $loop = 1;
                        $sub_total = 0;
                        $cur = odbc_exec($con, "select id_item_venda,v.conta_movimento grupo,gc.descricao grupodesc,conta_produto produto, cm.descricao, cm.desc_prod as produtodesc,
                        quantidade,valor_total,descontop,descontos,descontotp,descontots, unidade_comercial  from sf_vendas_itens vi left join sf_contas_movimento gc on vi.grupo = gc.id_contas_movimento inner join sf_produtos cm
                        on cm.conta_produto = vi.produto inner join sf_vendas v on v.id_venda = vi.id_venda where cm.tipo = 'P' and vi.id_venda = " . $id_venda);
                        while ($RFP = odbc_fetch_array($cur)) {
                            $valor_unitario = $RFP["valor_total"] / $RFP["quantidade"];
                            $sub_total = $sub_total + $RFP["valor_total"];
                            if ($RFP["descontotp"] == 0) {
                                $desc_valor = $sub_total * ($RFP["descontop"] / 100);
                                $desconto = "%";
                            } else {
                                $desc_valor = $RFP["descontop"];
                                $desconto = "$";
                            }
                            ?>
                            <tr>
                                <td class="tabela" align="center"><?php echo $loop; ?></td>
                                <td class="tabela" align="center"><?php echo escreverNumero($RFP["quantidade"]); ?></td>
                                <td class="tabela" align="center"><?php echo $RFP["unidade_comercial"]; ?></td>
                                <td class="tabela"><?php
                                    echo utf8_encode($RFP["descricao"]);
                                    if ($tipo == "O") {
                                        ?><span style="font-size: 8px;"><?php echo str_replace("<em>", "", str_replace("</em>", "", utf8_encode($RFP["produtodesc"]))); ?></span><?php } ?></td>
                                <td class="tabela"><div style="text-align:right"><?php echo escreverNumero($valor_unitario); ?></div></td>
                                <td class="tabela"><div style="text-align:right"><?php echo escreverNumero($RFP["valor_total"]); ?></div></td>
                            </tr>
                            <?php
                            $loop++;
                        }
                    }
                    ?>
                    <tr>
                        <td class="tabela" colspan="4"></td>
                        <td class="tabela">Subtotal</td>
                        <td class="tabela"><div style="text-align:right;"><?php echo escreverNumero($sub_total, 1); ?></div></td>
                    </tr>
                    <tr>
                        <td class="tabela" colspan="3">Garantia:</td>
                        <td class="tabela"><?php echo $garantia_prod; ?></td>
                        <td class="tabela">Desconto (<?php echo $desconto; ?>)</td>
                        <td class="tabela"><div style="text-align:right;"><?php echo escreverNumero($desc_valor, 1); ?></div></td>
                    </tr>
                    <tr>
                        <td class="tabela" colspan="4" style="border-bottom: none;"></td>
                        <td class="tabela" style="border-bottom: none;"><b>Total Produto</b></td>
                        <td class="tabela" style="border-bottom: none;"><div style="text-align:right;"><b><?php echo escreverNumero(($sub_total - $desc_valor), 1); ?></b></div></td>
                    </tr>
                    <?php $TotalProduto = $sub_total - $desc_valor; ?>
                </table>
            <?php } if ($totServicos > 0) { ?>
                <table width="700px" border="1">
                    <tr style="background-color: lightgrey">
                        <td class="titulo" colspan="6" >Serviços</td>
                    </tr>
                    <tr>
                        <td width="33px" align="center"><b>Item</b></td>
                        <td width="58px" align="center"><b>Qde.</b></td>
                        <td width="35px" align="center"><b>Und.</b></td>
                        <td width="374px" align="center"><b>Descrição</b></td>
                        <td width="100px" align="center"><b>Val.Unit.</b></td>
                        <td width="100px" align="center"><b>Val.Total</b></td>
                    </tr>
                    <?php
                    if ($id_venda != "") {
                        $loop = 1;
                        $sub_total = 0;
                        $cur = odbc_exec($con, "select id_item_venda,v.conta_movimento grupo,gc.descricao grupodesc,conta_produto produto,cm.descricao, cm.desc_prod as produtodesc,
                        quantidade,valor_total,descontop,descontos,descontotp,descontots, unidade_comercial from sf_vendas_itens vi left join sf_contas_movimento gc on vi.grupo = gc.id_contas_movimento inner join sf_produtos cm
                        on cm.conta_produto = vi.produto inner join sf_vendas v on v.id_venda = vi.id_venda where cm.tipo = 'S' and vi.id_venda = " . $id_venda);
                        while ($RFP = odbc_fetch_array($cur)) {
                            $valor_unitario = $RFP["valor_total"] / $RFP["quantidade"];
                            $sub_total = $sub_total + $RFP["valor_total"];
                            if ($RFP["descontots"] == 0) {
                                $desc_valor = $sub_total * ($RFP["descontos"] / 100);
                                $desconto = "%";
                            } else {
                                $desc_valor = $RFP["descontos"];
                                $desconto = "$";
                            }
                            ?>
                            <tr>
                                <td class="tabela" align="center"><?php echo $loop; ?></td>
                                <td class="tabela" align="center"><?php echo escreverNumero($RFP["quantidade"]); ?></td>
                                <td align="center"><?php echo $RFP["unidade_comercial"]; ?></td>
                                <td class="tabela"><?php
                                    echo utf8_encode($RFP["descricao"]);
                                    if ($tipo == "O") {
                                        ?><span style="font-size: 8px;"><?php echo str_replace("<em>", "", utf8_encode($RFP["produtodesc"])); ?></span><?php } ?></td>
                                <td class="tabela"><div style="text-align:right"><?php echo escreverNumero($valor_unitario); ?></div></td>
                                <td class="tabela"><div style="text-align:right"><?php echo escreverNumero($RFP["valor_total"]); ?></div></td>
                            </tr>
                            <?php
                            $loop++;
                        }
                    }
                    ?>
                    <tr>
                        <td class="tabela" colspan="4"></td>
                        <td class="tabela">Subtotal</td>
                        <td class="tabela"><div style="text-align:right"><?php echo escreverNumero($sub_total, 1); ?></div></td>
                    </tr>
                    <tr>
                        <td class="tabela" colspan="3">Garantia:</td>
                        <td class="tabela"><?php echo $garantia_serv; ?></td>
                        <td class="tabela">Desconto (<?php echo $desconto; ?>)</td>
                        <td class="tabela"><div style="text-align:right"><?php echo escreverNumero($desc_valor, 1); ?></div></td>
                    </tr>
                    <tr>
                        <td class="tabela" colspan="4"></td>
                        <td class="tabela"><b>Total Serviço</b></td>
                        <td class="tabela"><div style="text-align:right"><b><?php echo escreverNumero($sub_total - $desc_valor, 1); ?></b></div></td>
                    </tr>
                    <?php $TotalServico = $sub_total - $desc_valor; 
                    if ($n_servicos == 1) {
                        $TotalGeral = $TotalProduto;
                    } else {
                        $TotalGeral = $TotalProduto + $TotalServico;
                    }
                    ?>
                    <tr>
                        <td style="background-color: lightgrey;" colspan="5"><b>Total Geral</b></td>
                        <td style="background-color: lightgrey;text-align: right;" ><b><?php echo escreverNumero($TotalGeral, 1); ?></b></td>
                    </tr>
                </table>
                <br><br>
            <?php } ?>            
            <table width="100%" border="1" cellspacing="0">
                <tr>
                    <td colspan="2" class="titulo">Condições Comerciais</td>
                </tr>
                <tr>
                    <td>Técnico Responsável:</td>
                    <td style="text-align:center">
                        <?php $cur = odbc_exec($con, "select id_usuario,login_user,nome from sf_usuarios where id_usuario = '" . $para . "' order by nome") or die(odbc_errormsg());
                        while ($RFP = odbc_fetch_array($cur)) {
                            echo utf8_encode($RFP['nome']);
                        }?>
                    </td>
                </tr>
            </table>
            <br><br>
            <table width="100%" border="1" cellspacing="0">
                <tr>
                    <td style="height:50px; width:33%"></td>
                    <td style="height:50px; width:34%"></td>
                    <td style="height:50px; width:33%"></td>
                </tr>
                <tr>
                    <td style="text-align:center">Visto Atendente</td>
                    <td style="text-align:center">Visto Superior</td>
                    <td style="text-align:center">Visto <?php echo $adm_associado;?></td>
                </tr>
                <tr>
                    <td style="height:25px; text-align:center; vertical-align:bottom">____/____/________</td>
                    <td style="height:25px; text-align:center; vertical-align:bottom">____/____/________</td>
                    <td style="height:25px; text-align:center; vertical-align:bottom">____/____/________</td>
                </tr>
            </table>
            <div style="font-size: 10px;margin-top: 15px; ">Gerado pelo sistema SIVIS Finance - www.sivis.com.br</div>
        </div>
    </body>
</html>