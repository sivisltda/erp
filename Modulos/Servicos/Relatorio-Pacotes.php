<?php
include './../../Connections/configini.php';

$imprimir = 0;
$F1 = 1;
$F2 = "";
$F3 = 0;
$F4 = 0;
$descricao = "";
$DateBegin = "";
$DateEnd = "";

if (is_numeric($_GET["imp"]) && $_GET["imp"] > 0) {
    $imprimir = 1;
}
if (is_numeric($_GET["tp"])) {
    $F1 = $_GET["tp"];
}
if (is_numeric($_GET["ts"])) {
    $F2 = $_GET["ts"];
    $cur = odbc_exec($con, "select * from sf_fornecedores_despesas where id_fornecedores_despesas = " . $F2);
    while ($RFP = odbc_fetch_array($cur)) {
        $descricao = utf8_encode($RFP["razao_social"]);
    }
}
if (is_numeric($_GET["cm"])) {
    $F3 = $_GET["cm"];
}
if (is_numeric($_GET["tf"])) {
    $F4 = $_GET["tf"];
}
if ($_GET["dti"] != "") {
    $DateBegin = str_replace("_", "/", $_GET["dti"]);
}
if ($_GET["dtf"] != "") {
    $DateEnd = str_replace("_", "/", $_GET["dtf"]);
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>                
        <style>
            #example th { padding: 0 10px; }
            #example td { padding: 5px 10px; }
        </style>
    </head>
    <body>
        <?php if ($imprimir == 0) { ?>
            <div id="loader"><img src="../../img/loader.gif"/></div>
        <?php } ?>
        <div class="wrapper">
            <?php
            if ($imprimir == 0) {
                include("../../menuLateral.php");
            }
            ?>
            <div <?php
            if ($imprimir == 0) {
                echo "class=\"body\"";
            }
            ?>>
                    <?php
                    if ($imprimir == 0) {
                        include("../../top.php");
                    }
                    ?>
                <div class="content">
                    <?php
                    if ($imprimir !== 0) {
                        $visible = "hidden";
                    }
                    ?>
                    <div class="page-header" <?php echo $visible; ?>>
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Financeiro<small>Relatório de Pacotes</small></h1>
                    </div>
                    <div class="row-fluid" <?php echo $visible; ?>>
                        <div class="span12">
                            <div class="block" style="font-size:13px">
                                <button name="btnPrint" class="button button-blue" type="button" onClick="AbrirBox(1)"><span class="ico-print"></span></button>
                                <select name="txtEstado" id="txtEstado" class="input-medium" style="width:110px; height:31px">
                                    <option value="0" <?php
                                    if ($F1 == 0) {
                                        echo "SELECTED";
                                    }
                                    ?>>Todos</option>
                                    <option value="1" <?php
                                    if ($F1 == 1) {
                                        echo "SELECTED";
                                    }
                                    ?>>Em Aberto</option>
                                    <option value="2" <?php
                                    if ($F1 == 2) {
                                        echo "SELECTED";
                                    }
                                    ?>>Concluídos</option>
                                </select>
                                <select name="txtPacote" id="txtPacote" class="input-medium" style="width:200px; height:31px">
                                    <option value="0" <?php
                                    if ($F3 == 0) {
                                        echo "SELECTED";
                                    }
                                    ?>>Todos</option>
                                            <?php
                                            $cur = odbc_exec($con, "select distinct conta_produto,descricao from sf_produtos_materiaprima 
                                                                  inner join sf_produtos on sf_produtos.conta_produto = sf_produtos_materiaprima.id_produtomp");
                                            while ($RFP = odbc_fetch_array($cur)) {
                                                ?>
                                        <option value="<?php echo utf8_encode($RFP["conta_produto"]); ?>" <?php
                                        if ($F3 == $RFP["conta_produto"]) {
                                            echo "SELECTED";
                                        }
                                        ?>><?php echo utf8_encode($RFP["descricao"]); ?></option>
                                            <?php } ?>
                                </select>
                                <select name="txtFuncionario" id="txtFuncionario" class="input-medium" style="width:175px; height:31px">
                                    <option value="0" <?php
                                    if ($F3 == 0) {
                                        echo "SELECTED";
                                    }
                                    ?>>Todos</option>
                                            <?php
                                            if (is_numeric($filial)) {
                                                $cur = odbc_exec($con, "select id_fornecedores_despesas login_user,razao_social nome from sf_fornecedores_despesas f
                                                                where f.inativo = 0 and f.tipo = 'E' and (tbcomiss_prod <> 0 or tbcomiss_serv <> 0) order by razao_social");
                                                while ($RFP = odbc_fetch_array($cur)) {
                                                    ?>
                                            <option value="<?php echo utf8_encode($RFP["login_user"]); ?>" <?php
                                            if ($F4 == $RFP["login_user"]) {
                                                echo "SELECTED";
                                            }
                                            ?>><?php echo utf8_encode($RFP["nome"]); ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                </select>
                                <input id="txtId" name="txtId" type="hidden" value="<?php echo $F2; ?>"/>
                                <input id="txtDesc" name="txtDesc" type="text" class="inputbox" style="width:180px; height:31px" value="<?php echo $descricao; ?>" size="10"/>
                                de <input type="text" class="datepicker" style="width:80px; height:31px" id="txt_dt_begin" name="txt_dt_begin" value="<?php echo $DateBegin; ?>" placeholder="Data Inicial"/> 
                                até <input type="text" class="datepicker" style="width:80px; height:31px" id="txt_dt_end" name="txt_dt_end" value="<?php echo $DateEnd; ?>" placeholder="Data Final"/> 
                                <button name="btnfind" id="btnfind" class="button button-turquoise" type="button" onClick="AbrirBox(0)"><span class="ico-search icon-white"></span></button>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead" <?php echo $visible; ?>>
                        <div class="boxtext">Relatório de Pacotes</div>
                    </div>
                    <div <?php
                    if ($imprimir == 0) {
                        echo "class=\"boxtable\"";
                    }
                    ?>>
                            <?php
                            if ($imprimir == 1) {
                                $titulo_pagina = "Relatório de Pacotes - Loja " . $filial;
                                include "../Financeiro/Cabecalho-Impressao.php";
                            }
                            ?>
                        <table <?php
                        if ($imprimir == 0) {
                            echo "class=\"table\"";
                        } else {
                            echo "border=\"1\"";
                        }
                        ?> style="float:none" cellpadding="0" cellspacing="0" width="100%" id="example">
                            <?php
                            $cliente = "0";
                            $venda = "0";
                            if ($DateBegin != "" && $DateEnd != "") {
                                $where = " AND v.data_venda BETWEEN " . valoresDataHora2($DateBegin, "00:00:00") . " AND " . valoresDataHora2($DateEnd, "23:59:59") . " ";
                            }
                            if (is_numeric($filial)) {
                                $where .= " AND v.empresa = " . $filial;
                            }
                            if (is_numeric($F1) && $F1 > 0) {
                                if ($F1 == 1) {
                                    $where .= " AND (select COUNT(*) from sf_vendas_itens_pacotes pc where pc.venda_item = p.venda_item and pc.venda_aplicada is null) > 0 ";
                                } else if ($F1 == 2) {
                                    $where .= " AND (select COUNT(*) from sf_vendas_itens_pacotes pc where pc.venda_item = p.venda_item and pc.venda_aplicada is null) = 0 ";
                                }
                            }
                            if (is_numeric($F2) && $F2 > 0) {
                                $where .= " AND v.cliente_venda = " . $F2;
                            }
                            if (is_numeric($F3) && $F3 > 0) {
                                $where .= " AND p1.conta_produto = " . $F3;
                            }
                            if (is_numeric($F4) && $F4 > 0) {
                                $where .= " AND vi.vendedor_comissao = " . $F4;
                            }
                            $query = "set dateformat dmy;select v.id_venda,v.data_venda,v.cliente_venda,c.razao_social,p1.descricao d1,p2.descricao d2,va.data_venda data_aplicada,f.razao_social comissionado 
                                      from sf_vendas_itens_pacotes p 
                                      inner join sf_vendas_itens vi on p.venda_item = vi.id_item_venda 
                                      inner join sf_vendas v on vi.id_venda = v.id_venda 
                                      inner join sf_fornecedores_despesas c on c.id_fornecedores_despesas = v.cliente_venda 
                                      inner join sf_produtos_materiaprima mp on mp.id_prodmp = p.produto_usar
                                      inner join sf_produtos p1 on mp.id_produtomp = p1.conta_produto 
                                      inner join sf_produtos p2 on mp.id_materiaprima = p2.conta_produto
                                      left join sf_fornecedores_despesas f on f.id_fornecedores_despesas = vi.vendedor_comissao
                                      left join sf_vendas_itens via on p.venda_aplicada = via.id_item_venda
                                      left join sf_vendas va on via.id_venda = va.id_venda 
                                      where v.status <> 'Reprovado' " . $where . " order by id_venda,d1,d2";
                            $cur = odbc_exec($con, $query);
                            while ($RFP = odbc_fetch_array($cur)) {
                                if ($cliente != $RFP["cliente_venda"]) {
                                    ?>
                                    <?php if ($cliente != "0") { ?>
                                        <tr>
                                            <td style="background:#FFF" colspan="3">&nbsp;</td>
                                        </tr>
                                    <?php } ?>
                                    <thead>
                                        <tr>
                                            <th colspan="3"><?php
                                                echo utf8_encode($RFP["razao_social"]);
                                                $cliente = $RFP["cliente_venda"];
                                                ?></th>
                                        </tr>
                                    </thead>
                                    <?php
                                } if ($venda != $RFP["id_venda"]) {
                                    $venda = $RFP["id_venda"];
                                    ?>
                                    <tr>
                                        <td style="width:40%"><strong><?php echo escreverData($RFP["data_venda"]) . " - " . utf8_encode($RFP["d1"]); ?></strong></td>
                                        <td><strong><?php echo utf8_encode($RFP["comissionado"]); ?></strong></td>
                                        <td style="width:20%; text-align:right"><strong>Data Aplicação</strong></td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td><?php echo " >>" . utf8_encode($RFP["d2"]); ?></td>
                                    <td></td>
                                    <td style="text-align:right"><?php echo escreverData($RFP["data_aplicada"]); ?></td>
                                </tr>
                            <?php } ?>
                            <thead>
                                <tr>
                                    <th colspan="3">&nbsp;</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/jquery.mousewheel.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            
            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
            
            function AbrirBox(id) {
                var retPrint = "";
                if ($("#txtEstado").val() !== "") {
                    retPrint = retPrint + "&tp=" + $("#txtEstado").val();
                }
                if ($("#txtId").val() !== "" && $("#txtDesc").val() !== "") {
                    retPrint = retPrint + "&ts=" + $("#txtId").val();
                }
                if ($("#txtPacote").val() !== "") {
                    retPrint = retPrint + "&cm=" + $("#txtPacote").val();
                }
                if ($("#txtFuncionario").val() !== "") {
                    retPrint = retPrint + "&tf=" + $("#txtFuncionario").val();
                }
                if ($("#txt_dt_begin").val() !== "") {
                    retPrint = retPrint + "&dti=" + $("#txt_dt_begin").val().replace(/\//g, "_");
                }
                if ($("#txt_dt_end").val() !== "") {
                    retPrint = retPrint + "&dtf=" + $("#txt_dt_end").val().replace(/\//g, "_");
                }
                if (id === 0) {
                    window.location = "Relatorio-Pacotes.php?imp=0" + retPrint;
                } else if (id === 1) {
                    window.location = "Relatorio-Pacotes.php?imp=1" + retPrint;
                }
            }
            $(function () {
                $("#txtDesc").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "./../CRM/ajax.php",
                            dataType: "json",
                            data: {
                                q: request.term,
                                t: "C"
                            },
                            success: function (data) {
                                response(data);
                            }
                        });
                    },
                    minLength: 3,
                    select: function (event, ui) {
                        $("#txtId").val(ui.item.id);
                    }
                });
            });
        </script>        
    </body>
    <?php if ($imprimir == 1) { ?>
        <script type="text/javascript">
            $(window).load(function () {
                $(".body").css("margin-left", 0);
                window.print();
                window.history.back();
            });
        </script>
    <?php } odbc_close($con); ?>
</html>