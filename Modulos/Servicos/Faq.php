<?php
    include './../../Connections/configini.php';
    $titulo_artigo = "Ajuda";
    $SelectEsp = "";
    $finalURL = "";    
    
    if (is_numeric($_GET['id'])) {
        $cur = odbc_exec($con, "select * from sf_faq left join sf_usuarios on sf_usuarios.id_usuario = sf_faq.usuario left join sf_telemarketing_especificacao on sf_telemarketing_especificacao.id_especificacao = sf_faq.especificacao where id_faq =" . $_GET['id']);
        while ($RFP = odbc_fetch_array($cur)) {
            $titulo_artigo = utf8_encode($RFP['titulo']);
            $SelectEsp = utf8_encode($RFP['nome_especificacao'])." - ";
        }
        $finalURL = "?id=".$_GET['id'];
        odbc_exec($con, "UPDATE sf_faq SET acessos = acessos + 1 WHERE id_faq =" . $_GET['id']);
    }    
    
    if (is_numeric($_GET['Del'])) {
        odbc_exec($con, "DELETE FROM sf_faq WHERE id_faq =" . $_GET['Del']);
        if (is_numeric($_GET['id'])) {
            $final = "?id=".$_GET['id'];
        }
        header("Location: Faq.php".$final."");
    }    
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>    
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>           
    </head>
    <body>    
        <div id="loader"><img src="../../img/loader.gif"/></div>
        <div class="wrapper">        
            <?php include('../../menuLateral.php'); ?>
            <div class="body">
                <?php include("../../top.php"); ?>
                <div class="content">                
                    <div class="page-header">
                        <div class="icon">
                            <span class="ico-question-sign" style="line-height:34px"></span>
                        </div>
                        <h1>Ajuda <small>Dúvidas Frequentes</small></h1>
                    </div>
                    <div class="row-fluid">
                        <div  <?php if (!is_numeric($_GET['id'])) { echo "class=\"span8\"";} ?>>
                            <div class="block">                                                                
                                <div class="boxhead">
                                    <div class="boxtext">
                                        <?php if(is_numeric($_GET['id'])) { ?>
                                            <button class="button button-blue btn-primary" style="float: left;margin-top: 2px;" type="button" onClick="window.location.href ='Faq.php'">
                                                <span class="ico-angle-left icon-white"></span>
                                            </button>                                        
                                        <?php } echo $SelectEsp.$titulo_artigo;?>                                        
                                        <button class="button button-green btn-primary" style="float: right;margin-top: 2px;" type="button" onClick="AbrirBox(0)">
                                            <span class="ico-plus icon-white"></span>
                                        </button>
                                    </div>
                                </div>                    
                                <div class="boxtable">                                      
                                    <table class="table" style="float:none" cellpadding="0" cellspacing="0" width="100%" id="example">                            
                                        <thead>
                                            <tr>                                                
                                                <th width="20%">Autor</th>
                                                <th>Título</th> 
                                                <?php if (!is_numeric($_GET['id'])) {?>
                                                <th width="15%">Especificação</th>       
                                                <?php } ?>                                                                                                 
                                                <th width="5%"><center>Ação</center></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="4" class="dataTables_empty">Carregando dados ...</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div style="clear:both"></div>
                                </div>
                            </div>
                        </div>
                        <?php if (!is_numeric($_GET['id'])) { ?>
                        <div class="span4">
                            <div class="block">  
                                <div class="boxhead">
                                    <div class="boxtext">
                                        Dúvidas Frequentes
                                    </div>                                
                                </div>                                
                                <div class="data-fluid">
                                    <ul class="list" id="faqListController">
                                        <?php $cur = odbc_exec($con, "select top 20 * from sf_faq order by acessos desc");
                                        while ($RFP = odbc_fetch_array($cur)) {?>
                                            <li><a href="Faq.php?id=<?php echo utf8_encode($RFP['id_faq']); ?>"><?php echo utf8_encode($RFP['titulo']); ?></a></li>
                                        <?php } ?>                                        
                                    </ul>                        
                                </div>
                            </div>
                        </div>
                        <?php }?>
                    </div>  
                </div>            
            </div>        
        </div>
    <div class="dialog" id="source" style="display: none;" title="Source"></div>     
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/excanvas.js'></script>
    <script type='text/javascript' src='../../js/plugins/other/jquery.mousewheel.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/sparklines/jquery.sparkline.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
    <script type='text/javascript' src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
    <script type='text/javascript' src='../../js/plugins.js'></script>
    <script type='text/javascript' src='../../js/charts.js'></script>
    <script type='text/javascript' src='../../js/actions.js'></script>
    <script type='text/javascript' src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>   
    <script type='text/javascript' src="../../js/plugins/select/select2.min.js"></script>    
    <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>            
    <script type="text/javascript" src="../../js/util.js"></script>                
    <script type="text/javascript">
        
        function AbrirBox(id) {
            var myId = "<?php echo "?idx=".$_GET['id'];?>";
            if (id > 0) {
                var myId = "?id=" + id + "<?php echo "&idx=".$_GET['id'];?>";
            }
            abrirTelaBox("FormFaq.php" + myId, 530, 794);
        }
        
        function FecharBox() {
            var oTable = $('#example').dataTable();
            oTable.fnDraw(false);          
            $("#newbox").remove();
        }    
        
        $(document).ready(function () {
            $('#example').dataTable({
                "iDisplayLength": 20,
                "aLengthMenu": [20, 30, 40, 50, 100],
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "Faq_server_processing.php<?php echo $finalURL;?>",
                'oLanguage': {
                    'oPaginate': {
                        'sFirst': "Primeiro",
                        'sLast': "Último",
                        'sNext': "Próximo",
                        'sPrevious': "Anterior"
                    }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
                    'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
                    'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
                    'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
                    'sLengthMenu': "Visualização de _MENU_ registros",
                    'sLoadingRecords': "Carregando...",
                    'sProcessing': "Processando...",
                    'sSearch': "Pesquisar:",
                    'sZeroRecords': "Não foi encontrado nenhum resultado"},
                "sPaginationType": "full_numbers"
            });
        });    
    </script>         
    <?php odbc_close($con);?>
    </body>
</html>