<?php
include './../../Connections/configini.php';

$imprimir = 0;
$tipo2 = "";
$contato = "";
$finalUrlServer = "";
$contatoName = "";

if (is_numeric($_GET["imp"])) {
    $imprimir = $_GET["imp"];
}

if (is_numeric($_GET["us"])) {
    $contato = $_GET["us"];
    $finalUrlServer .= "&us=" . $_GET["us"];
    $cur = odbc_exec($con, "select id_usuario,login_user,nome from sf_usuarios where inativo = 0 and (id_usuario = " . $_GET["us"] . ") order by nome") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $contatoName = $RFP["nome"];
    }
} else {
    $cur = odbc_exec($con, "select id_usuario,login_user,nome from sf_usuarios where inativo = 0 and (id_usuario = " . $_SESSION["id_usuario"] . ") order by nome") or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $contatoName = $RFP["nome"];
    }
}

if (is_numeric($_GET["id"])) {
    $finalUrlServer .= "&id=" . $_GET["id"];
    $cur = odbc_exec($con, "select id_fornecedores_despesas,tipo,razao_social,nome_fantasia from dbo.sf_fornecedores_despesas where id_fornecedores_despesas = " . $_GET["id"]) or die(odbc_errormsg());
    while ($RFP = odbc_fetch_array($cur)) {
        $tipo = $RFP["tipo"];
        $contatoName = $RFP["razao_social"];
        if ($RFP["razao_social"] != "") {
            $contatoName = $contatoName . " (" . $RFP["nome_fantasia"] . ")";
        }
    }
}

if (is_numeric($_GET["tpc"])) {
    $tipo2 = $_GET["tpc"];
    $finalUrlServer .= "&tpc=" . $_GET["tpc"];
}

if ($_GET["dti"] != "") {
    $DateBegin = str_replace("_", "/", $_GET["dti"]);
    $finalUrlServer .= "&dti=" . $_GET["dti"];
} else {
    $DateBegin = getData("T");
}

if ($_GET["dtf"] != "") {
    $DateEnd = str_replace("_", "/", $_GET["dtf"]);
    $finalUrlServer .= "&dtf=" . $_GET["dtf"];
} else {
    $DateEnd = getData("T");
}

if (is_numeric($_GET["Del"])) {
    odbc_exec($con, "DELETE FROM sf_mensagens_telemarketing WHERE id_tele = " . $_GET["Del"]);
    odbc_exec($con, "DELETE FROM sf_telemarketing_item WHERE id_telemarketing = " . $_GET["Del"]);
    odbc_exec($con, "DELETE FROM sf_telemarketing WHERE id_tele = " . $_GET["Del"]);
    echo "<script>window.top.location.href = 'ServicosSer.php?us=" . $_GET["us"] . "'; </script>";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <title>Sivis Business</title>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="../../css/stylesheets.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/main.css"/>
        <script type="text/javascript" src="../../js/graficos.js"></script>
        <script type="text/javascript" src="../../js/justgage.1.0.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script> 
        <script type="text/javascript" src="../../../SpryAssets/SpryValidationTextField.js"></script>
        <link rel="stylesheet" type="text/css" href="../../css/main.css" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="../../../SpryAssets/SpryValidationTextField.css"/>
        <style>            
            .fancybox-custom .fancybox-skin {
                box-shadow: 0 0 50px #069;
            }
            #example td {
                padding: 5px;
                line-height: 16px;
            }
            .select2-choice { height: 29px !important; }
            .select2-choice span { font-size:13px; line-height: 29px !important; }
        </style>
    </head>
    <body>
        <?php if ($imprimir == 0) { ?>
            <div id="loader"><img src="../../img/loader.gif"/></div>
        <?php } ?>
        <div class="wrapper">
            <?php
            if ($imprimir == 0) {
                include("../../menuLateral.php");
            }
            ?>
            <div <?php
            if ($imprimir == 0) {
                echo "class=\"body\"";
            }
            ?>>
                    <?php
                    if ($imprimir == 0) {
                        include("../../top.php");
                    }
                    ?>
                <div class="content">
                    <?php
                    if ($imprimir !== 0) {
                        $visible = "hidden";
                    }
                    ?>
                    <div class="page-header" <?php echo $visible; ?>>
                        <div class="icon"> <span class="ico-arrow-right"></span></div>
                        <h1>Serviços<small>Ordem de Serviços</small></h1>
                    </div>
                    <div class="row-fluid" <?php echo $visible; ?>>
                        <div class="span12">
                            <div class="block" style="font-size:13px">
                                <button class="button button-green btn-primary" type="button" onClick="AbrirBox(0, 0, 8)"><span class="ico-file-4 icon-white"></span></button>
                                <button name="btnPrint" class="button button-blue" type="button" onClick="AbrirBox(0, 0, 7)"><span class="ico-print"></span></button>
                                <select name="txtPenCon" id="txtPenCon" class="select input-medium" style="width:130px; height:31px">
                                    <option value="U" <?php
                                    if ($tipo == "U") {
                                        echo "SELECTED";
                                    }
                                    ?>>Atendente</option>
                                    <option value="C" <?php
                                    if ($tipo == "C") {
                                        echo "SELECTED";
                                    }
                                    ?>>Cliente</option>
                                    <option value="P" <?php
                                    if ($tipo == "P") {
                                        echo "SELECTED";
                                    }
                                    ?>>Prospect</option>
                                </select>
                                <input name="txtValue" id="txtValue" type="hidden" value="<?php echo $contato; ?>"/>
                                <input name="txtDesc" id="txtDesc" type="text" style="width:150px; height:31px" size="10" value="<?php echo $contatoName; ?>"/>
                                <select name="txtTipo" id="txtTipo" class="select input-medium" style="width:120px; height:31px">
                                    <option value="null">Todos</option>
                                    <option value="0" <?php
                                    if ($tipo2 == "0") {
                                        echo "SELECTED";
                                    }
                                    ?>>Fechados</option>
                                    <option value="1" <?php
                                    if ($tipo2 == "1") {
                                        echo "SELECTED";
                                    }
                                    ?>>Abertos</option>
                                </select> 
                                de <input name="txt_dt_begin" id="txt_dt_begin" type="text" class="datepicker" style="width:80px; height:31px" value="<?php echo $DateBegin; ?>" placeholder="Data Inicial"/> 
                                até <input name="txt_dt_end" id="txt_dt_end" type="text" class="datepicker" style="width:80px; height:31px" value="<?php echo $DateEnd; ?>" placeholder="Data Final"/> 
                                <button name="btnfind" id="btnfind" class="button button-turquoise" type="button" onClick="refreshFind()"><span class="ico-search icon-white"></span></button>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div class="boxhead" <?php echo $visible; ?>>
                        <div class="boxtext">Ordem de Serviços</div>
                    </div>
                    <div <?php
                    if ($imprimir == 0) {
                        echo "class=\"boxtable\"";
                    }
                    ?>>
                            <?php
                            if ($imprimir == 1) {
                                $titulo_pagina = "RELATÓRIO DE CONSULTA<br/>ATENDIMENTOS DE SERVIÇOS";
                                include "../Financeiro/Cabecalho-Impressao.php";
                            }
                            ?>
                        <table <?php
                        if ($imprimir == 0) {
                            echo "class=\"table\"";
                        } else {
                            echo "border=\"1\"";
                        }
                        ?> style="float:none" cellpadding="0" cellspacing="0" width="100%" id="example">
                            <thead>
                                <tr>
                                    <th width="8%"><center>Data</center></th>
                                    <th width="17%">Cliente</th>
                                    <th width="10%">Origem</th>
                                    <th width="8%">Procedência</th>
                                    <th width="10%">Atendente</th>
                                    <th width="5%"><center>Pendente</center></th>
                                    <th width="10%">Encaminhado</th>
                                    <th width="10%"><center>Próx. Cont.</center></th>
                                    <th width="15%"><center>Periodo para Cont.</center></th>
                                    <?php if ($imprimir == 0) { ?>
                                    <th width="5%"><center>Ação</center></th>
                                    <?php } ?>
                                </tr>
                            </thead>
                        </table>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialog" id="source" title="Source"></div>
        <script type="text/javascript" src="../../fancyapps/source/jquery.fancybox.js?v=2.1.4"></script>
        <link rel="stylesheet" type="text/css" href="../../fancyapps/source/jquery.fancybox.css?v=2.1.4" media="screen" />
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery/globalize.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/excanvas.js"></script>
        <script type="text/javascript" src="../../js/plugins/other/jquery.mousewheel.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/cookies/jquery.cookies.2.2.0.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/sparklines/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins.js"></script>
        <script type="text/javascript" src="../../js/charts.js"></script>
        <script type="text/javascript" src="../../js/actions.js"></script>
        <script type="text/javascript" src="../../js/plugins/uniform/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>
        <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
        <script type="text/javascript" src="../../js/util.js"></script>        
        <script type="text/javascript">
            
            $("#txt_dt_begin, #txt_dt_end").mask(lang["dateMask"]);
            
            $(document).ready(function () {
                $("#txtPenCon").change(function(event){
                    $("#txtDesc").val("");
                });
                $("#txtDesc").autocomplete({
                    source: function (request, response) {
                    $.ajax({
                    url: "./../CRM/ajax.php",
                        dataType: "json",
                        data: {
                        q: request.term,
                                t: $("#txtPenCon").val()
                        },
                        success: function (data) {
                        response(data);
                        }
                    });},
                    minLength: 3,
                    select: function (event, ui) {
                        $("#txtValue").val(ui.item.id);
                    }
                });
                $("#example").dataTable({
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": finalFind(),
                    "oLanguage": {
                    "oPaginate": {
                    "sFirst": "Primeiro",
                            "sLast": "Último",
                            "sNext": "Próximo",
                            "sPrevious": "Anterior"
                    }, "sEmptyTable": "Não foi encontrado nenhum resultado",
                            "sInfo": "Visualização do registro _START_ ao _END_ de _TOTAL_",
                            "sInfoEmpty": "Visualização do registro 0 ao 0 de 0",
                            "sInfoFiltered": "(filtrado de _MAX_ registros totais)",
                            "sLengthMenu": "Visualização de _MENU_ registros",
                            "sLoadingRecords": "Carregando...",
                            "sProcessing": "Processando...",
                            "sSearch": "Pesquisar:",
                            "sZeroRecords": "Não foi encontrado nenhum resultado"},
                    "sPaginationType": "full_numbers",
    <?php if ($imprimir == 1) { ?>
                "fnInitComplete": function(oSettings, json){window.print(); window.history.back(); },
    <?php } ?>
            });
        });
<?php if ($imprimir == 0) { ?>
            function refreshFind(){
            var oTable = $("#example").dataTable({
            "bServerSide": true,
                    "iDisplayLength": 20,
                    "aLengthMenu": [20, 30, 40, 50, 100],
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": finalFind(),
                    "oLanguage": {
                    "oPaginate": {
                    "sFirst": "Primeiro",
                            "sLast": "Último",
                            "sNext": "Próximo",
                            "sPrevious": "Anterior"
                    }, "sEmptyTable": "Não foi encontrado nenhum resultado",
                            "sInfo": "Visualização do registro _START_ ao _END_ de _TOTAL_",
                            "sInfoEmpty": "Visualização do registro 0 ao 0 de 0",
                            "sInfoFiltered": "(filtrado de _MAX_ registros totais)",
                            "sLengthMenu": "Visualização de _MENU_ registros",
                            "sLoadingRecords": "Carregando...",
                            "sProcessing": "Processando...",
                            "sSearch": "Pesquisar:",
                            "sZeroRecords": "Não foi encontrado nenhum resultado"},
                    "sPaginationType":  "full_numbers",
                    "bDestroy": true
            });
            }
<?php } ?>
        function finalFind(){
<?php if ($imprimir == 0) { ?>
            var retorno = "";
                    if ($("#txtDesc").val() != "" && $("#txtValue").val() != ""){
            if ($("#txtPenCon").val() == "U"){
            retorno = retorno + "&us=" + $("#txtValue").val();
            } else{
            retorno = retorno + "&id=" + $("#txtValue").val();
            }
            }
            if ($("#txtTipo").val() != ""){
            retorno = retorno + "&tpc=" + $("#txtTipo").val();
            }
            if ($("#txt_dt_begin").val() != ""){
            retorno = retorno + "&dti=" + $("#txt_dt_begin").val();
            }
            if ($("#txt_dt_end").val() != ""){
            retorno = retorno + "&dtf=" + $("#txt_dt_end").val();
            }
            return "Atendimentos_server_processing.php?zp=1&tp=1&imp=0" + retorno.replace(/\//g, "_") + "&ord=<?php echo $_POST['ordem']; ?>";
<?php } else { ?>
            return "Atendimentos_server_processing.php?zp=1&tp=1&imp=1<?php echo $finalUrlServer; ?>&ord=<?php echo $_POST['ordem']; ?>";
<?php } ?>
        }
        function AbrirBox(id, gb, op) {
        if (op === 0) {
        $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:667px; height:500px; position:absolute; top:50%; left:50%; margin-left:-333px; margin-top:-250px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:500px' src='./../CRM/FormTelemarketing.php?id=" + id + "&gb=" + gb + "&jn=o'></iframe></div></div></div></div></div>");
        } else if (op === 7) {
        var retorno = "";
                if ($("#txtDesc").val() != "" && $("#txtValue").val() != ""){
        if ($("#txtPenCon").val() == "U"){
        retorno = retorno + "&us=" + $("#txtValue").val();
        } else{
        retorno = retorno + "&id=" + $("#txtValue").val();
        }
        }
        if ($("#txtTipo").val() != ""){
        retorno = retorno + "&tpc=" + $("#txtTipo").val();
        }
        if ($("#txt_dt_begin").val() != ""){
        retorno = retorno + "&dti=" + $("#txt_dt_begin").val();
        }
        if ($("#txt_dt_end").val() != ""){
        retorno = retorno + "&dtf=" + $("#txt_dt_end").val();
        }
        window.location = "ServicosSer.php?imp=1" + retorno.replace(/\//g, "_");
        } else if (op === 8) {
        window.location = "Servicos_Atendimentos.php?Cli=0";
        } else {
        $("body").append("<div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'><div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:667px; height:500px; position:absolute; top:50%; left:50%; margin-left:-333px; margin-top:-250px; opacity:1; overflow:visible;'><div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'><div class='fancybox-outer'><div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'><iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:500px' src='./../CRM/FormTelemarketing.php?id=" + id + "&gb=" + gb + "&jn=o&fx=s'></iframe></div></div></div></div></div>");
        }
        }
        function FecharBox() {
            var oTable = $("#example").dataTable();
            oTable.fnDraw(false);
            $("#newbox").remove();
        }
<?php if ($imprimir == 1) { ?>
        $(window).load(function () {
            $("#example_length").remove();
            $("#example_filter").remove();
            $("#example_paginate").remove();
            $("#formPQ > th").css("background-image", "none");
        });
<?php } ?>
        </script>        
        <?php odbc_close($con); ?>
    </body>
</html>