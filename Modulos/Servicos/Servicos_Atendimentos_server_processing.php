<?php

include './../../Connections/configini.php';

$aColumns = array('data_tele', 'de_pessoa', 'pendente', 'para_pessoa', 'proximo_contato', 'proximo_contato', 'relato', 'numficha');
$iTotal = 0;
$iFilteredTotal = 0;
$sWhereX = "";
$sWhere = "";
$sOrder = " ORDER BY data_tele desc ";

if ($_GET['Cli'] != "") {
    $sWhereX = $sWhereX . " and t.pessoa =" . $_GET['Cli'];
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            if (intval($_GET['iSortCol_' . $i]) > 0 && intval($_GET['iSortCol_' . $i]) < 9) {
                $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) - 1] . "
                                    " . $_GET['sSortDir_' . $i] . ", ";
            }
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY data_tele desc";
    }
}

if ($_GET['sSearch'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns[$i] . " LIKE '%" . utf8_decode($_GET['sSearch']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

for ($i = 0; $i < count($aColumns); $i++) {
    if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
        $sWhere .= $aColumns[$i] . " AND LIKE '%" . utf8_decode($_GET['sSearch_' . $i]) . "%' ";
    }
}

$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder . ") as row,id_tele,u.login_user as de_pessoa,u2.login_user as para_pessoa,data_tele,pendente,proximo_contato,obs,relato,id_fornecedores_despesas,obs_fechamento,numficha
            from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa
            LEFT JOIN sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = t.pessoa
            LEFT JOIN sf_usuarios u2 on u2.id_usuario = t.para_pessoa 
            where atendimento_servico = 1 and pessoa > 0 " . $sWhereX . $sWhere . ") as a WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) . "" . $sOrder;
$cur = odbc_exec($con, $sQuery1);

$sQuery = "SELECT COUNT(*) total from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa where atendimento_servico = 1 and pessoa > 0 $sWhereX " . $sWhere;
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $RFP['total'];
}

$sQuery = "SELECT COUNT(*) total from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa where atendimento_servico = 1 and pessoa > 0 $sWhereX ";
$cur2 = odbc_exec($con, $sQuery);
while ($RFP = odbc_fetch_array($cur2)) {
    $iTotal = $RFP['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $backColor = "";
    $pendente = "NÃO";
    $dias = "-";
    $proxContato = escreverDataHora($aRow['proximo_contato']);
    if ($aRow['pendente'] == 1) {
        $backColor = " style='color: red' ";
        $pendente = "SIM";
        if ($aRow['proximo_contato'] != null) {
            $diferenca = dataDiff(date('Y-m-d'), escreverData($aRow['proximo_contato'], 'Y-m-d'), 'd');
            if ($diferenca > 0) {
                $dias = $diferenca . " dia(s) restantes";
            } else {
                if ($diferenca == 0) {
                    $dias = "Hoje";
                } else {
                    $dias = (-1 * $diferenca) . " dia(s) atrasados";
                }
            }
        }
    }
    $row[] = "<center><a title=\"" . escreverDataHora($aRow['data_tele']) . "\" href=\"javascript:void(0)\" onClick=\"AbrirBox(" . $aRow['id_tele'] . "," . utf8_encode($aRow['id_fornecedores_despesas']) . ",0)\"><div id='formPQ' title='" . escreverDataHora($aRow['data_tele']) . "'>" . escreverDataHora($aRow['data_tele']) . "</div></a></center>";
    $row[] = "<center><div id='formPQ' " . $backColor . " title='" . utf8_encode($aRow['numficha']) . "'>" . utf8_encode($aRow['numficha']) . "</div></center>";
    $row[] = "<center><div id='formPQ' " . $backColor . " title='" . utf8_encode($aRow['de_pessoa']) . "'>" . utf8_encode($aRow['de_pessoa']) . "</div></center>";
    $row[] = "<center><div id='formPQ' " . $backColor . " title='" . $pendente . "'>" . $pendente . "</div></center>";
    $row[] = "<center><div id='formPQ' " . $backColor . " title='" . utf8_encode($aRow['para_pessoa']) . "'>" . utf8_encode($aRow['para_pessoa']) . "</div></center>";
    $row[] = "<center><div id='formPQ' " . $backColor . " title='" . $proxContato . "'>" . $proxContato . "</div></center>";
    $row[] = "<center><div id='formPQ' " . $backColor . " title='" . $dias . "'>" . $dias . "</div></center>";
    $row[] = "<div id='formPQ' " . $backColor . " title='*R:" . utf8_encode($aRow['relato']) . "\n*O:" . utf8_encode($aRow['obs']) . "\n*F:" . utf8_encode($aRow['obs_fechamento']) . "'>" . utf8_encode($aRow['relato']) . "</div>";
    if ($imprimir == 0) {
        $action = "";
        if ($ckb_crm_fec_atend_ == 1 && $aRow['pendente'] == 1) {
            $action = $action . "<a href=\"javascript:void(0)\" onClick=\"AbrirBox(" . $aRow['id_tele'] . "," . utf8_encode($aRow['id_fornecedores_despesas']) . ",1)\"><img src=\"../../img/close.png\" width='18' height='18' title=\"Encerrar Pendência\" value='Enviar'></a>";
        }
        if ($ckb_crm_exc_atend_ == 1 && ($aRow['de_pessoa'] == $_SESSION["login_usuario"] || $aRow['para_pessoa'] == $_SESSION["login_usuario"])) {
            $action = $action . "<a href='Servicos_Atendimentos.php?Delx=" . $aRow['id_tele'] . "&gb=" . $_GET['Cli'] . "'><input name='' type='image' onClick=\"return confirm('Deseja deletar esse registro?')\" src=\"../../img/1365123843_onebit_33 copy.PNG\" width='18' height='18' title=\"Excluir\" value='Enviar'></a>";
        }
        $row[] = "<center>" . $action . "</center>";
    }
    $output['aaData'][] = $row;
}
echo json_encode($output);
odbc_close($con);
