<?php

require_once(__DIR__ . '/../../Connections/configini.php');
require_once(__DIR__ . '/../../util/util.php');
$aColumns = array('data_tele', 'razao_social', 'contato', 'tipo', 'u.login_user de_pessoax', 'pendente', 'u2.login_user para_pessoax', 'proximo_contato', 'proximo_contato');
$iTotal = 0;
$iFilteredTotal = 0;
$imprimir = 0;
$tpd = "0";
$tipoX = "";
$sWhereX = "";
$sWhere = "";
$zp = 0;
$sOrder = " ORDER BY data_tele desc ";
$sOrder2 = " ORDER BY data_tele desc ";
$mdl_aca_ = returnPart($_SESSION["modulos"],6);

if (is_numeric($_GET['tp'])) {
    $tipoX = " atendimento_servico = " . $_GET['tp'] . " and ";
}

if (is_numeric($_GET['tpd'])) {
    $tpd = $_GET['tpd'];
}

if (is_numeric($_GET['imp'])) {
    $imprimir = $_GET['imp'];
}

if (is_numeric($_GET['id'])) {
    $sWhereX = $sWhereX . " AND id_fornecedores_despesas = " . $_GET['id'];
}

if (is_numeric($_GET['tpc'])) {
    $sWhereX = $sWhereX . " AND pendente = " . $_GET['tpc'];
}

if (is_numeric($_GET['us'])) { //recepção | Serviços | CRM
    $sWhereX = $sWhereX . " and ((t.para_pessoa is null and t.de_pessoa = " . $_GET['us'] . ") or t.de_pessoa = " . $_GET['us'] . " or t.para_pessoa = " . $_GET['us'] . ")";
}

if (is_numeric($_GET['use'])) { //index
    $sWhereX = $sWhereX . " and ((t.acompanhar = 1 and t.de_pessoa = " . $_SESSION["id_usuario"] . ") or (t.para_pessoa is null and t.de_pessoa = " . $_SESSION["id_usuario"] . ") or t.para_pessoa = " . $_SESSION["id_usuario"] . " or (t.para_pessoa is null and t.departamento is null) or (t.para_pessoa is null and t.departamento is not null and t.departamento in (select departamento from sf_usuarios where departamento is not null and id_usuario = " . $_SESSION["id_usuario"] . ")) or id_tele in (select id_tele from dbo.sf_mensagens_telemarketing where id_usuario_para = " . $_SESSION["id_usuario"] . " ))";
}

if ($_GET['frm'] != "") {
    $sWhereX = $sWhereX . " AND especificacao = '" . $_GET['frm'] . "'";
}

if (isset($_GET['grp']) && $_GET['grp'] != "undefined") {
    $sWhereX = $sWhereX . " and fd.grupo_pessoa in (" . str_replace("\"", "'", str_replace(array("[", "]"), "", $_GET["grp"])) . ")";
}

if (is_numeric($_GET['uf'])) {
    $sWhereX = $sWhereX . " AND fd.estado = " . $_GET['uf'];
}

$char = " AND ";
if ($tpd == "0" && $_GET['dti'] != '' && $_GET['dtf'] != '') {
    $sWhereX .= " AND (";
    $char = "";
}

if ($_GET['dti'] != '') {
    $sWhereX .= $char . ($tpd == "0" ? "data_tele" : "proximo_contato") . " >= " . valoresDataHora2(str_replace("_", "/", $_GET['dti']), "00:00:00") . "";
}

if ($_GET['dtf'] != '') {
    $sWhereX .= " AND " . ($tpd == "0" ? "data_tele" : "proximo_contato") . " <= " . valoresDataHora2(str_replace("_", "/", $_GET['dtf']), "23:59:59") . "";
}

if ($tpd == "0" && $_GET['dti'] != '' && $_GET['dtf'] != '') {
    $sWhereX .= " or id_tele in (select id_tele from sf_mensagens_telemarketing where data between " . valoresDataHora2(str_replace("_", "/", $_GET['dti']), "00:00:00") . " and " . valoresDataHora2(str_replace("_", "/", $_GET['dtf']), "23:59:59") . "))";
}

if (is_numeric($_GET['zp'])) {
    $zp = $_GET['zp'];
}

function legenda($i) {
    switch ($i) {
        case "T":
            return "Telefone";
        case "E":
            return "Email";
        case "V":
            return "Visita";
        case "P":
            return "Presencial";
        case "S":
            return "SMS";
        case "W":
            return "Whatsapp";
        case "R":
            return "Portal";
    }
    return "Outro";
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = $_GET['iDisplayStart'];
    $sQtd = $_GET['iDisplayLength'];
}

if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";
    $sOrder2 = "ORDER BY  ";
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $sOrder .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i])], 1) . " " . $_GET['sSortDir_' . $i] . ", ";
            $sOrder2 .= prepareCollum($aColumns[intval($_GET['iSortCol_' . $i])], 0) . " " . $_GET['sSortDir_' . $i] . ", ";
        }
    }
    $sOrder = substr_replace($sOrder, "", -2);
    $sOrder2 = substr_replace($sOrder2, "", -2);
    if ($sOrder == "ORDER BY") {
        $sOrder = " ORDER BY data_tele desc";
        $sOrder2 = " ORDER BY data_tele desc";
    }
}

if ($_GET['sSearch'] != "") {
    $sWhere = "AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= prepareCollum($aColumns[$i], 0) . " LIKE '%" . utf8_decode($_GET['sSearch']) . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

$sQuery = "set dateformat dmy; SELECT COUNT(*) total from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa
           LEFT JOIN sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = t.pessoa
           LEFT JOIN sf_usuarios u2 on u2.id_usuario = t.para_pessoa
           where " . $tipoX . " pessoa > 0 " . $sWhereX . $sWhere;
//echo $sQuery; exit;
$cur2 = odbc_exec($con, $sQuery);
while ($aRow = odbc_fetch_array($cur2)) {
    $iFilteredTotal = $aRow['total'];
}

$sQuery = "set dateformat dmy; SELECT COUNT(*) total from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa
           LEFT JOIN sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = t.pessoa
           LEFT JOIN sf_usuarios u2 on u2.id_usuario = t.para_pessoa
           where " . $tipoX . " pessoa > 0 " . $sWhereX;
//echo $sQuery; exit;
$cur2 = odbc_exec($con, $sQuery);
while ($aRow = odbc_fetch_array($cur2)) {
    $iTotal = $aRow['total'];
}

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

odbc_exec($con, "set dateformat dmy;");
$sQuery1 = "SELECT * FROM(SELECT ROW_NUMBER() OVER (" . $sOrder2 . ") as row,id_tele,u.login_user as de_pessoax,u2.login_user as para_pessoax,
data_tele,pendente,proximo_contato,t.obs,de_pessoa,para_pessoa,razao_social,tipo,nome_fantasia,id_fornecedores_despesas,atendimento_servico,
nome_procedencia, fd.endereco, fd.numero,fd.complemento,fd.bairro,cidade_nome, estado_sigla, 
case when atendimento_servico = 2 then 'Evento' when atendimento_servico = 1 then 'Serviço' else 'CRM' end contato,
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 0 and fornecedores_despesas  = id_fornecedores_despesas) telefone_contato,
(select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas  = id_fornecedores_despesas) email_contato,
(select count(*) from sf_mensagens_telemarketing tm where tm.id_tele = t.id_tele) quantidade,especificacao
from sf_telemarketing t INNER JOIN sf_usuarios u on u.id_usuario = t.de_pessoa
LEFT JOIN sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = t.pessoa
LEFT JOIN sf_usuarios u2 on u2.id_usuario = t.para_pessoa
LEFT JOIN sf_procedencia on t.procedencia = id_procedencia and tipo_procedencia = 1
LEFT join tb_cidades on (fd.cidade = tb_cidades.cidade_codigo)
LEFT join tb_estados on (fd.estado = tb_estados.estado_codigo)
where " . $tipoX . " id_fornecedores_despesas is not null " . $sWhereX . $sWhere . ") as a " . (!isset($_GET['pdf']) ? "WHERE a.row > " . $sLimit . " and a.row <= " . ($sLimit + $sQtd) : "") . " " . $sOrder;
//echo $sQuery1;exit;
$cur = odbc_exec($con, $sQuery1);
while ($aRow = odbc_fetch_array($cur)) {
    $row = array();
    $backColor = "";
    $pendente = utf8_encode("NÃO");
    $dias = "-";
    $detalhe = "";
    $action = "";
    $delete = "";
    $l1 = (isset($_GET['home']) ? "Modulos/" : "../"); //$_GET['perfil'] == 'acad' || is_numeric($_GET["use"])
    if ($aRow['quantidade'] > 0) {
        $informativo = "<div style='text-align:center; position:absolute; width:13px; height:13px; background:#999; color:white; border-radius:50%'>" . $aRow['quantidade'] . "</div>";
    } else {
        $informativo = "";
    }
    if ($aRow['pendente'] == 1) {
        $backColor = " style='color:#73879C' ";
        $pendente = utf8_encode("SIM");
        if ($aRow['proximo_contato'] != null) {
            $diferenca = dataDiff(date('Y-m-d'), escreverData($aRow['proximo_contato'], 'Y-m-d'), 'd');
            if ($diferenca > 0) {
                $dias = $diferenca . " dia(s) restantes";
            } else {
                if ($diferenca == 0) {
                    $dias = "Hoje";
                } else {
                    $backColor = " style='color:#CC4748' ";
                    $dias = (-1 * $diferenca) . " dia(s) atrasados";
                }
            }
        }
    }
    $proxContato = escreverDataHora($aRow['proximo_contato']);
    if ($zp != 2 || ($zp == 2 && $imprimir == 0)) {
        $link = "#";
        if ($aRow["tipo"] == 'P') {
            $l1 .= "CRM/ProspectsForm.php";
        } else if($mdl_aca_ == 1) {
            $l1 .= "Academia/ClientesForm.php";
        } else {
            $l1 .= "CRM/CRMForm.php";
        }
        if ($imprimir == 0 && ($ckb_adm_cli_ == 1)) {
            $link = $l1 . "?id=" . $aRow["id_fornecedores_despesas"] . "&at=" . $aRow['id_tele'];
        }
        $row[] = "<center><a title='Atendimentos do Cliente' href='" . $link . "'><div id='formPQ'>" . escreverDataHora($aRow["data_tele"]) . "</div></a></center>";
    }
    if ($aRow['id_fornecedores_despesas'] == 0) {
        $cliente = "LIGAÇÃO AVULSA";
    } else {
        $cliente = utf8_encode($aRow['razao_social']);
        if (utf8_encode($aRow['nome_fantasia']) != "") {
            $cliente = $cliente . " (" . utf8_encode($aRow['nome_fantasia']) . ")";
        }
    }
    if ($zp != 2 || ($zp == 2 && $imprimir == 0)) {
        $row[] = "<div id='formPQ'>" . $cliente . "</div>";
        $row[] = "<div id='formPQ'>" . utf8_encode($aRow['contato']) . "</div>";
    } else {
        $row[] = $cliente;
        $row[] = utf8_encode($aRow['contato']);
    }
    if ($zp != 2 || ($zp == 2 && $imprimir == 0)) {
        $row[] = "<div id='formPQ'>" . legenda($aRow['especificacao']) . "</div>";
    }
    if ($zp != 2 || ($zp == 2 && $imprimir == 0)) {
        if ($pendente == "SIM") {
            $pendicon = "<span class='ico-warning' " . $backColor . "></span>";
        } else {
            $pendicon = "<span class='ico-minus-sign' " . $backColor . "></span>";
        }
        $row[] = "<div id='formPQ'>" . utf8_encode($aRow['de_pessoax']) . "</div>";
        $row[] = $informativo . "<center><div id='formPQ'>" . $pendicon . "</div></center>";
        $row[] = "<div id='formPQ'>" . utf8_encode($aRow['para_pessoax']) . "</div>";
        $row[] = "<center><div id='formPQ'>" . $proxContato . "</div></center>";
        $row[] = "<center><div id='formPQ'>" . $dias . "</div></center>";
    } else {
        if ($imprimir == 1) {
            $row[] = utf8_encode($aRow['endereco']) . ", Nº: " . utf8_encode($aRow['numero']) . ", Compl: " . utf8_encode($aRow['complemento']);
            $row[] = utf8_encode($aRow['bairro']);
            $row[] = utf8_encode($aRow['cidade_nome']) . "/" . utf8_encode($aRow['estado_sigla']);
            $row[] = utf8_encode($aRow['telefone_contato']);
            $row[] = utf8_encode($aRow['email_contato']);
        }
    }
    $detalhe = "<li><a href=\"javascript:void(0)\" onClick=\"AbrirBox(" . $aRow['id_tele'] . "," . utf8_encode($aRow['id_fornecedores_despesas']) . ",0)\"><input name='' type='image' onClick=\"return confirm('Deseja deletar esse registro?')\" src=\"../../img/1365123843_onebit_323 copy.PNG\" width='16' height='16' title=\"Excluir\" value='Enviar'> Detalhes do Atendimento</li>";
    if ($imprimir == 0) {
        if ($aRow['pendente'] == 1 && $ckb_crm_fec_atend_ == 1) {
            $action = "<li><a href=\"javascript:void(0)\" onClick=\"AbrirBox(" . $aRow['id_tele'] . "," . utf8_encode($aRow['id_fornecedores_despesas']) . ",1)\"><img src=\"../../img/close.png\" width='16' height='16' title=\"Encerrar Pendência\" value='Enviar'> Fechar Atendimento</a></li>";
        }
        if ($ckb_crm_exc_atend_ == 1 && ($aRow['de_pessoa'] == $_SESSION["id_usuario"] || $aRow['para_pessoa'] == $_SESSION["id_usuario"])) {
            if ($zp == 1) {
                $delete = "<li class=\"divider\"></li><li><a href='ServicosSer.php?Del=" . $aRow['id_tele'] . "&us=" . $_GET['us'] . "' onClick=\"return confirm('Deseja deletar esse registro?')\"><input name='' type='image' src=\"../../img/1365123843_onebit_33 copy.PNG\" width='16' height='16' title=\"Excluir\" value='Enviar'> Excluir</a></li>";
            } elseif ($zp == 2) {
                $delete = "<li class=\"divider\"></li><li><a href='Telemarketing.php?Del=" . $aRow['id_tele'] . "&us=" . $_GET['us'] . "' onClick=\"return confirm('Deseja deletar esse registro?')\"><input name='' type='image' src=\"../../img/1365123843_onebit_33 copy.PNG\" width='16' height='16' title=\"Excluir\" value='Enviar'> Excluir</a></li>";
            } elseif ($zp == 3) {
                $delete = "<li class=\"divider\"></li><li><a href='#' onClick=\"RemoverChamado(" . $aRow['id_tele'] . ")\"><input name='' type='image' src=\"../../img/1365123843_onebit_33 copy.PNG\" width='16' height='16' title=\"Excluir\" value='Enviar'> Excluir</a></li>";
            } elseif ($zp == 4) {
                $delete = "<li class=\"divider\"></li><li><a href='index.php?Delx=" . $aRow['id_tele'] . "' onClick=\"return confirm('Deseja deletar esse registro?')\"><input name='' type='image' src=\"../../img/1365123843_onebit_33 copy.PNG\" width='16' height='16' title=\"Excluir\" value='Enviar'> Excluir</a></li>";
            } else {
                $delete = "<li class=\"divider\"></li><li><a href='../Servicos/Servicos_Atendimentos.php?Delx=" . $aRow['id_tele'] . "&page=" . $_GET['page'] . "&gb=" . $_GET['id'] . "' onClick=\"return confirm('Deseja deletar esse registro?')\"><input name='' type='image' src=\"../../img/1365123843_onebit_33 copy.PNG\" width='16' height='16' title=\"Excluir\" value='Enviar'> Excluir</a></li>";
            }
        }
    }
    $url = "";
    if ($imprimir == 0 && (($aRow['atendimento_servico'] == '0' && $ckb_crm_tel_ == 1) || ($aRow['atendimento_servico'] == '1' && $ckb_ser_ser_ == 1))) {
        if ($aRow['atendimento_servico'] == 1) {
            if (utf8_encode($aRow['atendimento_servico']) == '1') {
                $url = "./../Servicos/Servicos_Atendimentos.php";
            } else {
                $url = "./../CRM/CRMForm.php";
            }
            if (is_numeric($_GET['use'])) {
                $url = str_replace("./../", "Modulos/", $url);
            }
            $url = "<li><a title=\"Atendimentos do Cliente\" href='" . $url . "?Cli=" . $aRow['id_fornecedores_despesas'] . "'><img src=\"../../img/1367947965_schedule.png\" width=\"16\" height=\"16\"> Atendimentos do Cliente</a></li>";
        }
    } else if ($imprimir == 0) {
        $url = "<li><img src=\"../../img/1367947965_schedule2.png\" width=\"16\" height=\"16\"> Atendimentos do Cliente</li>";
    }
    $row[] = "<center><div style=\"text-align:left\" class=\"btn-group\">
                <button class=\"btn dropdown-toggle\" style=\"height:14px; margin:-1px; background: var(--cor-secundaria)\" data-toggle=\"dropdown\">
                    <span class=\"caret\" style=\"margin-top:2px; border-top-color:#FFF\"></span>
                </button>
                <ul style=\"left:-130px;\" class=\"dropdown-menu\">" . $detalhe . $action . $url . $delete . "</ul>
             </div></center>";
    $output['aaData'][] = $row;
}

if (!isset($_GET['pdf'])) {
    echo json_encode($output);
}
odbc_close($con);
