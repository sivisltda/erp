<?php
include './../../Connections/configini.php';
$disabled = 'disabled';

function valoresTextoRich($nome) {
    $original = array("text-indent: -", "margin-left:-", "'");
    $novo = array("text-indent: ", "margin-left:", "");
    return "'" . utf8_decode(str_replace($original, $novo, $_POST[$nome])) . "'";
}

if (isset($_POST['bntSave'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "update sf_faq set " .
                        " titulo = " . valoresTexto('txtTituloArtigo') .
                        ",conteudo = " . valoresTextoRich('txtTextoArtigo') .
                        ",especificacao = " . $_POST['txtSelectEsp'] .
                        " where id_faq = " . $_POST['txtId']) or die(odbc_errormsg());
    } else {
        odbc_exec($con, "insert into sf_faq(data_pub,usuario,titulo,conteudo,especificacao,id_faq_rel)values(getdate()," .
                        $_SESSION["id_usuario"] . "," .
                        valoresTexto('txtTituloArtigo') . "," .
                        valoresTextoRich('txtTextoArtigo') . "," .
                        utf8_decode($_POST['txtSelectEsp']) . "," .
                        utf8_decode($_POST['txtIdx']) . ")") or die(odbc_errormsg());
        $res = odbc_exec($con, "select top 1 id_faq from sf_faq order by id_faq desc") or die(odbc_errormsg());
        $_POST['txtId'] = odbc_result($res, 1);
    }
}

if (isset($_POST['bntDelete'])) {
    if ($_POST['txtId'] != '') {
        odbc_exec($con, "DELETE FROM sf_faq WHERE id_faq = " . $_POST['txtId']);
        echo "<script>alert('Registro excluido com sucesso'); parent.FecharBox();</script>";
    }
}

if (isset($_POST['bntNew'])) {
    $_GET['id'] = '';
    $_POST['txtId'] = '';
}

if ($_GET['id'] != '' || $_POST['txtId'] != '') {
    $PegaURL = $_GET['id'];
    if ($_POST['txtId'] != '') {
        $disabled = 'disabled';
        $PegaURL = $_POST['txtId'];
    }
    $cur = odbc_exec($con, "select * from sf_faq where id_faq =" . $PegaURL);
    while ($RFP = odbc_fetch_array($cur)) {
        $id = utf8_encode($RFP['id_faq']);
        $titulo_artigo = utf8_encode($RFP['titulo']);
        $SelectEsp = utf8_encode($RFP['especificacao']);
        $texto_artigo = utf8_encode($RFP['conteudo']);
    }
} else {
    $disabled = '';
    $id = '';
    $titulo_artigo = '';
    $SelectEsp = '';
    $texto_artigo = '';
}

if (is_numeric($_GET['idx'])) {
    $idx = $_GET['idx'];
} else {
    $idx = "null";
}

if (isset($_POST['bntEdit'])) {
    if ($_POST['txtId'] != '') {
        $disabled = '';
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/ico" href="../../favicon.ico"/>
        <link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
        <link href="../../css/main.css" rel="stylesheet">
        <link href="../../css/formularios.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootbox.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src='../../js/plugins/jquery/jquery-1.9.1.min.js'></script>        
    </head>
    <body>
        <form action="FormFaq.php<?php
        if (is_numeric($_GET['idx'])) {
            echo "?idx=" . $_GET['idx'];
        }
        ?>" name="frmEnviaDados" method="POST">
            <div class="frmhead">
                <div class="frmtext">Artigo</div>
                <div class="frmicon" onClick="parent.FecharBox()">
                    <span class="ico-remove"></span>
                </div>
            </div>
            <div class="frmcont">
                <input name="txtId" id="txtId" value="<?php echo $id; ?>" type="hidden"/>
                <input name="txtIdx" id="txtIdx" value="<?php echo $idx; ?>" type="hidden"/> 
                <div style="width:69%; float:left;<?php
                    if (is_numeric($_GET['idx'])) {
                        echo "display: none;";
                    }
                    ?>">
                    <span>Título do Artigo:</span>
                    <input name="txtTituloArtigo" type="text" class="input-medium" value="<?php echo $titulo_artigo; ?>" <?php echo $disabled; ?>/>
                </div>
                <div style="width:30%; float:left; margin-left:1%;<?php
                    if (is_numeric($_GET['idx'])) {
                        echo "display: none;";
                    }
                    ?>">
                    <span>Especificação:</span>
                    <select class="select" name="txtSelectEsp" <?php echo $disabled; ?>>
                        <option value="null">Selecione uma Especificação</option>
                        <?php $cur = odbc_exec($con, "select id_especificacao,nome_especificacao from sf_telemarketing_especificacao order by nome_especificacao");
                        while ($RFP = odbc_fetch_array($cur)) { ?>  
                            <option value="<?php echo utf8_encode($RFP['id_especificacao']); ?>" <?php
                            if (!(strcmp($RFP['id_especificacao'], $SelectEsp))) {
                                echo "SELECTED";
                            }
                            ?>><?php echo utf8_encode($RFP['nome_especificacao']); ?></option>                                                                
                        <?php } ?>                                        
                    </select>
                </div>
                <div style="float:left;margin-top: 5px;<?php
                    if (is_numeric($_GET['idx'])) {
                        echo "margin-bottom: 40px;";
                    }
                    ?>">
                    <textarea name="txtTextoArtigo" id="txtTextoArtigo" <?php echo $disabled; ?>><?php echo $texto_artigo; ?></textarea>
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="frmfoot">
                <div class="frmbtn">
                    <?php if ($disabled == "") {
                        if ($_POST["txtId"] == "") {
                            $btnType = "onClick=\"parent.FecharBox()\"";
                        } else {
                            $btnType = "type=\"submit\" name=\"bntAlterar\"";
                        } ?>
                        <button class="btn green" type="submit" name="bntSave" onClick="return validaForm()"><span class="ico-checkmark"></span> Gravar</button>
                        <button class="btn yellow" title="Cancelar" <?php echo $btnType; ?>><span class="ico-reply"></span> Cancelar</button>
                    <?php } else { ?>
                        <button class="btn green" type="submit" name="bntNew" value="Novo"><span class="ico-plus-sign"></span> Novo</button>
                        <button class="btn green" type="submit" name="bntEdit" value="Alterar"><span class="ico-edit"></span> Alterar</button>
                        <button class="btn red" type="submit" name="bntDelete" value="Excluir" onClick="return confirm('Deseja deletar esse registro?');"><span class="ico-remove"></span> Excluir</button>
                    <?php } ?>
                </div>
            </div>
        </form>
        <?php odbc_close($con); ?>
    </body> 
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-ui-1.10.1.custom.min.js"></script>        
    <script type="text/javascript" src="../../js/plugins/jquery/jquery-migrate-1.1.1.min.js"></script>        
    <script type='text/javascript' src='../../js/jquery.validate.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/jquery.mask.js"></script>
    <script type='text/javascript' src='../../js/jquery.priceformat.min.js'></script>
    <script type='text/javascript' src='../../js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type="text/javascript" src="../../js/plugins/bootbox/bootbox.js"></script>        
    <script type="text/javascript" src="../../js/plugins/select/select2.min.js"></script>    
    <script type="text/javascript" src="../../js/plugins/cleditor/jquery.cleditor.js"></script>
    <script type="text/javascript" src="../../js/plugins/ckeditor/ckeditor.js"></script>  
    <script type="text/javascript" src="../../js/util.js"></script>    
    <script type="text/javascript">
        $("#txtTextoArtigo").cleditor({width: 752, height: 332, controls: "bold italic underline strikethrough subscript superscript | font size style | color highlight removeformat | bullets numbering | outdent indent | alignleft center alignright justify | undo redo | rule image link unlink | print source"});
    </script>    
</html>