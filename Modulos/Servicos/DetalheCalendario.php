<?php include './../../Connections/configini.php'; ?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<title>Sivis Business</title>
<link rel="icon" type="image/ico" href="../../favicon.ico"/>
<link href="../../css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="../../css/main.css" rel="stylesheet">
<script type="text/javascript" src="../../js/plugins/jquery/jquery-1.9.1.min.js"></script>
<style type="text/css">
    td {
        padding: 5px;
        border-right: 1px solid #CCC;
        border-bottom: 1px solid #CCC;
    }
    .tdhead {
        float: left;
        padding: 0 10px;
        font-weight: bold;
        line-height: 25px;
        background: #E9E9E9;
        border: 1px solid #CCC;
    }
    .tdbody {
        height: 208px;
        overflow-y: scroll;
        border: 1px solid #CCC;
        border-top: 0;
    }
</style>
<div class="row-fluid">
    <div class="frmhead">
        <div class="frmtext">Detalhamento de Contas Fixas</div>
        <div class="frmicon" style="top:13px" onClick="parent.FecharBox();">
            <span class="ico-remove"></span>
        </div>
    </div>
    <div class="frmcont" style="height:236px">
        <div class="tdhead" style="width:144px">Grupo</div>
        <div class="tdhead" style="width:143px; border-left:0">Conta</div>
        <div class="tdhead" style="width:143px; border-left:0">Histórico</div>
        <div class="tdhead" style="width:143px; border-left:0">Status</div>
        <div style="clear:both"></div>
        <div class="tdbody">
            <table width="100%" cellpadding="0" cellspacing="0">
                <?php
                if ($_GET["id"] != "") {
                    $cur = odbc_exec($con, "select l.id_lancamento_movimento,g.descricao grupo,cm.descricao,l.historico historico_baixa
                    ,isnull((select COUNT(p.id_lancamento_movimento) from sf_lancamento_movimento_parcelas p 
                    inner join sf_contas_movimento c on c.id_contas_movimento = l.conta_movimento 
                    where l.id_lancamento_movimento = p.id_lancamento_movimento and p.data_pagamento is null 
                    and dateadd(day,dias_tolerancia,data_vencimento) < GETDATE() and p.inativo = 0),0) as num_parc
                    ,isnull((select COUNT(p.id_lancamento_movimento) from sf_lancamento_movimento_parcelas p 
                    inner join sf_contas_movimento c on c.id_contas_movimento = l.conta_movimento 
                    where l.id_lancamento_movimento = p.id_lancamento_movimento and dateadd(DAY,30,data_vencimento) > cast(GETDATE() as DATE) and p.inativo = 0),0) as qtd_parc,bloqueado
                    from sf_lancamento_movimento l inner join sf_grupo_contas g on l.grupo_conta = g.id_grupo_contas
                    inner join sf_contas_movimento cm on l.conta_movimento = cm.id_contas_movimento
                    inner join sf_fornecedores_despesas on sf_fornecedores_despesas.id_fornecedores_despesas = fornecedor_despesa
                    where l.status = 'Aprovado' and fornecedor_despesa = " . $_GET["id"] . "");
                    while ($RFP = odbc_fetch_array($cur)) {
                        if ($RFP["bloqueado"] == 1) {
                            $status = "Bloqueado";
                            $bgstatus = "color:#FF0000";
                        } else {
                            if ($RFP["num_parc"] > 0) {
                                $status = "Pendência Financeira!";
                                $bgstatus = "color:#FF0000";
                            } else {
                                if ($RFP["qtd_parc"] > 0) {
                                    $status = "Ativo";
                                    $bgstatus = "";
                                } else {
                                    $status = "Sem Contrato";
                                    $bgstatus = "color:#FF0000";
                                }
                            }
                        } ?>
                        <tr class="txtPq">
                            <td style="width:154px"><?php echo utf8_encode($RFP["grupo"]); ?></td>
                            <td style="width:153px"><?php echo utf8_encode($RFP["descricao"]); ?></td>
                            <td style="width:153px"><?php echo utf8_encode($RFP["historico_baixa"]); ?></td>
                            <td style="border-right:0; <?php echo $bgstatus; ?>"><?php echo $status; ?></td>
                        </tr>
                        <?php
                    }
                } ?>
            </table>
        </div>
    </div>
    <script type="text/javascript" src="../../js/graficos.js"></script>
    <script type="text/javascript" src="../../js/util.js"></script>                    
    <?php odbc_close($con); ?>
</div>