(function () {
    var apinamespace = 'jSignature';
    function attachHandlers(buttonRenderer, apinamespace, extensionName) {
        var $undoButton = buttonRenderer.call(this);
        (function (jSignatureInstance, $undoButton, apinamespace) {
            jSignatureInstance.events.subscribe(
                    apinamespace + '.change'
                    , function () {
                        if (jSignatureInstance.dataEngine.data.length) {
                            $undoButton.show();
                        } else {
                            $undoButton.hide();
                        }
                    }
            );
        })(this, $undoButton, apinamespace);
        (function (jSignatureInstance, $undoButton, apinamespace) {
            var eventName = apinamespace + '.undo';
            $undoButton.bind('click', function () {
                jSignatureInstance.events.publish(eventName)
            });
            jSignatureInstance.events.subscribe(
                    eventName
                    , function () {
                        var data = jSignatureInstance.dataEngine.data
                        if (data.length) {
                            data.pop()
                            jSignatureInstance.resetCanvas(data)
                        }
                    }
            );
        })(
                this
                , $undoButton
                , this.events.topics.hasOwnProperty(apinamespace + '.undo') ?
                extensionName : apinamespace);
    }

    function ExtensionInitializer(extensionName) {
        var apinamespace = 'jSignature';
        this.events.subscribe(
                apinamespace + '.attachingEventHandlers'
                , function () {
                    if (this.settings[extensionName]) {
                        var oursettings = this.settings[extensionName]
                        if (typeof oursettings !== 'function') {
                            oursettings = function () {
                                var undoButtonSytle = 'position:absolute;display:none;margin:0 !important;top:auto'
                                        , $undoButton = $('<input type="button" value="Desfazer" style="' + undoButtonSytle + '" />')
                                        .appendTo(this.$controlbarLower);
                                var buttonWidth = $undoButton.width();
                                $undoButton.css('left', Math.round((this.canvas.width - buttonWidth) / 2));
                                if (buttonWidth !== $undoButton.width()) {
                                    $undoButton.width(buttonWidth);
                                }
                                return $undoButton;
                            };
                        }
                        attachHandlers.call(this, oursettings, apinamespace, extensionName);
                    }
                }
        );
    }

    var ExtensionAttacher = function () {
        $.fn[apinamespace]('addPlugin', 'instance', 'UndoButton', ExtensionInitializer);
    };
    
    //ExtensionAttacher();
})();