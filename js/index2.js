$(document).ready(function () {
    $.getJSON("./../../util/dcc/saldoDCC.php", {ajax: "true"}, function (j) {
        if ($.isNumeric(j)) {
            $("#saldoDcc").text(j);
        }
    });

    $.getJSON("./../../util/sms/SmsSaldoForm.php", {ajax: "true"}, function (j) {
        if ($.isNumeric(j)) {
            $("#saldoSMS").text(j);
        }
    });
});