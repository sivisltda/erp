function abrirTelaBox2(pagina,altura, largura){
    $("body").append("\
        <div id='newbox2' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'>\n\
            <div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:" + largura + "px; position:absolute; top:50%; left:50%; margin-left:" + ((($(window).width()/2) - (largura/2)) - ($(window).width()/2)) + "px;margin-top:" + ((($(window).height()/2) - (altura/2)) - ($(window).height()/2)) + "px; opacity:1; overflow:visible;'>\n\
                <div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'>\n\
                    <div class='fancybox-outer'>\n\
                        <div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'>\n\
                            <iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:" + altura + "px' " +
                                "src='"+pagina+"'>\n\
                            </iframe>\n\
                        </div>\n\
                    </div>\n\
                </div>\n\
            </div>\n\
        </div>");
}

function abrirTelaBox(pagina,altura, largura){
    $("body").append("\
        <div id='newbox' class='fancybox-overlay fancybox-overlay-fixed' style='width:auto; height:auto; display:block;'>\n\
            <div class='fancybox-wrap fancybox-desktop fancybox-type-iframe fancybox-opened' tabindex='-1' style='width:" + largura + "px; position:absolute; top:50%; left:50%; margin-left:" + ((($(window).width()/2) - (largura/2)) - ($(window).width()/2)) + "px;margin-top:" + ((($(window).height()/2) - (altura/2)) - ($(window).height()/2)) + "px; opacity:1; overflow:visible;'>\n\
                <div class='fancybox-skin' style='padding:0px; width:auto; height:auto;'>\n\
                    <div class='fancybox-outer'>\n\
                        <div class='fancybox-inner' style='overflow:auto; width:100%; height:100%;'>\n\
                            <iframe id='fancybox-frame1387205926318' name='fancybox-frame1387205926318' class='fancybox-iframe' frameborder='0' vspace='0' hspace='0' webkitallowfullscreen='' mozallowfullscreen='' allowfullscreen='' scrolling='no' style='height:" + altura + "px' " +
                                "src='"+pagina+"'>\n\
                            </iframe>\n\
                        </div>\n\
                    </div>\n\
                </div>\n\
            </div>\n\
        </div>");
}

function validateEmail(email)
{
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
} 

var language = $("#txtMnLanguage", window.parent.document).val();
var langdir = $('script[src*="js/util.js"]').attr('src').replace("js/util.js", "");
document.write("<script type=\"text/javascript\" src=\"" + langdir + "Languages/" + language + "/lang." + language + ".js\"></script>");