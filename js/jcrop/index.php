<html>
<head>
	<title>Upload and select image to crop on fly and save</title>
	<script type="text/javascript" src="jquery.min.js"></script>
	<script type="text/javascript" src="jquery.form.js"></script>
	<script type="text/javascript" >
		$(document).ready(function() { 
				$('#UploadButton').click(function(){
					$("#preview").html('');
					$("#gallery").ajaxForm({
						target: '#preview'
					}).submit();
				});
		});
	</script>
</head>

<body>
	<form action="upload.php" method="post" enctype="multipart/form-data" id="gallery">
		<label>Image: </label>
		<input type="file" name="file" value="">
		<input type="submit" value="Upload" id="UploadButton">
	</form>
	<div id="preview"></div>
</body>
</html>