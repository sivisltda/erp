<?php

// Get request id and load image
$conn = mysql_connect("localhost", "root", "");
mysql_select_db('image_up', $conn);

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$targ_w = $targ_h = 150;
	$jpeg_quality = 90;

	$src = 'upload/'.$_POST['src'];
	$img_r = imagecreatefromjpeg($src);
	$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

	imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],$targ_w,$targ_h,$_POST['w'],$_POST['h']);

	header('Content-type: image/jpeg');
	imagejpeg($dst_r,null,$jpeg_quality);

	exit; 
	
}
$id = $_GET['id'];
$qur = mysql_query("select * from image where id='$id'");
while($r = mysql_fetch_array($qur)){
	extract($r);
	$img = $src;
}
// If not a POST request, display page below:
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Live Cropping Demo</title>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
  <script src="jquery.min.js"></script>
  <script src="jquery.Jcrop.min.js"></script>
  <link rel="stylesheet" href="jquery.Jcrop.min.css" type="text/css" />

<script type="text/javascript">

  $(function(){

    $('#cropbox').Jcrop({
      aspectRatio: 1,
      onSelect: updateCoords
    });

  });

  function updateCoords(c)
  {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
  };

  function checkCoords()
  {
    if (parseInt($('#w').val())) return true;
    alert('Please select a crop region then press submit.');
    return false;
  };

</script>
<style type="text/css">
  #target {
    background-color: #ccc;
    width: 500px;
    height: 330px;
    font-size: 24px;
    display: block;
  }


</style>

</head>
<body>

<div class="container">
		<!-- This is the image we're attaching Jcrop to -->
		<img src="upload/<?php echo $img;?>" id="cropbox" />
		<!-- This is the form that our event handler fills -->
		<form action="crop.php" method="post" onsubmit="return checkCoords();">
			<input type="hidden" id="x" name="x" />
			<input type="hidden" id="y" name="y" />
			<input type="hidden" id="w" name="w" />
			<input type="hidden" id="h" name="h" />
			<input type="hidden" name="src" value="<?php echo $img;?>" />
			<input type="submit" value="Crop Image" class="btn btn-large btn-inverse" />
		</form>
	</div>
</body>
</html>
