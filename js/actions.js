$(document).ready(function(){
    
    function setCookie(c_name,value,exdays)
    {
        var exdate=new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
        document.cookie=c_name + "=" + c_value;
    }

    function getCookie(c_name)
    {
        var c_value = document.cookie;
        var c_start = c_value.indexOf(" " + c_name + "=");
        if (c_start == -1)
        {
            c_start = c_value.indexOf(c_name + "=");
        }
        if (c_start == -1)
        {
            c_value = null;
        }
        else
        {
            c_start = c_value.indexOf("=", c_start) + 1;
            var c_end = c_value.indexOf(";", c_start);
            if (c_end == -1)
            {
                c_end = c_value.length;
            }
            c_value = unescape(c_value.substring(c_start,c_end));
        }
        return c_value;
    }
    
    var sSet  = $.cookies.get('sSet');
    if(null == sSet){
        $.cookies.set('sSet','1');
        $.cookies.set('cNav','bordered');
    }
    
    var cNav  = $.cookies.get('cNav');
    var cCont = $.cookies.get('cCont');
    
    if(null != cNav){
        if(cNav == 'bordered'){
            $(".sidebar .navigation").addClass('bordered');                                
            $(".cNav").attr('checked',false);
            $(".cNav[value='bordered']").attr('checked',true).parent('span').addClass('checked');
        }
    }else
        $(".cNav[value='default']").attr('checked',true).parent('span').addClass('checked');

    if(null != cCont){        
        $(".wrapper").addClass(cCont);
        $(".cCont").attr('checked',false);
        $(".cCont[value='"+cCont+"']").attr('checked',true).parent('span').addClass('checked');        
    }else
        $(".cCont[value='']").attr('checked',true).parent('span').addClass('checked');
   
    $(".cNav").click(function(){
        var val = $(this).val();
        if(val != 'default'){
            $(".sidebar .navigation").addClass(val);
            $.cookies.set('cNav',val);
        }else{
            $(".sidebar .navigation").removeClass('bordered');
            $.cookies.set('cNav',null);
        }
    });
    
    $(".cCont").click(function(){
        var val = $(this).val();
        $(".wrapper").removeClass('fixed').addClass(val);
        $.cookies.set('cCont',val);
    });

    $(".navigation.narrow > li > a").click(function(){
        var li = $(this).parent('li');
        if(li.find('ul').length > 0){
            
            if(li.hasClass('active'))
                li.removeClass('active');
            else
                li.addClass('active');
            
            return false;
        }
        
    });
    
    $(".navButton a, .sidebar .close").click(function(){        
        if($(".sidebar").is(":visible"))
            $(".sidebar").slideUp();
        else
            $(".sidebar").slideDown();        
    });
    
    $(".sbutton a").click(function(){
        var popup = $(this).parent('.sbutton').find('.popup');
        if(popup.length > 0){
            if(popup.hasClass('active'))
                popup.removeClass('active');
            else{
                popup.addClass('active');
                popup.find('.checker').show();
                popup.find('.radio').show();
            }
            return false;
        }
    });
    
    $(".file .btn, .file input:text").click(function(){        
        var block = $(this).parent('.file');
        block.find('input:file').click();
        block.find('input:file').change(function(){
            block.find('input:text').val(block.find('input:file').val());
        });
    });

    $(".ublock").click(function(){
        var block = $(this).parents('[class^=block]');        
        add_loader(block);        
        setTimeout(function(){
            remove_loader(block);
        },2000);        
        return false;
    });    
    
    $(".head .buttons > li > a").click(function(event){        
        var li = $(this).parent('li');        
        if(li.find('ul').length > 0){
            if(li.hasClass('active'))
                li.removeClass('active');
            else
                li.addClass('active');
            return false;            
        }
        event.stopPropagation();
    });

    $(".cblock").click(function(){
        var block = $(this).parents('.block').find("[class^=data]");
        if(block.is(':visible')){
            block.fadeOut();            
        }else{
            block.fadeIn();            
        }

        return false;
    });

    $(".body .navigation li a").click(function(){       
        if($(this).attr('href') == '#'){
            if($(this).parent('li').hasClass("active")){
                $(this).parent('li').removeClass('active');
            }else{
                $(".body .navigation li").removeClass('active');
                $(this).parent('li').addClass('active');
            }
            return false;
        }
    });
    
    $(".sidebar .navigation > li > a, .sidebar .navigation > li > .open").click(function(){        
        if($(this).parent('li').find('ul').length > 0){ 
            if($(this).parent('li').hasClass('active')){
                $(this).parent('li').removeClass('active'); 
                //setCookie("menuOpen",0,365);
            }else{
                $(this).parent('li').addClass('active');
                //setCookie("menuOpen",1,365);
            }    
            return false;
        }
    });

    $("table .checkall").click(function(){        
        var iC = $(this).parents('th').index();
        var tB = $(this).parents('table').find('tbody');        
        if($(this).is(':checked'))
            tB.find('tr').each(function(){                
                $(this).find('td:eq('+iC+') input:checkbox:not(:disabled)').attr('checked',true).parent('span').addClass('checked');
            });
        else
            tB.find('tr').each(function(){
                $(this).find('td:eq('+iC+') input:checkbox:not(:disabled)').attr('checked',false).parent('span').removeClass('checked');
            });                    
    });    

    $("table .checker").click(function(event){        
        var tr = $(this).parents('tr');        
        if(tr.hasClass('active'))
            tr.removeClass('active');
        else
            tr.addClass('active');              
        event.stopPropagation();
    });

    $(".table-row-check tbody tr").click(function(){
        
        if($(this).hasClass('active'))
            $(this).removeClass('active');
        else
            $(this).addClass('active');
        
        $(this).find('input:checkbox').each(function(){
            
            if($(this).is(':checked')){
                $(this).attr('checked',false).parent('span').removeClass('checked');
            }else{
                $(this).attr('checked',true).parent('span').addClass('checked');
            }                            
        });        
    });
    $(".alert").click(function(){
        $(this).animate({
            opacity: 0
        },'200','linear',function(){
            $(this).remove();
        });
    });    
    
    $('[data-toggle="tooltip"]').tooltip({
        tooltipClass: "tooltip-styling"
    });
});

$(window).load(function(){    
    fix();            
    var c_name = "menuOpen";
    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1)
    {
        c_start = c_value.indexOf(c_name + "=");
    }
    if (c_start == -1)
    {
        c_value = null;
    }
    else
    {
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1)
        {
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start,c_end));
    }    
    $("#loader").hide();                
    if($(".thumbs").length > 0) thumbs();
});

function fix(){    
    fix_block_items_width('.input-prepend',['.add-on','button'],'input');
    fix_block_items_width('.input-append',['.add-on','button'],'input');         
    if($(".wrapper > .body").height() < window.innerHeight)
        $(".wrapper > .body").height(window.innerHeight+10);        
    else
        $(".wrapper > .body").removeAttr('style');
}


function add_loader(block){
    var bW = $(block).width();
    var bH = $(block).height();    
    $(block).append('<div class="loader" style="width: '+bW+'px; height: '+bH+'px;"><img src="img/loader.gif"/></div>');
}
function remove_loader(block){
    $(block).find('.loader').remove();
}

function fix_block_items_width(block,what,to){
    $(block).each(function(){        
        var iWidth = $(this).width();        
        if(what.length > 0){            
            for(var i=0; i < what.length; i++){
                $(this).find(what[i]).each(function(){
                    iWidth -= $(this).width()+(parseInt($(this).css('padding-left')) * 2);
                });
            }
            $(this).find(to).width(iWidth-14);            
        }
    });        
}

function thumbs(){
    
    $(".thumbs").each(function(){                
        var maxImgHeight = 0;
        var maxTextHeight = 0;            
        $(this).find(".thumbnail").each(function(){
            var imgHeight = $(this).find('a > img').height();
            var textHeight = $(this).find('.caption').height();
            
            maxImgHeight = maxImgHeight < imgHeight ? imgHeight : maxImgHeight;
            maxTextHeight = maxTextHeight < textHeight ? textHeight : maxTextHeight;
        });        
        $(this).find('.thumbnail > a').height(maxImgHeight);
        $(this).find('.thumbnail .caption').height(maxTextHeight);
    });
    
    var w_block = $(".thumbs").width()-20;
    var w_item  = $(".thumbs .thumbnail").width()+10;    
    var c_items = Math.floor(w_block/w_item);    
    var m_items = Math.floor( (w_block-w_item*c_items)/(c_items*2) );    
    $(".thumbs .thumbnail").css('margin',m_items);

}