<?php

function formataNumeros($valor) {
    if (is_numeric($_GET['ts']) && $_GET['ts'] > 0) {
        return escreverNumero($valor);
    } else {
        return $valor;
    }
}

$fat_tur_data = date("d/m/Y");
$fat_mes_data = date("m/Y");
$Status_alunos = [];
$Contas_pagar = [];
$Faturamentos = [];
$Meta_alunos = "0";
$Meta_veiculos = "0";
$Pendencias_A = [];
$Pendencias_S = [];
$veiculos = "0";
$veiculoSuspenso = "0";
$renovados = "0";
$nrenovados = "0";
$totalAtivosAnt = "0";
$clientesAtivosAnt = "0";
$veiculosAtivosAnt = "0";
$contratos_passado = "0";
$contratos_atual = "0";

$query = "set dateformat dmy; select isnull(sum(x.cmm),0) as manha,isnull(sum(x.cmt),0) as tarde,isnull(sum(x.cmn),0) as noite from(
select case when isnull(DATEPART(HOUR,data_cadastro),0) between 0 and 12 then sum(lp.valor_parcela) end cmm,
case when isnull(DATEPART(HOUR,data_cadastro),0) between 13 and 18 then sum(lp.valor_parcela) end cmt,
case when isnull(DATEPART(HOUR,data_cadastro),0) between 19 and 23 then sum(lp.valor_parcela) end cmn
from sf_lancamento_movimento l
inner join sf_grupo_contas gc on gc.id_grupo_contas = l.grupo_conta
inner join sf_lancamento_movimento_parcelas lp on lp.id_lancamento_movimento = l.id_lancamento_movimento
inner join sf_contas_movimento cm on l.conta_movimento = cm.id_contas_movimento
inner join sf_tipo_documento td on l.tipo_documento = td.id_tipo_documento
where lp.inativo = 0 and l.status = 'Aprovado' and lp.data_pagamento is not null and lp.somar_rel = 1 and lp.data_vencimento = '" . $fat_tur_data . "' and empresa = " . $filial . "
and cm.tipo = 'C'
group by data_cadastro,cm.id_contas_movimento,cm.descricao
union all select case when isnull(DATEPART(HOUR,data_cadastro),0) between 0 and 12 then sum(sp.valor_parcela) end cmm,
case when isnull(DATEPART(HOUR,data_cadastro),0) between 13 and 18 then sum(sp.valor_parcela) end cmt,
case when isnull(DATEPART(HOUR,data_cadastro),0) between 19 and 23 then sum(sp.valor_parcela) end cmn from sf_solicitacao_autorizacao p
inner join sf_grupo_contas gc on gc.id_grupo_contas = p.grupo_conta
inner join sf_solicitacao_autorizacao_parcelas sp on sp.solicitacao_autorizacao = p.id_solicitacao_autorizacao
inner join sf_contas_movimento cm on p.conta_movimento = cm.id_contas_movimento
inner join sf_tipo_documento td on sp.tipo_documento = td.id_tipo_documento
where sp.inativo = 0 and status = 'Aprovado' and sp.somar_rel = 1 and sp.data_pagamento is not null and sp.data_parcela = '" . $fat_tur_data . "' and empresa = " . $filial . "
and cm.tipo = 'C'
group by data_cadastro,cm.id_contas_movimento,cm.descricao
union all select case when isnull(DATEPART(HOUR,data_cadastro),0) between 0 and 12 then sum(sp.valor_parcela) end cmm,
case when isnull(DATEPART(HOUR,data_cadastro),0) between 13 and 18 then sum(sp.valor_parcela) end cmt,
case when isnull(DATEPART(HOUR,data_cadastro),0) between 19 and 23 then sum(sp.valor_parcela) end cmn from sf_vendas p
inner join sf_venda_parcelas sp on sp.venda = p.id_venda
inner join sf_tipo_documento td on sp.tipo_documento = td.id_tipo_documento
where sp.inativo = 0 and status = 'Aprovado' and sp.somar_rel = 1 and p.data_venda between '" . $fat_tur_data . " 00:00:00' and '" . $fat_tur_data . " 23:59:59' and empresa = " . $filial . "
and p.cov = 'V' group by data_cadastro,p.cov)as x ";
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $manha = $RFP["manha"];
    $tarde = $RFP["tarde"];
    $noite = $RFP["noite"];
}

$query = "set dateformat dmy; select isnull(sum(x.cmm),0) as primeira,isnull(sum(x.cmt),0) as segunda,isnull(sum(x.cmn),0) as terceira from(
select case when isnull(DATEPART(DAY,data_vencimento),0) between 1 and 10 then sum(lp.valor_parcela) end cmm,
case when isnull(DATEPART(DAY,data_vencimento),0) between 11 and 20 then sum(lp.valor_parcela) end cmt,
case when isnull(DATEPART(DAY,data_vencimento),0) between 21 and 31 then sum(lp.valor_parcela) end cmn
from sf_lancamento_movimento l
inner join sf_grupo_contas gc on gc.id_grupo_contas = l.grupo_conta
inner join sf_lancamento_movimento_parcelas lp on lp.id_lancamento_movimento = l.id_lancamento_movimento
inner join sf_contas_movimento cm on l.conta_movimento = cm.id_contas_movimento
inner join sf_tipo_documento td on l.tipo_documento = td.id_tipo_documento
where lp.inativo = 0 and l.status = 'Aprovado' and lp.somar_rel = 1 and lp.data_pagamento is not null and lp.data_vencimento between '01/" . $fat_mes_data . "' and dateadd(day,-1,dateadd(month,1,'01/" . $fat_mes_data . "'))
and cm.tipo = 'C' and empresa = " . $filial . "
group by data_vencimento,cm.id_contas_movimento,cm.descricao
union all select case when isnull(DATEPART(DAY,data_parcela),0) between 1 and 10 then sum(sp.valor_parcela) end cmm,
case when isnull(DATEPART(DAY,data_parcela),0) between 11 and 20 then sum(sp.valor_parcela) end cmt,
case when isnull(DATEPART(DAY,data_parcela),0) between 21 and 31 then sum(sp.valor_parcela) end cmn from sf_solicitacao_autorizacao p
inner join sf_grupo_contas gc on gc.id_grupo_contas = p.grupo_conta
inner join sf_solicitacao_autorizacao_parcelas sp on sp.solicitacao_autorizacao = p.id_solicitacao_autorizacao
inner join sf_contas_movimento cm on p.conta_movimento = cm.id_contas_movimento
inner join sf_tipo_documento td on sp.tipo_documento = td.id_tipo_documento
where sp.inativo = 0 and sp.somar_rel = 1 and status = 'Aprovado' and sp.data_pagamento is not null and sp.data_parcela between '01/" . $fat_mes_data . "' and dateadd(day,-1,dateadd(month,1,'01/" . $fat_mes_data . "'))
and cm.tipo = 'C' and empresa = " . $filial . "
group by data_parcela,cm.id_contas_movimento,cm.descricao
union all select case when isnull(DATEPART(DAY,CAST(data_venda as DATE)),0) between 1 and 10 then sum(sp.valor_parcela) end cmm,
case when isnull(DATEPART(DAY,CAST(data_venda as DATE)),0) between 11 and 20 then sum(sp.valor_parcela) end cmt,
case when isnull(DATEPART(DAY,CAST(data_venda as DATE)),0) between 21 and 31 then sum(sp.valor_parcela) end cmn from sf_vendas p
inner join sf_venda_parcelas sp on sp.venda = p.id_venda
inner join sf_tipo_documento td on sp.tipo_documento = td.id_tipo_documento
where sp.inativo = 0 and sp.somar_rel = 1 and status = 'Aprovado' and CAST(CAST(data_venda as DATE) as DATE) between '01/" . $fat_mes_data . "' and dateadd(day,-1,dateadd(month,1,'01/" . $fat_mes_data . "'))
and p.cov = 'V' and empresa = " . $filial . " group by CAST(data_venda as DATE),p.cov)as x ";
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $zero_dez = $RFP["primeira"];
    $onze_vinte = $RFP["segunda"];
    $vinte_trinta = $RFP["terceira"];
}

$query = "select fornecedores_status as estado,count(fornecedores_status) total from (
select case when SUBSTRING(fornecedores_status,1,6) = 'Ativo' then 'Ativo' else fornecedores_status end fornecedores_status
from dbo.sf_fornecedores_despesas where id_fornecedores_despesas > 0 and tipo = 'C' and inativo = 0 and fornecedores_status is not null) as x group by fornecedores_status";
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $Status_alunos[] = array("name" => $RFP["estado"], "value" => $RFP["total"]);
}

$query = "set dateformat dmy;select SUM(x.total_pagar) total_pagar, SUM(x.total_pago) total_pago from (
select isnull(SUM(case when valor_pago = 0 then valor_parcela else 0 end),0) total_pagar,
isnull(SUM(case when valor_pago > 0 then valor_parcela else 0 end),0) total_pago
from sf_lancamento_movimento_parcelas inner join sf_lancamento_movimento
on sf_lancamento_movimento_parcelas.id_lancamento_movimento = sf_lancamento_movimento.id_lancamento_movimento
inner join sf_contas_movimento on sf_lancamento_movimento.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas
inner join sf_fornecedores_despesas on sf_lancamento_movimento.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'D'
AND sf_lancamento_movimento_parcelas.inativo = '0' and sf_lancamento_movimento.status = 'Aprovado'
and data_vencimento between '01/" . date("m/Y") . "' and '" . date("t/m/Y") . "' union
select isnull(SUM(case when valor_pago = 0 then valor_parcela else 0 end),0) total_pagar,
isnull(SUM(case when valor_pago > 0 then valor_parcela else 0 end),0) total_pago
from sf_solicitacao_autorizacao_parcelas inner join sf_solicitacao_autorizacao
on sf_solicitacao_autorizacao_parcelas.solicitacao_autorizacao = sf_solicitacao_autorizacao.id_solicitacao_autorizacao
inner join sf_contas_movimento on sf_solicitacao_autorizacao.conta_movimento = sf_contas_movimento.id_contas_movimento
inner join sf_grupo_contas on sf_contas_movimento.grupo_conta = sf_grupo_contas.id_grupo_contas
inner join sf_fornecedores_despesas on sf_solicitacao_autorizacao.fornecedor_despesa = sf_fornecedores_despesas.id_fornecedores_despesas where sf_contas_movimento.tipo = 'D'
AND sf_solicitacao_autorizacao_parcelas.inativo = '0' and status = 'Aprovado'
and data_parcela between '01/" . date("m/Y") . "' and '" . date("t/m/Y") . "' union
select isnull(SUM(case when valor_pago = 0 then valor_parcela else 0 end),0) total_pagar,
isnull(SUM(case when valor_pago > 0 then valor_parcela else 0 end),0) total_pago
from sf_venda_parcelas inner join sf_vendas
on sf_venda_parcelas.venda = sf_vendas.id_venda
inner join sf_fornecedores_despesas on sf_vendas.cliente_venda = sf_fornecedores_despesas.id_fornecedores_despesas where sf_vendas.cov = 'C'
AND sf_venda_parcelas.inativo = '0' and status = 'Aprovado'
and data_parcela between '01/" . date("m/Y") . "' and '" . date("t/m/Y") . "') as x;";
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $Contas_pagar[] = array("name" => "Total a Pagar", "value" => $RFP['total_pagar']);
    $Contas_pagar[] = array("name" => "Total Pago", "value" => $RFP['total_pago']);
}

$query = "set dateformat dmy;
select faturamento_ativos, faturamento_planos from sf_metas where data_meta = '01/" . date("m/Y") . "'";
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $Meta_alunos = $RFP['faturamento_ativos'];
    $Meta_veiculos = $RFP['faturamento_planos'];
}

$fat_ult_mes = date("m/Y");
$query = "set dateformat dmy;select isnull(sum(x.a00),0) as a00,isnull(sum(x.a01),0) as a01,isnull(sum(x.a02),0) as a02,isnull(sum(x.a03),0) as a03 from(
select case when lp.data_vencimento between dateadd(MONTH,-3,'01/" . $fat_ult_mes . "') and dateadd(day,-1,dateadd(month,-2,'01/" . $fat_ult_mes . "')) then sum(lp.valor_parcela) end a03,
case when lp.data_vencimento between dateadd(MONTH,-2,'01/" . $fat_ult_mes . "') and dateadd(day,-1,dateadd(month,-1,'01/" . $fat_ult_mes . "')) then sum(lp.valor_parcela) end a02,
case when lp.data_vencimento between dateadd(MONTH,-1,'01/" . $fat_ult_mes . "') and dateadd(day,-1,dateadd(month,0,'01/" . $fat_ult_mes . "')) then sum(lp.valor_parcela) end a01,
case when lp.data_vencimento between dateadd(MONTH,-0,'01/" . $fat_ult_mes . "') and dateadd(day,-1,dateadd(month,1,'01/" . $fat_ult_mes . "')) then sum(lp.valor_parcela) end a00
from sf_lancamento_movimento l inner join sf_grupo_contas gc on gc.id_grupo_contas = l.grupo_conta
inner join sf_lancamento_movimento_parcelas lp on lp.id_lancamento_movimento = l.id_lancamento_movimento
inner join sf_contas_movimento cm on l.conta_movimento = cm.id_contas_movimento
inner join sf_tipo_documento td on l.tipo_documento = td.id_tipo_documento
where lp.inativo = 0 and l.status = 'Aprovado' and lp.somar_rel = 1 and lp.data_pagamento is not null and lp.data_vencimento between dateadd(MONTH,-3,'01/" . $fat_ult_mes . "') and dateadd(day,-1,dateadd(month,1,'01/" . $fat_ult_mes . "'))
and cm.tipo = 'C' and empresa = " . $filial . " group by data_vencimento,cm.id_contas_movimento,cm.descricao
union all select case when data_lanc between dateadd(MONTH,-3,'01/" . $fat_ult_mes . "') and dateadd(day,-1,dateadd(month,-2,'01/" . $fat_ult_mes . "')) then sum(sp.valor_parcela) end a03,
case when data_lanc between dateadd(MONTH,-2,'01/" . $fat_ult_mes . "') and dateadd(day,-1,dateadd(month,-1,'01/" . $fat_ult_mes . "')) then sum(sp.valor_parcela) end a02,
case when data_lanc between dateadd(MONTH,-1,'01/" . $fat_ult_mes . "') and dateadd(day,-1,dateadd(month,0,'01/" . $fat_ult_mes . "')) then sum(sp.valor_parcela) end a01,
case when data_lanc between dateadd(MONTH,-0,'01/" . $fat_ult_mes . "') and dateadd(day,-1,dateadd(month,1,'01/" . $fat_ult_mes . "')) then sum(sp.valor_parcela) end a00
from sf_solicitacao_autorizacao p
inner join sf_grupo_contas gc on gc.id_grupo_contas = p.grupo_conta
inner join sf_solicitacao_autorizacao_parcelas sp on sp.solicitacao_autorizacao = p.id_solicitacao_autorizacao
inner join sf_contas_movimento cm on p.conta_movimento = cm.id_contas_movimento
inner join sf_tipo_documento td on sp.tipo_documento = td.id_tipo_documento
where sp.inativo = 0 and status = 'Aprovado' and sp.somar_rel = 1 and sp.data_pagamento is not null and p.data_lanc between dateadd(MONTH,-3,'01/" . $fat_ult_mes . "') and dateadd(day,-1,dateadd(month,1,'01/" . $fat_ult_mes . "'))
and cm.tipo = 'C'  and empresa = " . $filial . "
group by data_lanc,cm.id_contas_movimento,cm.descricao
union all select case when CAST(data_venda as DATE) between dateadd(MONTH,-3,'01/" . $fat_ult_mes . "') and dateadd(day,-1,dateadd(month,-2,'01/" . $fat_ult_mes . "')) then sum(sp.valor_parcela) end a03,
case when CAST(data_venda as DATE) between dateadd(MONTH,-2,'01/" . $fat_ult_mes . "') and dateadd(day,-1,dateadd(month,-1,'01/" . $fat_ult_mes . "')) then sum(sp.valor_parcela) end a02,
case when CAST(data_venda as DATE) between dateadd(MONTH,-1,'01/" . $fat_ult_mes . "') and dateadd(day,-1,dateadd(month,0,'01/" . $fat_ult_mes . "')) then sum(sp.valor_parcela) end a01,
case when CAST(data_venda as DATE) between dateadd(MONTH,-0,'01/" . $fat_ult_mes . "') and dateadd(day,-1,dateadd(month,1,'01/" . $fat_ult_mes . "')) then sum(sp.valor_parcela) end a00
from sf_vendas p inner join sf_venda_parcelas sp on sp.venda = p.id_venda
inner join sf_tipo_documento td on sp.tipo_documento = td.id_tipo_documento
where sp.inativo = 0 and status = 'Aprovado' and sp.somar_rel = 1 and CAST(data_venda as DATE) between dateadd(MONTH,-3,'01/" . $fat_ult_mes . "') and dateadd(day,-1,dateadd(month,1,'01/" . $fat_ult_mes . "'))
and p.cov = 'V'  and empresa = " . $filial . " group by CAST(data_venda as DATE),p.cov
)as x ";
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    for ($i = 3; $i >= 0; $i--) {
        $Faturamentos[] = array("name" => date("m/y", strtotime("-" . $i . " month", strtotime(str_replace('/', '-', "01/" . $fat_ult_mes)))), "value" => $RFP["a0" . $i]);
    }
}

$query = "set dateformat dmy;select x.id_usuario,x.login_user,count(x.id_tele) total from (
select id_tele,login_user,id_usuario from sf_telemarketing
inner join sf_usuarios on sf_usuarios.id_usuario = para_pessoa
where pendente = 1 and (proximo_contato is null or proximo_contato < GETDATE())
and atendimento_servico = @@ union
select sf_telemarketing.id_tele,login_user,id_usuario from sf_telemarketing
inner join sf_mensagens_telemarketing on sf_mensagens_telemarketing.id_tele = sf_telemarketing.id_tele
inner join sf_usuarios on sf_usuarios.id_usuario = id_usuario_de
where pendente = 1 and (proximo_contato is null or proximo_contato < GETDATE())
and atendimento_servico = @@ union
select sf_telemarketing.id_tele,login_user,id_usuario from sf_telemarketing
inner join sf_mensagens_telemarketing on sf_mensagens_telemarketing.id_tele = sf_telemarketing.id_tele
inner join sf_usuarios on sf_usuarios.id_usuario = id_usuario_para
where pendente = 1 and (proximo_contato is null or proximo_contato < GETDATE())
and atendimento_servico = @@) as x group by x.id_usuario,x.login_user order by 3 desc";

$cur = odbc_exec($con, str_replace("@@", "0", $query));
while ($RFP = odbc_fetch_array($cur)) {
    $Pendencias_A[] = array("id" => $RFP['id_usuario'],"name" => $RFP['login_user'], "value" => $RFP['total']);
}

$cur = odbc_exec($con, str_replace("@@", "1", $query));
while ($RFP = odbc_fetch_array($cur)) {
    $Pendencias_S[] = array("id" => $RFP['id_usuario'],"name" => $RFP['login_user'], "value" => $RFP['total']);
}

$query = "set dateformat dmy; SELECT    
sum(case when sf_vendas_planos.planos_status = 'Ativo' then 1 else 0 end) total,
sum(case when sf_vendas_planos.planos_status = 'Suspenso' then 1 else 0 end) suspenso
from sf_fornecedores_despesas_veiculo v 
inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = v.id_fornecedores_despesas
left join sf_vendas_planos on id_veiculo = v.id left join sf_produtos on id_prod_plano = conta_produto
left join sf_usuarios on id_user_resp = id_usuario    
WHERE id > 0 AND f.tipo = 'C' AND v.id in(select id_veiculo from sf_vistorias where data_aprov is not null) 
and sf_vendas_planos.planos_status in ('Ativo', 'Suspenso')";
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $veiculos = $RFP["total"];
    $veiculoSuspenso = $RFP["suspenso"];
}

$query = "set dateformat dmy;select sum(case when dt_pagamento_mens is null then 0 else 1 end) renovados,
sum(case when dt_pagamento_mens is null then 1 else 0 end) nrenovados
from sf_vendas_planos_mensalidade m inner join sf_vendas_planos on id_plano = id_plano_mens
inner join sf_fornecedores_despesas on id_fornecedores_despesas = favorecido
inner join sf_produtos on sf_produtos.conta_produto = id_prod_plano	
where id_fornecedores_despesas > 0 and sf_fornecedores_despesas.tipo = 'C' and inativo = 0 and dt_cancelamento is null 
and planos_status not in ('Novo') and dt_inicio_mens between '" . getData("B") . "' and '" . getData("T") . "'";
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $renovados = $RFP["renovados"];
    $nrenovados = $RFP["nrenovados"];
}

$query = "set dateformat dmy;select * from sf_fechamento_mes where data = dateadd(month, -1, '" . getData("T") . "')";
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $totalAtivosAnt = ($RFP["valor_mensalidade"] + $RFP["valor_servico"]);
    $clientesAtivosAnt = $RFP["clientes_ativos"];
    $veiculosAtivosAnt = $RFP["veiculos_ativos"];
}

$query = "set dateformat dmy; 
select (select count(id_plano)
from sf_fornecedores_despesas inner join sf_vendas_planos on id_fornecedores_despesas=favorecido 
inner join sf_vendas_planos_mensalidade A on A.id_plano_mens = sf_vendas_planos.id_plano 
where sf_fornecedores_despesas.tipo='C'
and inativo=0 and id_item_venda_mens is not null 
and dt_pagamento_mens between dateadd(month,-1,'" . getData("B") . " 00:00:00') and dateadd(day,-1,'" . getData("B") . " 23:59:59')
and (select max(dt_pagamento_mens) from sf_vendas_planos_mensalidade B where B.id_plano_mens=A.id_plano_mens and B.id_mens < A.id_mens) is null) mes_passado,
(select count(id_plano)
from sf_fornecedores_despesas inner join sf_vendas_planos on id_fornecedores_despesas=favorecido 
inner join sf_vendas_planos_mensalidade A on A.id_plano_mens = sf_vendas_planos.id_plano 
where sf_fornecedores_despesas.tipo='C'
and inativo=0 and id_item_venda_mens is not null 
and dt_pagamento_mens between '" . getData("B") . " 00:00:00' and '" . getData("E") . " 23:59:59'
and (select max(dt_pagamento_mens) from sf_vendas_planos_mensalidade B where B.id_plano_mens=A.id_plano_mens and B.id_mens < A.id_mens) is null) mes_atual";
//echo $query; exit;
$cur = odbc_exec($con, $query);
while ($RFP = odbc_fetch_array($cur)) {
    $contratos_passado = $RFP["mes_passado"];
    $contratos_atual = $RFP["mes_atual"];
}